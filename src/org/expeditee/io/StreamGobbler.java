/**
 *    StreamGobbler.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.io;


import java.util.*;
import java.io.*;

import javax.swing.SwingUtilities;

import org.expeditee.gui.MessageBay;

// Base on: "When When Runtime.exec() won't"
//   http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html

public class StreamGobbler extends Thread
{
	public enum MessageBayType { display, error };
	
	class UpdateMessageBay implements Runnable {
		
		  protected final MessageBayType type;
		  protected final String line;
		  
		  UpdateMessageBay(MessageBayType type, String line) 
		  {
		    this.type = type;
		    this.line = line;
		  }
		  
		  public void run() 
		  {
			  if (type == MessageBayType.display) {
            	  MessageBay.displayMessage(line);
              }
              else {
            	  MessageBay.errorMessage(line);
              }
		    
		  }
		}
		
		
		
    InputStream is;
    MessageBayType type;

    
    public StreamGobbler(String threadName, InputStream is, MessageBayType type)
    {
    	super(threadName);
    	this.is = is;
    	this.type = type;
    }

    public void run()
    {
        try
        {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line=null;
            
            while ( (line = br.readLine()) != null)
            {
            	// MessageBay is on the AWT event thread, so need to use 'invokeLater' to avoid thread deadlock
            	Runnable updateMessageBayTask = new UpdateMessageBay(type,line);
                SwingUtilities.invokeLater(updateMessageBayTask);
            	
            }
           
           
        } 
        catch (IOException ioe) {
            ioe.printStackTrace();  
        }
    }
}

