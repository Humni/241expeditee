/**
 *    ProxyWriter.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.io;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PipedWriter;
import java.io.Writer;

public class ProxyWriter extends BufferedWriter {

	private StringBuffer _contents = null;

	public ProxyWriter(Writer out) {
		super(out);
	}

	public ProxyWriter(Writer out, int sz) {
		super(out, sz);
	}

	public ProxyWriter(boolean useClipboard) {
		// create a writer that does nothing
		super(new PipedWriter());
		_contents = new StringBuffer("");
	}

	@Override
	public void write(String s) throws IOException {
		if (_contents != null) {
			_contents.append(s);
		} else
			super.write(s);
	}

	@Override
	public void flush() throws IOException {
		if (_contents != null) {
			StringSelection selection = new StringSelection(_contents
					.toString());
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
					selection, null);
		} else
			super.flush();
	}

	@Override
	public void close() throws IOException {
		if (_contents == null)
			super.close();
	}
}
