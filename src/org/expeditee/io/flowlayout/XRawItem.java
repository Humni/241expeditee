/**
 *    XRawItem.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.io.flowlayout;

import org.expeditee.items.Item;
import org.expeditee.items.Text;

public class XRawItem extends XItem 
{
	protected Item item;
	protected XGroupItem xgroup;
	
	public XRawItem(Item item, final XGroupItem belongsTo) 
	{
		this.item = item;
		this.xgroup = belongsTo;
		
		if (item instanceof Text) {
			// Expecting it to be a text item
			// For it's bounding rectangle, go with the tighter 'PixelBounds' version
			
			Text text_item = (Text)item;
			bounding_rect = text_item.getPixelBoundsUnion();
		}
		else {
			// Just in case it isn't a text item
			bounding_rect = item.getArea().getBounds();
		}
		
	}

	public Item getItem()
	{
		return item;
	}
	
	public XGroupItem getGroup() {
		return xgroup;
	}
	
	public String toString() {
		return "XRawItem For " + item.toString();
	}
	
}
