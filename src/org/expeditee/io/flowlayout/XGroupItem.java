/**
 *    XGroupItem.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.io.flowlayout;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.expeditee.gui.Frame;
import org.expeditee.gui.FrameUtils;
import org.expeditee.items.Item;
import org.expeditee.items.Line;
import org.expeditee.items.Text;

public class XGroupItem extends XItem {
	public static final Text GROUPSEP_END = new Text('2' + ""); // ASCII '2' is
																// start of text

	public static final Text GROUPSEP_START = new Text('3' + ""); // ASCII '3'
																	// is end of
																	// text

	enum FlowType {
		in_flow, out_of_flow_original, out_of_flow_faked_position
	};

	public static boolean doImplicitBoxing = true;

	class MultiArrowHeadComparable implements Comparator<Item> {

		@Override
		public int compare(Item o1, Item o2) {
			int o1y = o1.getY();
			int o2y = o2.getY();

			int order = 0; // default to assume they are identical

			if (o1y < o2y) {
				order = -1; // done, o1 is higher up the frame than o2 => our
							// definition of 'before'
			} else if (o2y < o1y) {
				order = +1; // also done, o1 is lower down the frame than o2 =>
							// our definition of 'after'

			} else {
				// have identical 'y' values, so look to 'x' to tie-break

				int o1x = o1.getX();
				int o2x = o2.getX();

				if (o1x < o2x) {
					order = -1; // done, o1 is to the left of o2 => our
								// definition of 'before'
				} else if (o2x < o1x) {
					order = +1; // also done, o1 is to the right of o2 => our
								// definition of 'after'
				}
			}

			return order;
		}
	}

	Frame frame;

	protected int y_span_height;
	protected YOverlappingItemsSpan[] yitems_span_array;

	List<Text> raw_text_item_list;
	List<XGroupItem> grouped_item_list;
	List<Item> remaining_item_list;

	FlowType out_of_flow;

	protected XGroupItem() {
		frame = null;

		y_span_height = 0;
		yitems_span_array = null;

		raw_text_item_list = null;
		grouped_item_list = null;
		remaining_item_list = null;

		out_of_flow = FlowType.in_flow;
	}

	public XGroupItem(Frame frame, List<Item> y_ordered_items,
			Polygon enclosing_polygon) {
		this.frame = frame;
		this.out_of_flow = FlowType.in_flow;
		;

		if (enclosing_polygon == null) {
			// e.g. when the top-level case, with no enclosing polygon at this
			// stage
			// => generate one based on the bounding box of the y_ordered_items
			enclosing_polygon = DimensionExtent
					.boundingBoxPolygon(y_ordered_items);
		}

		Rectangle enclosing_bounding_rect = enclosing_polygon.getBounds();
		initSpanArray(enclosing_bounding_rect);

		// Step 1: Separate out the raw-text-items, the grouped-items and
		// 'remaining' (e.g. points and lines)

		raw_text_item_list = new ArrayList<Text>();
		grouped_item_list = new ArrayList<XGroupItem>();
		remaining_item_list = new ArrayList<Item>();

		separateYOverlappingItems(frame, y_ordered_items, enclosing_polygon,
				raw_text_item_list, grouped_item_list, remaining_item_list);

		// Step 2: Add in the raw-text items
		for (Text text_item : raw_text_item_list) {

			XRawItem x_raw_item = new XRawItem(text_item, this);
			// overspill can occur (and is acceptable) when raw-text item spills
			// out of enclosing shape (such as a rectangle)
			if(x_raw_item.bounding_rect == null) {
				final StringBuilder errorMsg = 
						new StringBuilder("Was about to try mapInItem(XRawItem) but found a null bounding_rect.  Item details: ");
				final String nl = System.getProperty("line.separator");
				errorMsg.append("\t Item parent: " 			+ text_item.getParent() + nl);
				errorMsg.append("\t Item position: " 		+ text_item.getPosition() + nl);
				errorMsg.append("\t Item text content: " 	+ text_item.getText() + nl);
				System.err.println(errorMsg.toString());
			}
			else mapInItem(x_raw_item);
		}

	}

	public XGroupItem(Frame frame, List<Item> y_ordered_items) {
		this(frame, y_ordered_items, null);
	}

	protected XGroupItem(XGroupItem imprint, Rectangle copy_to_bounding_rect) {
		super();

		// Implement a shallow copy => share references to frame, and item list,
		// and y-span
		// Only the bounding box is changed => set to the
		// 'copy_to_bounding_rect' passed in

		this.frame = imprint.frame;

		y_span_height = imprint.y_span_height;
		yitems_span_array = imprint.yitems_span_array;

		this.raw_text_item_list = imprint.raw_text_item_list;
		this.grouped_item_list = imprint.grouped_item_list;

		// int offX = imprint.getBoundingRect().x - copy_to_bounding_rect.x;
		// int offY = imprint.getBoundingRect().y - copy_to_bounding_rect.y;
		// this.grouped_item_list = new ArrayList<XGroupItem>();
		// for(XGroupItem newChild : imprint.grouped_item_list) {
		// Rectangle newRect = newChild.getBoundingRect();
		// newRect.x -= offX;
		// newRect.y -= offY;
		// this.grouped_item_list.add(new XGroupItem(newChild, newRect));
		// }
		this.remaining_item_list = imprint.remaining_item_list;

		this.out_of_flow = imprint.out_of_flow; // Or perhaps set it to
												// FlowType.out_of_flow_fake_position
												// straight away?

		this.bounding_rect = new Rectangle(copy_to_bounding_rect); // deep copy
																	// to be on
																	// the safe
																	// side
	}

	protected void initSpanArray(Rectangle bounding_rect) {
		this.bounding_rect = bounding_rect;

		int y_top = getBoundingYTop();
		int y_bot = getBoundingYBot();

		if (y_top <= y_bot) {
			// => have non-trivial span

			y_span_height = (y_bot - y_top) + 2; // Polygon in Java excludes
													// right and bottom sides so
													// need extra "+1" in calc

			yitems_span_array = new YOverlappingItemsSpan[y_span_height];
		} else {
			y_span_height = 0;
			yitems_span_array = null;
		}
	}

	public YOverlappingItemsSpan getSpanItemAt(int index) {
		return yitems_span_array[index];
	}

	public void setOutOfFlow(FlowType flow_type) {
		out_of_flow = flow_type;
	}

	public FlowType getFlowItem() {
		return out_of_flow;
	}

	public boolean isOriginalOutOfFlowItem() {
		return out_of_flow == FlowType.out_of_flow_original;
	}

	public boolean isFakedOutOfFlowItem() {
		return out_of_flow == FlowType.out_of_flow_faked_position;
	}

	public List<Text> getRawTextItemList() {
		return raw_text_item_list;
	}

	public List<Text[]> getRawTextLines() {
		final List<Text> rawText = getRawTextItemList();
		final List<Text> lineStarts = new LinkedList<Text>();
		for (final YOverlappingItemsSpan span : this.yitems_span_array) {
			if (span instanceof YOverlappingItemsTopEdge) {
				final YOverlappingItemsTopEdge topEdge = (YOverlappingItemsTopEdge) span;
				final List<XItem> xLine = topEdge.getXOrderedLine()
						.getXItemList();
				for (final XItem xitem : xLine) {
					if (xitem instanceof XRawItem) {
						final Text item = (Text) ((XRawItem) xitem).getItem();
						lineStarts.add(item);
						break;
					}
				}
			}
		}
		final List<Integer> indexes = new LinkedList<Integer>();
		for (final Text lineStart : lineStarts)
			indexes.add(rawText.indexOf(lineStart));
		final List<Text[]> ret = new LinkedList<Text[]>();
		int rangeEnd = indexes.get(0);
		int last = rangeEnd;
		for (int i = 1; i < indexes.size(); i++) {
			rangeEnd = indexes.get(i);
			final List<Text> part = new LinkedList<Text>(rawText.subList(0,
					rangeEnd - last));
			last = rangeEnd;
			rawText.removeAll(part);
			part.toArray(new Text[] {});
			ret.add(part.toArray(new Text[] {}));
		}
		ret.add(rawText.toArray(new Text[] {}));
		return ret;
	}

	public List<XGroupItem> getGroupedItemList() {
		return grouped_item_list;
	}

	public List<Item> getRemainingItemList() {
		return remaining_item_list;
	}

	public int getItemSpanLength() {
		return yitems_span_array.length;
	}

	public YOverlappingItemsSpan getYOverlappingItemsSpan(int y) {
		int y_index = y - getBoundingYTop();

		if ((y_index < 0) || (y_index >= yitems_span_array.length)) {
			int y_top = getBoundingYTop();
			int y_bot = y_top + yitems_span_array.length - 1;

			System.err
					.println("Error in getYOverlappingItemsSpan(): index out of bounds for value "
							+ y);
			System.err.println("  => Operation mapped into local array: y-top="
					+ y_top + ", y-bot=" + y_bot);

			return null;
		}
		return yitems_span_array[y_index];
	}

	protected int cropToTop(int y) {
		int y_index = y - getBoundingYTop();

		if (y_index < 0) {
			y = getBoundingYTop();
		}

		return y;
	}

	protected int cropToBot(int y) {
		int y_index = y - getBoundingYTop();

		if (y_index >= yitems_span_array.length) {
			y = getBoundingYBot();
		}

		return y;
	}

	public void setYOverlappingItemsSpan(int y,
			YOverlappingItemsSpan yitems_span) {
		int y_index = y - getBoundingYTop();

		if ((y_index < 0) || (y_index >= yitems_span_array.length)) {

			int y_top = getBoundingYTop();
			int y_bot = y_top + yitems_span_array.length - 1;

			System.err
					.println("Error in setYOverlappingItemsSpan(): index out of bounds for value "
							+ y);
			System.err.println("  => Operation mapped into local array: y-top="
					+ y_top + ", y-bot=" + y_bot);

			return;
		}
		yitems_span_array[y_index] = yitems_span;
	}

	protected List<Item> followLinesToArrowHeads(Collection<Item> visited,
			Item anchor_item, List<Line> used_in_lines) {
		List<Item> arrow_head_endpoints = new ArrayList<Item>();

		for (Line line : used_in_lines) {

			Item start_item = line.getStartItem();

			if (start_item == anchor_item) {
				// the line we're considering is heading in the right direction

				Item end_item = line.getEndItem();

				if (!visited.contains(end_item)) {
					// Needs processing
					visited.add(end_item);

					List<Line> follow_lines = end_item.getLines();

					if (follow_lines.size() == 1) {
						// reached an end-point
						if (end_item.hasVisibleArrow()) {
							arrow_head_endpoints.add(end_item);
						}
					} else if (follow_lines.size() > 1) {

						List<Item> followed_arrow_heads = followLinesToArrowHeads(
								visited, end_item, follow_lines);
						arrow_head_endpoints.addAll(followed_arrow_heads);
					}
				}
			}

		}
		return arrow_head_endpoints;
	}

	protected XGroupItem innermostXGroup(Item arrow_head,
			List<XGroupItem> grouped_item_list) {
		XGroupItem innermost_item = null;

		for (XGroupItem xgroup_item : grouped_item_list) {
			if (xgroup_item.containsItem(arrow_head)) {

				innermost_item = xgroup_item;

				// Now see if it is in any of the nested ones?

				List<XGroupItem> nested_group_item_list = xgroup_item.grouped_item_list;

				XGroupItem potentially_better_item = innermostXGroup(
						arrow_head, nested_group_item_list);

				if (potentially_better_item != null) {
					innermost_item = potentially_better_item;
				}
				break;
			}

		}

		return innermost_item;
	}

	protected void removeXGroupItem(XGroupItem remove_item,
			List<XGroupItem> grouped_item_list) {

		for (XGroupItem xgroup_item : grouped_item_list) {

			if (xgroup_item == remove_item) {
				grouped_item_list.remove(xgroup_item);
				return;
			} else {
				List<XGroupItem> nested_group_item_list = xgroup_item.grouped_item_list;
				removeXGroupItem(remove_item, nested_group_item_list);

			}
		}
	}

	protected boolean daisyChainContainsLoop(final XGroupItem toplevel_xgroup,
			final Item exitingDot, Collection<XGroupItem> groupsSeen) {
		final Item start_item = exitingDot;
		final Item end_item = start_item.getLines().get(0).getEndItem();
		final XGroupItem xgroup = innermostXGroup(end_item,
				toplevel_xgroup.getGroupedItemList());
		
		if(xgroup == null) return false;

		if (groupsSeen.contains(xgroup))
			return true;

		groupsSeen.add(xgroup);

		for (final Item i : xgroup.remaining_item_list) {
			final List<Line> lines = i.getLines();
			if (lines.size() == 1 && lines.get(0).getStartItem() == i) {
				return daisyChainContainsLoop(toplevel_xgroup, i, groupsSeen);
			}
		}
		return false;
	}

	protected void repositionOutOfFlowGroupsFollowLine(
			XGroupItem toplevel_xgroup, Item remaining_item,
			Collection<XGroupItem> out_of_flow) {
		// See if this item is the start of a line
		// Follow it if it is
		// For each end of the line (potentially a multi-split poly-line) that
		// has an arrow head:
		// See if the arrow-head falls inside an XGroup area
		// => Ignore endings that are in the same XGroupItem as the line started
		// in
		List<Line> used_in_lines = remaining_item.getLines();
		if (used_in_lines.size() == 1) {
			// at the start (or end) of a line

			Item start_item = used_in_lines.get(0).getStartItem();

			if (remaining_item == start_item) {

				// found the start of a line

				Collection<Item> visited = new HashSet<Item>();
				visited.add(remaining_item);

				List<Item> arrow_head_endpoints = followLinesToArrowHeads(
						visited, remaining_item, used_in_lines);

				// System.out.println("**** For Xgroup " + this +
				// " with dot starting at " + remaining_item + " has " +
				// arrow_head_endpoints.size() + " arrow head endpoints");

				Collections.sort(arrow_head_endpoints,
						new MultiArrowHeadComparable());

				for (Item arrow_head : arrow_head_endpoints) {
					// find the inner-most group it falls within

					List<XGroupItem> toplevel_grouped_item_list = toplevel_xgroup
							.getGroupedItemList();

					XGroupItem xgroup_item = innermostXGroup(arrow_head,
							toplevel_grouped_item_list);

					if (xgroup_item != null) {

						// Ignore if the found 'xgroup_item' is 'this'
						// (i.e. the found arrow head is at the same XGroupItem
						// level we are currently processing)

						if (xgroup_item != this) {

							if (/* out_of_flow.contains(xgroup_item) && */daisyChainContainsLoop(
									toplevel_xgroup, start_item,
									new ArrayList<XGroupItem>())) {
								System.err.println("#Found infinite loop; not following.  On frame: " +
									start_item.getParent().getName() + " At position: " + 
										start_item.getPosition());
								continue;
							}

							if (!out_of_flow.contains(xgroup_item)) {
								for (Item remaining_item_deeper : xgroup_item
										.getRemainingItemList()) {
									xgroup_item
											.repositionOutOfFlowGroupsFollowLine(
													toplevel_xgroup,
													remaining_item_deeper,
													out_of_flow);
								}
							}

							out_of_flow.add(xgroup_item);

							// Can't delete here, as it causes a concurrent
							// exception => add to 'out_of_flow' hashmap and
							// perform removal later

							// System.out.println("**** innermost XGroupItem = "
							// + xgroup_item);

							// Artificially rework its (x,y) org and dimension
							// to make it appear where the start of the arrow is

							Rectangle start_rect = start_item.getArea()
									.getBounds();

							XGroupItem xgroup_item_shallow_copy = new XGroupItem(
									xgroup_item, start_rect);

							// Perhaps the following two lines should be moved
							// to inside the constructor??

							xgroup_item
									.setOutOfFlow(FlowType.out_of_flow_original);
							xgroup_item_shallow_copy
									.setOutOfFlow(FlowType.out_of_flow_faked_position);

							// xgroup_item.setBoundingRect(start_rect);
							// xgroup_item.setOutOfFlow();

							// Finally add it in
							// mapInItem(xgroup_item);
							mapInItem(xgroup_item_shallow_copy);
						}
					}
				}
			}

		}
	}

	/**
	 * Look for any 'out-of-flow' XGroup boxes, signalled by the user drawing an
	 * arrow line to it => force an artificial change to such boxes so its
	 * dimensions becomes the starting point of the arrow line. This will make
	 * it sort to the desired spot in the YOverlapping span
	 */

	public void repositionOutOfFlowGroupsRecursive(XGroupItem toplevel_xgroup,
			Collection<XGroupItem> out_of_flow) {
		// Map in all the items in the given list:
		for (Item remaining_item : remaining_item_list) {

			repositionOutOfFlowGroupsFollowLine(toplevel_xgroup,
					remaining_item, out_of_flow);
		}

		// Now recursively work through each item's nested x-groups
		for (XGroupItem xgroup_item : grouped_item_list) {

			if (!out_of_flow.contains(xgroup_item)) {
				xgroup_item.repositionOutOfFlowGroupsRecursive(toplevel_xgroup,
						out_of_flow);
			}
		}
	}

	public void repositionOutOfFlowGroups(XGroupItem toplevel_xgroup) {
		// Collection<XGroupItem> out_of_flow = new HashSet<XGroupItem>();
		Collection<XGroupItem> out_of_flow = new ArrayList<XGroupItem>();

		repositionOutOfFlowGroupsRecursive(toplevel_xgroup, out_of_flow);

		// List<XGroupItem >toplevel_grouped_item_list =
		// toplevel_xgroup.getGroupedItemList();

		// ****

		// Want to remove the original "out-of-position" blocks that were found,
		// as (for each arrow
		// point to such an original) shallow-copies now exist with 'faked'
		// positions that correspond
		// to where the arrows start.

		// No longer remove them from the nested grouped structure, rather, rely
		// on these items being
		// tagged "isOutOfFLow()" to be skipped by subsequent traversal calls
		// (e.g., to generate
		// the normal "in-flow" nested blocks)

		// Structuring things this way, means that it is now possible to have
		// multiple arrows
		// pointing to the same block of code, and both "out-of-flow" arrows
		// will be honoured,
		// replicating the code at their respective start points

		/*
		 * for (XGroupItem xgroup_item: out_of_flow) {
		 * removeXGroupItem(xgroup_item,toplevel_grouped_item_list); }
		 */

	}

	/**
	 * Focusing only on enclosures that fit within the given polygon *and* have
	 * this item in it, the method finds the largest of these enclosures and
	 * returns all the items it contains
	 * 
	 * @return
	 */
	public Collection<Item> getItemsInNestedEnclosure(Item given_item,
			AreaPolygon outer_polygon) {
		Collection<Item> sameEnclosure = null;
		Collection<Item> seen = new HashSet<Item>();

		double enclosureArea = 0.0;

		for (Item i : frame.getItems()) {

			// Go through all the enclosures ...

			if (!seen.contains(i) && i.isEnclosed()) {

				Collection<Item> i_enclosing_dots_coll = i.getEnclosingDots();
				List<Item> i_enclosing_dots = new ArrayList<Item>(
						i_enclosing_dots_coll); // Change to List of items

				seen.addAll(i_enclosing_dots);

				Polygon i_polygon = new Polygon();
				for (int di = 0; di < i_enclosing_dots.size(); di++) {
					Item d = i_enclosing_dots.get(di);

					i_polygon.addPoint(d.getX(), d.getY());
				}

				// ... looking for one that is completely contained in the
				// 'outer_polygon' and ...
				if (outer_polygon.completelyContains(i_polygon)) {

					Collection<Item> enclosed = i.getEnclosedItems();

					// ... includes this item
					if (enclosed.contains(given_item)) {

						// Remember it if it is larger than anything we've
						// encountered before
						if ((i.getEnclosedArea() > enclosureArea)) {
							enclosureArea = i.getEnclosedArea();
							sameEnclosure = enclosed;
						}
					}
				}

			}
		}

		if (sameEnclosure == null)
			return new LinkedList<Item>();

		return sameEnclosure;
	}

	public void separateYOverlappingItems(Frame frame, List<Item> item_list,
			Polygon enclosing_polygon, List<Text> raw_text_item_list,
			List<XGroupItem> grouped_item_list, List<Item> remaining_item_list) {
		final List<Item> origonal_item_list = new ArrayList<Item>(item_list);
		// raw_text_items_list => all the non-enclosed text items
		// grouped_items_list => list of all enclosures containing text items
		// remaining_item_list => whatever is left: mostly dots that form part
		// of lines/polylines/polygons

		AreaPolygon area_enclosing_polygon = new AreaPolygon(enclosing_polygon);

		List<AreaPolygon> area_enclosed_polygon_list = new ArrayList<AreaPolygon>();

		while (item_list.size() > 0) {
			Item item = item_list.remove(0); // shift

			if (item instanceof Text) {
				Text text = (Text) item;

				Collection<Item> items_in_nested_enclosure_coll = getItemsInNestedEnclosure(
						text, area_enclosing_polygon);
				List<Item> items_in_nested_enclosure = new ArrayList<Item>(
						items_in_nested_enclosure_coll); // Change to handling
															// it as a list

				if (items_in_nested_enclosure.size() == 0) {
					// if(text.getPixelBoundsUnion().x >=
					// this.getBoundingXLeft() && text.getPixelBoundsUnion().y
					// >= this.getBoundingYTop())
					// raw_text_item_list.add(text);
					// else remaining_item_list.add(text);
					raw_text_item_list.add(text);
				} else {

					// Something other than just this text-item is around

					while (items_in_nested_enclosure.size() > 0) {

						Item enclosure_item = items_in_nested_enclosure
								.remove(0); // shift

						Polygon enclosed_polygon = enclosure_item
								.getEnclosedShape();

						if (enclosed_polygon != null) {
							// Got a group
							// => Remove any items-in-nested-enclosure from:
							//
							// 'item_list' (so we don't waste our time
							// discovering and processing again these related
							// items)
							// and
							// 'raw_text_item_list' and 'remaining_item_lst' (as
							// we now know they are part of the nested enclosed
							// group)

							AreaPolygon area_enclosed_polygon = new AreaPolygon(
									enclosed_polygon);
							area_enclosed_polygon_list
									.add(area_enclosed_polygon);

							item_list.remove(enclosure_item);
							item_list.removeAll(items_in_nested_enclosure);

							raw_text_item_list.remove(enclosure_item);
							raw_text_item_list
									.removeAll(items_in_nested_enclosure);

							remaining_item_list.remove(enclosure_item);
							remaining_item_list
									.removeAll(items_in_nested_enclosure);

							// Now remove any of the just used enclosed-items if
							// they formed part of the perimeter
							List<Item> items_on_perimeter = new ArrayList<Item>();

							Iterator<Item> item_iterator = items_in_nested_enclosure
									.iterator();
							while (item_iterator.hasNext()) {
								Item item_to_check = item_iterator.next();
								Point pt_to_check = new Point(
										item_to_check.getX(),
										item_to_check.getY());

								if (area_enclosed_polygon
										.isPerimeterPoint(pt_to_check)) {
									items_on_perimeter.add(item_to_check);
								}
							}

							items_in_nested_enclosure
									.removeAll(items_on_perimeter);
						}

						else {
							// Text or single point (with no enclosure)

							// This item doesn't feature at this level
							// => will be subsequently capture by the group
							// polygon below
							// and processed by the recursive call

							item_list.remove(enclosure_item);
						}
					}

				}

			} // end of Text test
			else {
				// non-text item => add to remaining_item_list
				remaining_item_list.add(item);
			}

		}

		// Sort areas, smallest to largest
		Collections.sort(area_enclosed_polygon_list,
				new Comparator<AreaPolygon>() {

					public int compare(AreaPolygon ap1, AreaPolygon ap2) {
						Double ap1_area = ap1.getArea();
						Double ap2_area = ap2.getArea();

						return ap2_area.compareTo(ap1_area);
					}
				});

		// Remove any enclosed polygon areas that are completely contained in a
		// larger one

		for (int ri = 0; ri < area_enclosed_polygon_list.size(); ri++) {
			// ri = remove index pos

			AreaPolygon rpoly = area_enclosed_polygon_list.get(ri);

			for (int ci = ri + 1; ci < area_enclosed_polygon_list.size(); ci++) {
				// ci = check index pos
				AreaPolygon cpoly = area_enclosed_polygon_list.get(ci);
				if (rpoly.completelyContains(cpoly)) {
					area_enclosed_polygon_list.remove(ci);
					ri--; // to offset to the outside loop increment
					break;
				}
			}
		}

		// By this point, guaranteed that the remaining polygons are the largest
		// ones
		// that capture groups of items
		//
		// There may be sub-groupings within them, which is the reason for the
		// recursive call below

		for (AreaPolygon area_polygon : area_enclosed_polygon_list) {

			Collection<Item> enclosed_items = FrameUtils.getItemsEnclosedBy(
					frame, area_polygon);
			List<Item> enclosed_item_list = new ArrayList<Item>(enclosed_items);

			int i = 0;
			while (i < enclosed_item_list.size()) {
				Item enclosed_item = enclosed_item_list.get(i);

				// Filter out enclosed-items points that are part of the
				// polygon's perimeter
				if (area_polygon.isPerimeterPoint(enclosed_item.getPosition())) {
					enclosed_item_list.remove(i);
					// Don't include items the user hasn't asked us to.
				}
				// Only include items the user has asked to to include.
				else if (!origonal_item_list.contains(enclosed_item))
					enclosed_item_list.remove(i);
				else {
					i++;
				}
			}

			// Recursively work on the identified sub-group

			XGroupItem xgroup_item = new XGroupItem(frame, enclosed_item_list,
					area_polygon);

			grouped_item_list.add(xgroup_item);
		}
	}

	protected void castShadowIntoEmptySpace(
			YOverlappingItemsTopEdge y_item_span_top_edge, int yt, int yb) {
		// Assumes that all entries cast into are currently null
		// => Use the more general castShadow() below, if this is not guaranteed
		// to be the case

		YOverlappingItemsShadow y_span_shadow = new YOverlappingItemsShadow(
				y_item_span_top_edge);

		for (int y = yt; y <= yb; y++) {

			setYOverlappingItemsSpan(y, y_span_shadow);
		}
	}

	protected void castShadow(YOverlappingItemsTopEdge y_item_span_top_edge,
			int yt, int yb) {
		// Cast shadows only in places where there are currently no shadow
		// entries

		YOverlappingItemsShadow y_span_shadow = new YOverlappingItemsShadow(
				y_item_span_top_edge);

		int y = yt;

		while (y <= yb) {
			YOverlappingItemsSpan y_item_span = getYOverlappingItemsSpan(y);

			if (y_item_span == null) {
				setYOverlappingItemsSpan(y, y_span_shadow);
				y++;
			} else if (y_item_span instanceof YOverlappingItemsTopEdge) {
				// Our shadow has run into another top-edged zone
				// => need to extend the shadow to include this zone as well
				// (making this encountered top-edge obsolete)

				// Merge the newly encountered top-line in with the current one
				// and change all its shadow references to our (dominant) one

				XOrderedLine dominant_x_line = y_item_span_top_edge
						.getXOrderedLine();

				YOverlappingItemsTopEdge obsolete_top_edge = (YOverlappingItemsTopEdge) y_item_span;
				XOrderedLine obsolete_x_line = obsolete_top_edge
						.getXOrderedLine();

				for (XItem xitem : obsolete_x_line.getXItemList()) {

					dominant_x_line.orderedMergeItem(xitem);
				}

				while (y_item_span != null) {

					setYOverlappingItemsSpan(y, y_span_shadow);

					y++;

					if (y <= yb) {
						y_item_span = getYOverlappingItemsSpan(y);
					} else {
						y_item_span = null;
					}
				}
			} else {
				y++;
			}
		}
	}

	protected int autocropToTop(XItem xitem) {
		// top-edge over-spill can occur (and is acceptable) when (for example)
		// the given xitem is a raw-text item
		// that doesn't neatly fit in an enclosed area (i,e., is poking out the
		// top of an enclosing rectangle)
		// => cut down to the top of 'this' xgroupitem

		int yt = xitem.getBoundingYTop();

		yt = cropToTop(yt); // Only changes yt if in an over-spill situation

		return yt;

	}

	protected int autocropToBot(XItem xitem) {

		// similar to autocropToTop(), bottom-edge over-spill can occur (and is
		// acceptable) with
		// a less than precise placed raw-text item

		int yb = xitem.getBoundingYBot();

		yb = cropToBot(yb); // Only changes yb if in an over-spill situation

		return yb;
	}

	protected boolean yExtentIntersection(XGroupItem xgroup_item1,
			XGroupItem xgroup_item2) {
		// Computation applied to y-extent of each rectangle

		// To intersect, either a point in item1 falls inside item2,
		// or else item2 is completely within item1

		int yt1 = xgroup_item1.getBoundingYTop();
		int yb1 = xgroup_item1.getBoundingYBot();

		int yt2 = xgroup_item2.getBoundingYTop();

		// yt1 insides item2?
		if (xgroup_item2.containedInYExtent(yt1)) {
			return true;
		}

		// yb1 inside item2?
		if (xgroup_item2.containedInYExtent(yb1)) {
			return true;
		}

		// item2 completely inside item1?
		//
		// With the previous testing, this can be determined
		// by checking only one of the values is inside.
		// Doesn't matter which => choose yt2

		if (xgroup_item1.containedInYExtent(yt2)) {
			return true;
		}

		// Get to here if there is no intersection
		return false;

	}

	protected ArrayList<XGroupItem> calcXGroupYExtentIntersection(
			XGroupItem xgroup_pivot_item, List<XGroupItem> xgroup_item_list) {
		ArrayList<XGroupItem> intersect_list = new ArrayList<XGroupItem>();

		for (XGroupItem xgroup_item : xgroup_item_list) {

			if (xgroup_item == xgroup_pivot_item) {
				// Don't bother working out the intersection with itself!
				continue;
			}

			if (yExtentIntersection(xgroup_pivot_item, xgroup_item)) {
				intersect_list.add(xgroup_item);
			}
		}

		return intersect_list;
	}

	protected ArrayList<YOverlappingItemsTopEdge> calcYSpanOverlap(XItem xitem) {
		int yt = autocropToTop(xitem);
		int yb = autocropToBot(xitem);

		ArrayList<YOverlappingItemsTopEdge> top_edge_list = new ArrayList<YOverlappingItemsTopEdge>();

		for (int y = yt; y <= yb; y++) {

			YOverlappingItemsSpan item_span = getYOverlappingItemsSpan(y);

			if (item_span != null) {

				if (item_span instanceof YOverlappingItemsTopEdge) {

					YOverlappingItemsTopEdge y_item_span_top_edge = (YOverlappingItemsTopEdge) item_span;

					top_edge_list.add(y_item_span_top_edge);
				}
			}

		}

		return top_edge_list;
	}

	protected boolean multipleYSpanOverlap(XItem xitem) {
		return calcYSpanOverlap(xitem).size() > 0;
	}

	public void mapInItem(XItem xitem) {

		int yt = autocropToTop(xitem);
		int yb = autocropToBot(xitem);
		/*
		 * int yt = xitem.getBoundingYTop(); int yb = xitem.getBoundingYBot();
		 * 
		 * // top-edge over-spill can occur (and is acceptable) when (for
		 * example) the given xitem is a raw-text item // that doesn't neatly
		 * fit in an enclosed area (i,e., is poking out the top of an enclosing
		 * rectangle) yt = cropToTop(yt); // Only changes yt if in an over-spill
		 * situation
		 * 
		 * // similarly, bottom-edge over-spill can occur (and is acceptable)
		 * with a less than precise placed raw-text item yb = cropToBot(yb); //
		 * Only changes yb if in an over-spill situation
		 */

		// Merge into 'items_span' checking for overlaps (based on xitem's
		// y-span)

		boolean merged_item = false;
		for (int y = yt; y <= yb; y++) {

			YOverlappingItemsSpan item_span = getYOverlappingItemsSpan(y);

			if (item_span != null) {

				if (item_span instanceof YOverlappingItemsTopEdge) {

					// Hit a top edge of an existing item

					// Need to:
					// 1. *Required* Insert new item into current x-ordered line
					// 2. *Conditionally* Move entire x-ordered line up to
					// higher 'y' top edge
					// 3. *Required* Cast shadow for new item

					// Note for Step 2:
					// i) No need to do the move if y == yt
					// (the case when the new top edge is exactly the same
					// height as the existing one)
					// ii) If moving top-edge (the case when y > yt) then no
					// need to recalculate the top edge for existing shadows, as
					// this 'connection' is stored as a reference

					// Step 1: insert into existing top-edge

					YOverlappingItemsTopEdge y_item_span_top_edge = (YOverlappingItemsTopEdge) item_span;
					XOrderedLine xitem_span = y_item_span_top_edge
							.getXOrderedLine();

					xitem_span
							.orderedMergeItem(xitem.getBoundingXLeft(), xitem);

					// Step 2: if our new top-edge is higher than the existing
					// one, then need to move existing top-edge
					if (y > yt) {

						// Move to new position
						setYOverlappingItemsSpan(yt, y_item_span_top_edge);

						// Old position needs to become a shadow reference
						YOverlappingItemsShadow y_span_shadow = new YOverlappingItemsShadow(
								y_item_span_top_edge);
						setYOverlappingItemsSpan(y, y_span_shadow);
					}

					// Step 3: Cast shadow
					castShadow(y_item_span_top_edge, yt + 1, yb);

				} else {
					// Top edge to our new item has hit a shadow entry (straight
					// off)
					// => Look up what the shadow references, and then add in to
					// that

					// Effectively after the shadow reference lookup this is the
					// same
					// as the above, without the need to worry about Step 2 (as
					// no move is needed)

					YOverlappingItemsShadow y_item_span_shadow = (YOverlappingItemsShadow) item_span;
					YOverlappingItemsTopEdge y_item_span_top_edge = y_item_span_shadow
							.getTopEdge();

					XOrderedLine xitem_span = y_item_span_top_edge
							.getXOrderedLine();

					// merge with this item list, preserving x ordering
					xitem_span
							.orderedMergeItem(xitem.getBoundingXLeft(), xitem);

					// Now Cast shadow
					castShadow(y_item_span_top_edge, yt + 1, yb);
				}

				merged_item = true;
				break;

			}

		}

		// Having checked all the y-location's ('yt' to 'yb') of this x-item, if
		// all y-span entries were found to be null
		// => 'merged_item' is still false

		if (!merged_item) {
			// xitem didn't intersect with any existing y-spans
			// => simple case for add (i.e. all entries affected by map are
			// currently null)

			// Start up a new x-ordered-line (consisting of only 'xitem'), add
			// in top edge and cast shadow

			XOrderedLine xitem_line = new XOrderedLine(xitem);

			YOverlappingItemsTopEdge y_item_span_top_edge = new YOverlappingItemsTopEdge(
					xitem_line);

			setYOverlappingItemsSpan(yt, y_item_span_top_edge);

			castShadowIntoEmptySpace(y_item_span_top_edge, yt + 1, yb);
		}

	}

	private ArrayList<DimensionExtent> calcXGroupXGaps(
			XGroupItem xgroup_pivot_item, List<XGroupItem> xgroup_item_list) {
		// 'xgroup_pivot_item' current not used!!

		int enclosing_xl = getBoundingXLeft();
		int enclosing_xr = getBoundingXRight();

		int enclosing_x_dim = enclosing_xr - enclosing_xl + 1;
		boolean is_x_shadow[] = new boolean[enclosing_x_dim]; // defaults all
																// values to
																// false

		ArrayList<DimensionExtent> x_gap_list = new ArrayList<DimensionExtent>();

		for (XGroupItem xgroup_item : xgroup_item_list) {
			int xl = xgroup_item.getBoundingXLeft();
			int xr = xgroup_item.getBoundingXRight();

			for (int x = xl; x <= xr; x++) {
				int x_offset = x - enclosing_xl;
				is_x_shadow[x_offset] = true;
			}
		}

		// New find where the contiguous runs of 'false' are, as they point to
		// the gaps

		int x = 1;
		int start_x_gap = enclosing_xl;
		boolean in_gap = true;

		while (x < is_x_shadow.length) {
			boolean prev_is_shadow = is_x_shadow[x - 1];
			boolean this_is_shadow = is_x_shadow[x];

			if (prev_is_shadow && !this_is_shadow) {
				// reached end of shadow, record start of a gap
				start_x_gap = enclosing_xl + x;
				in_gap = true;
			} else if (!prev_is_shadow && this_is_shadow) {
				// reached the end of a gap, record it
				int end_x_gap = enclosing_xl + x - 1;
				DimensionExtent de = new DimensionExtent(start_x_gap, end_x_gap);
				x_gap_list.add(de);
				in_gap = false;
			}
			x++;
		}

		if (in_gap) {
			int end_x_gap = enclosing_xl + x - 1;
			DimensionExtent de = new DimensionExtent(start_x_gap, end_x_gap);
			x_gap_list.add(de);
		}

		return x_gap_list;
	}

	protected void boxMultipleXRawItemsInGaps(XGroupItem xgroup_pivot_item,
			ArrayList<DimensionExtent> x_text_gap_list) {
		ArrayList<YOverlappingItemsTopEdge> y_span_overlap = calcYSpanOverlap(xgroup_pivot_item);

		// work out where continuous runs of raw-text items intersect with the
		// gaps occurring between boxed items

		// foreach x-gap,
		// foreach y-span's list of x-sorted objects,
		// => see how many raw-text items fit into it

		ArrayList<ArrayList<XRawItem>> implicit_box_list = new ArrayList<ArrayList<XRawItem>>();

		for (DimensionExtent de_gap : x_text_gap_list) {

			int deg_xl = de_gap.min;
			int deg_xr = de_gap.max;

			ArrayList<XRawItem> grouped_raw_text_list = new ArrayList<XRawItem>();

			int y_span_line_count = 0;

			for (YOverlappingItemsTopEdge y_top_edge : y_span_overlap) {

				List<XItem> x_item_list = y_top_edge.x_items.getXItemList();

				boolean used_x_raw_text = false;

				for (XItem x_item : x_item_list) {

					if (x_item instanceof XRawItem) {
						XRawItem x_raw_item = (XRawItem) x_item;

						int xri_xl = x_raw_item.getBoundingXLeft();
						int xri_xr = x_raw_item.getBoundingXRight();

						if ((xri_xl >= deg_xl) && (xri_xr <= deg_xr)) {
							grouped_raw_text_list.add(x_raw_item);
							used_x_raw_text = true;
						}
					}
				}

				if (used_x_raw_text) {
					y_span_line_count++;
				}
			}

			// Only interested in implicitly boxing if the formed group is used
			// over 2 or more y-span lines
			if (y_span_line_count >= 2) {
				implicit_box_list.add(grouped_raw_text_list);
			}
		}

		// System.err.println("*** Number of implicit groups found: " +
		// implicit_box_list.size());

		for (ArrayList<XRawItem> implicit_x_item_list : implicit_box_list) {

			for (YOverlappingItemsTopEdge y_top_edge : y_span_overlap) {

				List<XItem> yspan_x_item_list = y_top_edge.x_items
						.getXItemList();

				// Remove all the XRawItems from the current y-span
				yspan_x_item_list.removeAll(implicit_x_item_list);

			}

			// Create a list of original Text items
			// (OK that they are not y-ordered)
			ArrayList<Item> implicit_item_list = new ArrayList<Item>();
			for (XRawItem x_raw_item : implicit_x_item_list) {
				Item item = (Text) x_raw_item.item;
				implicit_item_list.add(item);
			}

			// Now insert them as their own boxed up items
			XGroupItem implicit_xgroup = new XGroupItem(frame,
					implicit_item_list);
			this.mapInItem(implicit_xgroup);
		}

	}

	protected void implicitBoxing(XGroupItem xgroup_pivot_item,
			List<XGroupItem> xgroup_item_list) {

		// Implicit boxing is needed if there are two or more raw-text items
		// that overlap in y-span-map of the given xgroup_item

		// First establish if there is any kind of multiple overlap
		if (multipleYSpanOverlap(xgroup_pivot_item)) {

			// Overlap could be with other boxed items, so need to investigate
			// further

			// Do this by working out if there are any raw-text items lurking
			// between the pivot xgroup_item
			// and the list of other xgroup_items

			// Step 1: Get list intersection (y-dim) of this group item, with
			// any
			// other group items in xgroup_item_list
			ArrayList<XGroupItem> y_overlapping_xgroup_item_list = calcXGroupYExtentIntersection(
					xgroup_pivot_item, xgroup_item_list);

			// Step 2: Work out where the gaps are between the y-overlapping
			// boxes in the x-dimension
			ArrayList<DimensionExtent> x_text_gap_list = calcXGroupXGaps(
					xgroup_pivot_item, y_overlapping_xgroup_item_list);

			// Step 3: Remove any sequences of raw-text items that fall in the
			// gaps from YSpan, box them up, and reinsert
			boxMultipleXRawItemsInGaps(xgroup_pivot_item, x_text_gap_list);

		}
	}

	public void mapInXGroupItemsRecursive(List<XGroupItem> xgroup_item_list) {

		// Map in all the items in the given list:
		for (XGroupItem xgroup_item : xgroup_item_list) {
			if (!xgroup_item.isOriginalOutOfFlowItem()) {

				// See if any implicit boxing of raw-text items is needed
				if (doImplicitBoxing) {
					implicitBoxing(xgroup_item, xgroup_item_list);
				}
				mapInItem(xgroup_item); // Map in this x-group-item
			}
		}

		// Now recursively work through each item's nested x-groups
		for (XGroupItem xgroup_item : xgroup_item_list) {

			List<XGroupItem> nested_xgroup_item_list = xgroup_item
					.getGroupedItemList();

			if (nested_xgroup_item_list.size() > 0) {
				xgroup_item.mapInXGroupItemsRecursive(nested_xgroup_item_list);
			}
		}
	}

	public List<Item[]> getYXOverlappingItemListLines() {
		final List<XRawItem> yxOverlappingXRawItemList = getYXOverlappingXRawItemList(true);
		final List<Item[]> lines = new ArrayList<Item[]>();
		final List<XRawItem> currentLineXItem = new ArrayList<XRawItem>();
		for (final XRawItem xitem : yxOverlappingXRawItemList) {
			if (xitem.getItem() == GROUPSEP_START)
				continue;
			else if (xitem.getItem() == GROUPSEP_END) {
				if (currentLineXItem.isEmpty())
					continue;
				else {
					final List<Item> ln = new ArrayList<Item>();
					for (final XRawItem xrawitem : currentLineXItem)
						ln.add(xrawitem.getItem());
					lines.add(ln.toArray(new Item[] {}));
					currentLineXItem.clear();
				}
			} else {
				if (currentLineXItem.isEmpty())
					currentLineXItem.add(xitem);
				else {
					final YOverlappingItemsSpan[] itemSpanArray = xitem
							.getGroup().yitems_span_array;
					final int index = xitem.getBoundingYTop()
							- xitem.getGroup().getBoundingYTop();
					final YOverlappingItemsSpan itemSpan = itemSpanArray[index];
					final List<XItem> xOrderedList = itemSpan instanceof YOverlappingItemsTopEdge ? ((YOverlappingItemsTopEdge) itemSpan)
							.getXOrderedLine().getXItemList()
							: ((YOverlappingItemsShadow) itemSpan).getTopEdge()
									.getXOrderedLine().getXItemList();
					if (!xOrderedList.contains(currentLineXItem.get(0))) {
						if (!currentLineXItem.isEmpty()) {
							final List<Item> ln = new ArrayList<Item>();
							for (final XRawItem xrawitem : currentLineXItem)
								ln.add(xrawitem.getItem());
							lines.add(ln.toArray(new Item[] {}));
							currentLineXItem.clear();
						}
						currentLineXItem.add(xitem);
					} else {
						currentLineXItem.add(xitem);
					}
				}
			}
		}
		if (!currentLineXItem.isEmpty()) {
			final List<Item> ln = new ArrayList<Item>();
			for (final XRawItem xrawitem : currentLineXItem)
				ln.add(xrawitem.getItem());
			lines.add(ln.toArray(new Item[] {}));
			currentLineXItem.clear();
		}
		return lines;
	}

	public ArrayList<Item> getYXOverlappingItemList(boolean separateGroups) {
		final List<XRawItem> yxOverlappingXItemList = getYXOverlappingXRawItemList(separateGroups);
		final ArrayList<Item> yxOverlappingItemList = new ArrayList<Item>();
		for (final XRawItem xitem : yxOverlappingXItemList)
			yxOverlappingItemList.add(xitem.getItem());
		return yxOverlappingItemList;
	}

	public List<XRawItem> getYXOverlappingXRawItemList(
			final boolean separateGroups) {
		ArrayList<XRawItem> overlapping_y_ordered_items = new ArrayList<XRawItem>();

		for (int y = 0; y < y_span_height; y++) {

			YOverlappingItemsSpan item_span = yitems_span_array[y];

			if (item_span != null) {

				if (item_span instanceof YOverlappingItemsTopEdge) {

					YOverlappingItemsTopEdge item_span_top_edge = (YOverlappingItemsTopEdge) item_span;
					XOrderedLine xitem_line = item_span_top_edge
							.getXOrderedLine();

					for (XItem xspan : xitem_line.getXItemList()) {
						if (xspan instanceof XRawItem) {
							XRawItem xitem_span = (XRawItem) xspan;
							// Item item = xitem_span.getItem();
							//
							// overlapping_y_ordered_items.add(item);
							overlapping_y_ordered_items.add(xitem_span);
						} else {
							// Must be an XGroupItem => recursive call on xspan
							// item

							XGroupItem nested_group_item = (XGroupItem) xspan;
							List<XRawItem> nested_overlapping_items = nested_group_item
									.getYXOverlappingXRawItemList(separateGroups);
							if (separateGroups) {
								overlapping_y_ordered_items.add(new XRawItem(
										GROUPSEP_START, this));
							}
							overlapping_y_ordered_items
									.addAll(nested_overlapping_items);
							if (separateGroups) {
								overlapping_y_ordered_items.add(new XRawItem(
										GROUPSEP_END, this));
							}
						}
					}
				}
			}
		}

		return overlapping_y_ordered_items;
	}

	public ArrayList<Item> getYXOverlappingItemList() {
		return getYXOverlappingItemList(false);
	}

	public String toString() {
		return "XGroupItem";
	}
}
