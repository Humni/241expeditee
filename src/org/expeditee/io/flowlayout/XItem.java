/**
 *    XItem.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.io.flowlayout;

import java.awt.Point;
import java.awt.Rectangle;

import org.expeditee.items.Item;

public abstract class XItem 
{
	protected Rectangle bounding_rect;
		
	protected XItem()
	{
		bounding_rect = null;
	}
	
	protected XItem(Rectangle imprint_bounding_rect)
	{
		this.bounding_rect = imprint_bounding_rect;
	}
	
	public int getX()
	{
		return bounding_rect.x;
	}
	
	public int getBoundingXLeft()
	{
		// name alias for getX()
		return bounding_rect.x;
	}
	
	public int getY()
	{
		return bounding_rect.y;
	}
	
	public int getBoundingYTop()
	{
		// name alias for getY()
		return bounding_rect.y;
	}
	
	public int getBoundingYBot()
	{
		return bounding_rect.y + bounding_rect.height -1;
	}
	
	public int getBoundingXRight()
	{
		return bounding_rect.x + bounding_rect.width -1;
	}
	
	public int getBoundingWidth()
	{
		return bounding_rect.width;
	}
	
	public int getBoundingHeight()
	{
		return bounding_rect.height;
	}
	
	public Rectangle getBoundingRect()
	{
		return bounding_rect;
	}
	
	public Rectangle setBoundingRect(Rectangle rect)
	{
		return bounding_rect = rect;
	}
	
	public boolean containsItem(Item item)
	{
		int x = item.getX();
		int y = item.getY();
		
		Point pt = new Point(x,y);
		
		return bounding_rect.contains(pt);
	}
	
	public boolean containedInXExtent(int x) 
	{
		int xl = getBoundingXLeft();
		int xr = getBoundingXRight();
		
		return (x>= xl) && (x<=xr);
	}
	
	
	public boolean containedInYExtent(int y) 
	{
		int yt = getBoundingYTop();
		int yb = getBoundingYBot();
		
		return (y>= yt) && (y<=yb);
	}
	
	
}
