/**
 *    AreaPolygon.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.io.flowlayout;

import java.awt.Point;
import java.awt.Polygon;

public class AreaPolygon extends Polygon 
{
	
	private static final long serialVersionUID = 1863373131107377026L;
	
	double area;
	
	public AreaPolygon(Polygon polygon)
	{
		super(polygon.xpoints,polygon.ypoints,polygon.npoints);
		area = calcArea();
	}
	
	protected double calcArea() 
	{
		int n = npoints;
		int[] x_pts = xpoints;
		int[] y_pts = ypoints;
		
		double area = 0;
		
		for (int i = 0; i < n; i++) {
			int j = (i + 1) % n;
			area += x_pts[i] * y_pts[j];
			area -= x_pts[j] * y_pts[i];
		}
		
		area /= 2.0;
		
		return Math.abs(area);
	}

	public double getArea()
	{
		return area;
	}
	
	public boolean completelyContains(Polygon p)
	{
		boolean inside = true;
		
		int p_n = p.npoints;
		int[] p_x = p.xpoints;
		int[] p_y = p.ypoints;
		
		for (int i=0; i<p_n; i++) {
			if (!contains(p_x[i],p_y[i])) {
				inside = false;
				break;
			}
		}
		
		return inside;
	}
	
	public boolean isPerimeterPoint(Point pt)
	{	
		int n = npoints;
		int[] poly_x_pts = xpoints;
		int[] poly_y_pts = ypoints;
		
		boolean isVertex = false;
		
		
		for (int i = 0; i < n; i++) {
			if ((poly_x_pts[i]==pt.x) && (poly_y_pts[i]==pt.y)) {
				isVertex = true;
				break;
			}
		}
		
		return isVertex;
	}
	
}
