/**
 *    ProxyAuth.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.io;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

public class ProxyAuth extends Authenticator {

	private String httpUser = null, httpPass = null, httpsUser = null, httpsPass = null;
	private int attempts = 0;
	
	public ProxyAuth() {
	}
	
	@Override
	public PasswordAuthentication getPasswordAuthentication() {
		// TODO: differentiate between HTTP and HTTPS proxies somehow
		// currently just chooses whichever one is set, preferentially http
		System.out.println("Authenticating");
		// stop it from breaking from a redirect loop
		attempts++;
		if(attempts > 5) {
			return null;
		}
		String user = httpUser != null ? httpUser : httpsUser;
		char[] pass = httpPass != null ? httpPass.toCharArray() : (httpsPass != null ? httpsPass.toCharArray() : null);
		if(user == null || user.length() == 0 || pass == null || pass.length == 0) {
			return null;
		}
		return new PasswordAuthentication(user, pass);
	}
	
	public void setup(String user, String pass) {
		// System.out.println("setup proxy");
		this.httpUser = user;
		this.httpPass = pass;
		this.httpsUser = user;
		this.httpsPass = pass;
		attempts = 0;
	}
}
