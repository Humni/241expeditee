/**
 *    TEXWriter.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.io;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.expeditee.gui.Frame;
import org.expeditee.items.Item;
import org.expeditee.items.ItemUtils;
import org.expeditee.items.Text;

public class TEXWriter extends DefaultTreeWriter {

	// may be needed for sectioning commands
	private Text _title = null;

	@Override
	protected void initialise(Frame start, Writer writer) throws IOException {
		_format = ".tex";
		super.initialise(start, writer);
	}

	protected void writeTitle(Text title, List<Item> items) throws IOException {
		_title = title;

		Text text = ItemUtils.FindTag(items, "@TexSection");

		if (text != null) {
			String first = text.getFirstLine();
			int ind = first.indexOf(":");

			if (ind > 0) {
				String command = first.substring(ind + 1).trim().toLowerCase();

				_writer.write("\\" + command + "{");

				List<String> titleLines = _title.getTextList();
				for (int i = 0; i < titleLines.size() - 1; i++) {
					_writer.write(titleLines.get(i));
					_writer.write(ItemWriter.NEW_LINE);
				}

				_writer.write(titleLines.get(titleLines.size() - 1));

				_writer.write("}" + ItemWriter.NEW_LINE);
			}
		}
	}

	@Override
	protected void writeStartLink(Item linker) throws IOException {
		// only output text items
		if (!(linker instanceof Text))
			return;

		Text text = (Text) linker;
		List<String> toWrite = text.getTextList();

		String first = toWrite.get(0);
		TexEnvironment te = new TexEnvironment(first);

		if (te.isValid()) {
			if (te.hasComment())
				_writer.write("%" + te.getComment() + ItemWriter.NEW_LINE);
			_writer.write("\\begin{" + te.getName() + "}" + ItemWriter.NEW_LINE);
		}
	}
	
	@Override
	protected void writeEndLink(Item linker) throws IOException {
		// only output text items
		if (!(linker instanceof Text))
			return;

		Text text = (Text) linker;
		List<String> toWrite = text.getTextList();

		String first = toWrite.get(0);
		TexEnvironment te = new TexEnvironment(first);

		if (te.isValid()) {
			_writer.write("\\end{" + te.getName() + "}" + ItemWriter.NEW_LINE);
		}

	}

	@Override
	protected void writeText(Text text) throws IOException {
		for (String s : text.getTextList()) {
			_writer.write(s);
			_writer.write(ItemWriter.NEW_LINE);
		}
	}

	@Override
	protected void writeAnnotationText(Text text) throws IOException {

	}
}

