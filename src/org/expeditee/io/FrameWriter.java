/**
 *    FrameWriter.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.io;

import java.io.IOException;
import java.io.Writer;

import org.expeditee.gui.Frame;

public interface FrameWriter {

	public void setOutputLocation(String filename);

	public String writeFrame(Frame toWrite) throws IOException;

	/**
	 * Methods for writing out frames to file as well as with other writers such
	 * as StringWriter.
	 * 
	 * @param toWrite
	 * @param writer
	 * @return
	 * @throws IOException
	 */
	public String writeFrame(Frame toWrite, Writer writer) throws IOException;

	public void stop();

	public boolean isRunning();

	public String getFileContents();
}
