/**
 *    TexEnvironment.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.io;

public class TexEnvironment {
	private String _comment = "";

	private String _name = "";

	public TexEnvironment(String s) {
		int colonPos = s.indexOf(':');

		if (colonPos > 0) {
			_name = s.substring(0, colonPos).trim();
			if (s.length() > colonPos + 1)
				_comment = s.substring(colonPos + 1).trim();

			for (int i = 0; i < _name.length(); i++) {
				if (!Character.isLetter(_name.charAt(i))) {
					_name = "";
				}
			}
		}
	}

	public boolean isValid() {
		return _name != "";
	}

	public String getComment() {
		return _comment;
	}

	public String getName() {
		return _name;
	}

	public boolean hasComment() {
		return _comment != "";
	}
}
