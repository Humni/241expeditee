/**
 *    DefaultFramesetWriter.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.io;

import java.io.IOException;

import org.expeditee.gui.Frame;
import org.expeditee.gui.FrameIO;
import org.expeditee.gui.MessageBay;

public class DefaultFramesetWriter extends DefaultFrameWriter {
	protected long  _firstFrame = 1;
	protected long _maxFrame = Long.MAX_VALUE;
	
	protected DefaultFramesetWriter(long firstFrame, long maxFrame){
		_firstFrame = firstFrame;
		_maxFrame = maxFrame;
	}
	
	@Override
	protected void outputFrame(Frame toWrite) throws IOException {
		String framesetName = toWrite.getFramesetName();
		
		_maxFrame = Math.min(_maxFrame, FrameIO.getLastNumber(framesetName));
		
		for (long i = _firstFrame; i <= _maxFrame; i++) {
			if (_stop) {
				break;
			}
			String frameName = framesetName + i;
			Frame nextFrame = FrameIO.LoadFrame(frameName);
			if (nextFrame != null) {
				MessageBay.overwriteMessage("Processing " + frameName);
				super.outputFrame(nextFrame);
			}
		}
	}
	
	@Override
	protected String finaliseFrame() throws IOException {
		return "Frameset" + finalise();
	}
}
