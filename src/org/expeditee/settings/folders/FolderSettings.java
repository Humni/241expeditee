/**
 *    FolderSettings.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.settings.folders;

import org.expeditee.gui.AttributeValuePair;
import org.expeditee.gui.FrameIO;
import org.expeditee.gui.FrameUtils;
import org.expeditee.items.Text;
import org.expeditee.setting.ListSetting;
import org.expeditee.setting.Setting;
import org.expeditee.settings.UserSettings;

public class FolderSettings {

	public static final ListSetting<String> FrameDirs = new ListSetting<String>("Directories to look in for frames") {
		@Override
		public boolean setSetting(Text text) {
			_value.addAll(FrameUtils.getDirs(text));
			return true;
		}
	};
	public static final Setting FramesetDir = new Setting("Adds a directory to look in for frames") {
		@Override
		public boolean setSetting(Text text) {
			if(text.getText().indexOf(':') == -1) {
				return false;
    		}
    		AttributeValuePair avp = new AttributeValuePair(text.getText());
    		if(avp.getValue().trim().length() != 0) {
    			String dir = FrameUtils.getDir(avp.getValue());
    			if (dir != null) {
    				FolderSettings.FrameDirs.get().add(dir);
    				return true;
    			}
    		}
    		return false;
		}
	};

	public static ListSetting<String> ImageDirs = new ListSetting<String>("Directories to look in for images") {
		@Override
		public boolean setSetting(Text text) {
			_value.addAll(FrameUtils.getDirs(text));
			return true;
		}
	};
	public static final Setting ImageDir = new Setting("Adds a directory to look in for images") {
		@Override
		public boolean setSetting(Text text) {
    		if(text.getText().indexOf(':') == -1) {
    			return false;
    		}
    		AttributeValuePair avp = new AttributeValuePair(text.getText());
    		if(avp.getValue().trim().length() != 0) {
    			String dir = FrameUtils.getDir(avp.getValue());
    			if(dir != null) {
    				FolderSettings.ImageDirs.get().add(0, dir);
    				return true;
    			}
    		}
    		return false;
		}
	};
	
	public static final Setting LogDir = new Setting("Set the directory to save logs") {
		@Override
		public boolean setSetting(Text text) {
			if(text.getText().indexOf(':') == -1) {
    			return false;
    		}
    		AttributeValuePair avp = new AttributeValuePair(text.getText());
    		if(avp.getValue().trim().length() != 0) {
    			FrameIO.LOGS_DIR = FrameUtils.getDir(avp.getValue());
    		}
    		return true;
		}
	};
}
