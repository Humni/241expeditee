/**
 *    NetworkSettings.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.settings.network;


import org.expeditee.gui.Frame;
import org.expeditee.items.Text;
import org.expeditee.setting.FrameSetting;
import org.expeditee.setting.IntegerSetting;
import org.expeditee.setting.StringSetting;

public abstract class NetworkSettings {
	
	// The URL to prepend to web searches
	public static final StringSetting SearchEngine = new StringSetting("The search engine for the JfxBrowser", "https://duckduckgo.com/?q=");
	
	public static final StringSetting HomePage = new StringSetting("The home page for the JfxBrowser", "https://duckduckgo.com");
	
	public static final IntegerSetting FrameShareTimeout = new IntegerSetting("Timeout for FrameShare socket, in milliseconds", 1000);
	
	public static final FrameSetting FrameShare = new FrameSetting("Enable accessing remote frames") {
		@Override
		public void run(Frame frame) {
			org.expeditee.network.FrameShare.init(frame);
		}
	};
	
	public static void onParsed(Text text) {
		// System.out.println("network parsed");
	}
}
