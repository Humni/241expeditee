/**
 *    ExploratorySearchSettings.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.settings.exploratorysearch;

import org.expeditee.items.Text;
import org.expeditee.setting.BooleanSetting;
import org.expeditee.setting.IntegerSetting;
import org.expeditee.setting.StringSetting;

/**
 * The settings for the exploratory search user interface.
 * @author csl14
 */
public abstract class ExploratorySearchSettings {
	
	// Horz and Vert offset for the JfxBrowser's position to work with the Exploratory Search web browser overlay
	public static final int BROWSER_HORZ_OFFSET = 140;
	public static final int BROWSER_VERT_OFFSET = 50;
	
	public static final BooleanSetting BrowserFullScreen = new BooleanSetting("Start Exploratory Search browser in fullscreen", true);
	
	public static final IntegerSetting BrowserDefaultWidth = new IntegerSetting("Default Browser width", 800);
	
	public static final IntegerSetting BrowserDefaultHeight = new IntegerSetting("Default Browser height", 600);
	
	public static final IntegerSetting BrowserLeftMargin = new IntegerSetting("Size of left hand margin for Browser", 0);
	
	public static final IntegerSetting BrowserRightMargin = new IntegerSetting("Size of right hand margin for Browser", 0);
	
	public static final IntegerSetting BrowserTopMargin = new IntegerSetting("Size of Top margin for Browser", 0);
	
	public static final IntegerSetting BrowserBottomMargin = new IntegerSetting("Size of bottom margin for Browser", 0);
}