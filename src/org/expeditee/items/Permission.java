/**
 *    Permission.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.items;

public enum Permission {
	none, followLinks, copy, createFrames, full;

	/**
	 * Converts the given Expeditee permission code into a int corresponding to
	 * the constants defined in Item.
	 * 
	 * @param permissionCode
	 *            The Expeditee permission code to convert
	 * @return The resulting int corresponding to one of the constants defined
	 *         in Item
	 */
	public static Permission convertString(String permissionCode) {
		return getPermission(permissionCode, Permission.full);
	}

	public static Permission getPermission(String permissionCode,
			Permission defaultPermission) {
		if (permissionCode == null)
			return defaultPermission;

		permissionCode = permissionCode.trim().toLowerCase();
		if (permissionCode.length() == 0)
			return defaultPermission;

		// if it is a single char just match the first character
		try {
			return values()[Integer.parseInt(permissionCode)];
			// Otherwise match the whole string
		} catch (Exception ex) {
			try {
				return valueOf(permissionCode);
			} catch (Exception e) {
			}
		}

		// default permission
		return defaultPermission;
	}

	public int getCode() {
		return ordinal();
	}

	public String toString() {
		return this.name();
	}

	public static Permission min(Permission p1, Permission p2) {
		return p1.ordinal() < p2.ordinal() ? p1 : p2;
	}

	public static Permission max(Permission p1, Permission p2) {
		return p1.ordinal() > p2.ordinal() ? p1 : p2;
	}
}
