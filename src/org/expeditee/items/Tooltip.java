package org.expeditee.items;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.expeditee.settings.templates.TemplateSettings;

public class Tooltip {
	
	Tooltip() { }
	
	private Map<Text, Item> tooltipItems = new HashMap<Text, Item>();
	
	public Text addTooltip(String content, final Item toItem) {
		if(content.trim().toLowerCase().startsWith("text") && content.contains(":")) 
			content = content.substring(content.indexOf(':') + 1);
		final Text tooltip = TemplateSettings.TooltipTemplate.get().copy();
		tooltip.setText(content);
		return addTooltip(tooltip, toItem);
	}
	
	public Text addTooltip(final Text tooltip, final Item toItem) {
		tooltipItems.put(tooltip, toItem);
		return tooltip;
	}
	
	public Collection<Text> getTooltips() { return tooltipItems.keySet(); }
	
	public List<String> asStringList() { 
		final List<String> ret = new LinkedList<String>();
		for(final Text tooltip: tooltipItems.keySet()) ret.add(tooltip.getText());
		return ret;
	}
	
	public int getWidth() {
		int max = 0;
		for(final Text tooltip: tooltipItems.keySet()) 
			if(tooltip.getBoundsWidth() > max) max = tooltip.getBoundsWidth();
		return max;
	}

	public int getHeight() {
		int max = 0;
		for(final Text tooltip: tooltipItems.keySet()) 
			if(tooltip.getBoundsWidth() > max) max = tooltip.getBoundsHeight();
		return max;
	}
	
	public int getCollectiveHeight() {
		int height = 0;
		for(final Text tooltip: tooltipItems.keySet()) height += tooltip.getBoundsHeight();
		return height;
	}
}
