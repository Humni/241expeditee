/**
 *    DataFrameWidget1.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.items.widgets;

import javax.swing.JComboBox;

import org.expeditee.gui.Frame;
import org.expeditee.items.Text;

public class DataFrameWidget1 extends DataFrameWidget {

	static String testItems[] = new String[] { "dog", "fish", "cat", "pig" };

	private JComboBox _combo;

	public DataFrameWidget1(Text source, String[] args) {
		super(source, new JComboBox(testItems), 200, 200, 50, 50);
		_combo = (JComboBox) super._swingComponent;

		refresh();
	}

	@Override
	protected String[] getArgs() {
		String[] stateArgs = new String[1];
		stateArgs[0] = Integer.toString(_combo.getSelectedIndex());
		return stateArgs;
	}

	@Override
	public void refresh() {
		super.refresh();
		Frame frame = getDataFrame();
		if (frame != null) {
			_combo.removeAllItems();
			for (Text text : frame.getBodyTextItems(false)) {
				_combo.addItem(text.getText());
			}
			if (_combo.getItemCount() > 0)
				_combo.setSelectedIndex(0);
		}
	}

	@Override
	public float getMinimumBorderThickness() {
		return 0.0F;
	}
}
