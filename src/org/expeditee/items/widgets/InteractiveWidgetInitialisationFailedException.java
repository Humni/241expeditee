/**
 *    InteractiveWidgetInitialisationFailedException.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.items.widgets;

/**
 * Thrown if a widget fails to be instantiated. 
 * Must encapsulate all exceptions in constructor with this exception for all types of 
 * InteractiveWidgets
 * @author Brook Novak
 */
public class InteractiveWidgetInitialisationFailedException extends Exception {

		protected static final long serialVersionUID = 0L;
		
		public InteractiveWidgetInitialisationFailedException() {
			super();
		}
		
		public InteractiveWidgetInitialisationFailedException(String message) {
			super(message);
		}
		
		public InteractiveWidgetInitialisationFailedException(String message, Exception inner) {
			super(message, inner);
		}
	}
