package org.expeditee.items.widgets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.expeditee.gui.DisplayIO;
import org.expeditee.gui.Frame;
import org.expeditee.gui.FrameGraphics;
import org.expeditee.gui.FrameIO;
import org.expeditee.items.Text;

//Creates a button that when pressed, restores the previous action
public class RefreshButton extends ButtonWidget{

	public RefreshButton(Text source, String[] args){		
				
		super(78, "org/expeditee/assets/images/reset.svg", source, args);
		
		clicked_.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent e) {	        	
	         
	           Frame f = DisplayIO.getCurrentFrame();	  
	           try{
	        	   FrameIO.ForceSaveFrame(f);
	        	   Frame g = FrameIO.LoadRestoreFrame(f);	
	        	   DisplayIO.setCurrentFrame(g, false);
		           FrameGraphics.refresh(true);
	           }
	           catch(Exception e1){
	        	   
	        	   e1.printStackTrace();
	           }         
	              
	        }         
	     });		
	}
	
	@Override
	protected String[] getArgs() {
		// TODO Auto-generated method stub
		return null;
	}	
	
}
