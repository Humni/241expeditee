package org.expeditee.items.widgets;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.expeditee.items.Text;

import com.kitfox.svg.app.beans.SVGIcon;

//Will be the default class for Rubbish Bin, Reset and Undo Button Widgets
public class ButtonWidget extends InteractiveWidget{

	protected JButton clicked_ = new JButton("");	
	protected SVGIcon icon_ = null;
	
	public ButtonWidget(int dimension, String filePath, Text source, String[] args)
	{
		//Controls how the widget is displayed
		super(source, new JPanel(new BorderLayout()), 80, -1, 80, -1);	
				
		clicked_.setBorder(new EmptyBorder(0, 0, 0, 0));
		clicked_.setBackground(Color.white);
		
		Dimension size = new Dimension(dimension, dimension);
		icon_ = new SVGIcon();
		try{
			URL imageURL = ClassLoader.getSystemResource(filePath);		
			icon_.setSvgURI(imageURL.toURI());
			//This STILL works, unsure why. Don't remove it has an affect on the button appearance.		
			icon_.setUseAntiAlias(true);
			icon_.setScaleToFit(true);
			icon_.setPreferredSize(size);
			clicked_.setIcon(icon_);
			_swingComponent.add(clicked_);
			this.setWidgetEdgeThickness(0.0f);
			this.setWidgetEdgeColor(Color.white);	
		}
		catch(Exception e){
			
			//Set a default image if image is missing
			System.out.println("Unable to load image from directory");			
		}
	}

	@Override
	protected String[] getArgs() {
		// TODO Auto-generated method stub
		return null;
	}
	public boolean itemHeldWhileClicked(InteractiveWidget bw) {
		// TODO Auto-generated method stub
		return false;
	}
	public boolean getdropInteractableStatus(){
		return false;
	}

	@Override
	boolean setPositions(WidgetCorner src, float x, float y) {
		boolean result = super.setPositions(src, x, y);

		int iw_x_dim = getWidth();
		int iw_y_dim = getHeight();

		icon_.setPreferredSize(new Dimension(iw_x_dim, iw_y_dim));

		return result;
	}
}
