package org.expeditee.items.widgets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.expeditee.gui.DisplayIO;
import org.expeditee.items.Text;

//Creates a button that when pressed, restores the previous action
public class Undo extends ButtonWidget{

	public Undo(Text source, String[] args){		
		
		super(120, "org/expeditee/assets/images/undo.svg", source, args);		
		
		clicked_.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	        		           
	           DisplayIO.getCurrentFrame().undo();		     
	        }          
	     });		
	}
	
	@Override
	protected String[] getArgs() {
		// TODO Auto-generated method stub
		return null;
	}
}
