package org.expeditee.items.widgets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.expeditee.gui.DisplayIO;
import org.expeditee.gui.FrameMouseActions;
import org.expeditee.gui.FreeItems;
import org.expeditee.items.Item;
import org.expeditee.items.Text;

//When the Rubbish Bin Widget is clicked by any of the mouse buttons, whatever is currently being picked up by the mouse is destroyed
public class RubbishBin extends ButtonWidget{	
		
	public RubbishBin(Text source, String[] args){	
					
		super(80, "org/expeditee/assets/images/bin.svg", source, args);

		clicked_.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent e) {	
	        	
	        }          
	     });		
	}
	public boolean ItemsLeftClickDropped() {
		
		//Find what the mouse was holding at the time and delete it
		Item freeItems = FreeItems.getItemAttachedToCursor();
		if( freeItems != null){
			freeItems.setParent(DisplayIO.getCurrentFrame());
			
			try{
			
			FrameMouseActions.delete(freeItems);		
			}
			catch(Exception e){
				
				e.printStackTrace();
			}			
		}
		return true;
	}
	
	@Override
	protected String[] getArgs() {
		// TODO Auto-generated method stub
		return null;
	}
	//this should be a generic method for any widget that is clicked while an item is being held
	public boolean itemHeldWhileClicked(InteractiveWidget iw){
			
		ItemsLeftClickDropped();
		return true;
			
	}
	public boolean getdropInteractableStatus(){
		return true;
	}
	
}
