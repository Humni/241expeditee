/**
 *    Polar.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.items.widgets.charts;

import java.awt.Color;

import org.expeditee.items.Text;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.Axis;
import org.jfree.chart.plot.PolarPlot;

public class Polar extends AbstractXY {

	public Polar(Text source, String[] args) {
		super(source, args);
	}

	@Override
	protected JFreeChart createNewChart() {
		return ChartFactory.createPolarChart(DEFAULT_TITLE, getChartData(),
				true, // legend?
				true, // tooltips?
				false // URLs?
				);
	}

	@Override
	public void setSourceColor(Color c) {
		super.setSourceColor(c);
		if (getChart() == null)
			return;

		PolarPlot plot = ((PolarPlot) getChart().getPlot());

		if (c == null) {
			plot.setAngleLabelPaint(Axis.DEFAULT_AXIS_LABEL_PAINT);
		} else {
			plot.setAngleLabelPaint(c);
		}
	}
	
	@Override
	public void setSourceThickness(float newThickness, boolean setConnected) {
		super.setSourceThickness(newThickness, setConnected);
		if (getChart() == null)
			return;
		PolarPlot plot = ((PolarPlot) getChart().getPlot());
		plot.setAngleLabelFont(plot.getAngleLabelFont().deriveFont(getFontSize(newThickness, Axis.DEFAULT_TICK_LABEL_FONT.getSize2D())));
	}
}
