/**
 *    Password.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.items.widgets;

import java.awt.GridLayout;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import org.expeditee.items.Text;

/**
 * Allows entering password data in non-visible form
 * TODO: encrypt this before storing it
 * @author jts21
 */
public class Password extends InteractiveWidget {

	private JPasswordField passwordField;
	private JCheckBox savePassword;
	
	public Password(Text source, String[] args) {
		super(source, new JPanel(new GridLayout(0, 1)), 200, 200, 60, 60);
		JPanel p = (JPanel) super._swingComponent;
		this.passwordField = new JPasswordField();
		this.savePassword = new JCheckBox("Save password?");
		p.add(passwordField);
		p.add(savePassword);
		if(args != null && args.length > 0) {
			String[] password = args[0].split(":");
			StringBuilder s = new StringBuilder();
			for(String str : password) {
				char c = (char) Byte.parseByte(str);
				s.append(c);
			}
			this.passwordField.setText(s.toString());
			this.savePassword.setSelected(true);
		}
	}

	@Override
	protected String[] getArgs() {
		if(!savePassword.isSelected()) {
			return null;
		}
		char[] password = this.passwordField.getPassword();
		StringBuilder s = new StringBuilder();
		for(char b : password) {
			s.append((byte)b + ":");
		}
		return new String[] { s.toString() };
	}
	
	public String getPassword() {
		return new String(this.passwordField.getPassword());
	}
	
	public void setPassword(String password) {
		this.passwordField.setText(password);
	}

}
