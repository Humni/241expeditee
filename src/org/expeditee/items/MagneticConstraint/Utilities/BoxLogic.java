package org.expeditee.items.MagneticConstraint.Utilities;

import java.awt.Rectangle;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.expeditee.gui.Frame;
import org.expeditee.items.Dot;
import org.expeditee.items.Item;

public class BoxLogic {
	public static List<org.expeditee.items.Line> getVerticalLines(
			final Frame frame) {
		final List<org.expeditee.items.Line> ret = new LinkedList<org.expeditee.items.Line>();
		final Collection<Item> allItems = frame.getAllItems();
		final List<Dot> dots = new LinkedList<Dot>();
		for (final Item canditate : allItems)
			if (canditate instanceof Dot)
				dots.add((Dot) canditate);
		for (final Dot dot : dots) {
			final List<org.expeditee.items.Line> lines = dot.getLines();
			for (final org.expeditee.items.Line vertCanditate : lines) {
				if (!ret.contains(vertCanditate)
						&& vertCanditate.getStartItem().getX() == vertCanditate
								.getEndItem().getX())
					ret.add(vertCanditate);
			}
		}
		return ret;
	}
	
	public static boolean boxCollision(final Item item) {
		if(item.getParent() == null) return false;
		final List<org.expeditee.items.Line> verticalLines = BoxLogic
				.getVerticalLines(item.getParent());
		for (final org.expeditee.items.Line canditate : verticalLines) {
			final int topPos = Math.min(canditate.getStartItem().getY(),
					canditate.getEndItem().getY());
			final int bottomPos = Math.max(canditate.getStartItem().getY(),
					canditate.getEndItem().getY());
			final Rectangle itemHitBox = item.getArea().getBounds();
			if (topPos < itemHitBox.getY() && bottomPos > itemHitBox.getY()) {
				if (itemHitBox.getX() + itemHitBox.getWidth() > canditate
						.getStartItem().getX()
						&& itemHitBox.getX() < canditate.getStartItem().getX()) {
					return true;
				}
			}
		}
		return false;
	}
	
//	private static boolean lineShadowsItem(final org.expeditee.items.Line line, final Item item) {
//		final int topPos = Math.min(line.getStartItem().getY(),
//				line.getEndItem().getY());
//		final int bottomPos = Math.max(line.getStartItem().getY(),
//				line.getEndItem().getY());
//		final Rectangle itemHitBox = item.getArea().getBounds();
//		return topPos < itemHitBox.
//	}

//	public static void resolveCollision(final Item item, final MagneticConstraintAction remagnet) {
//		boolean updateCursor = FrameUtils.getCurrentItem() == item;
//		final Line currentLine = Line.getLineContainingToken(item);
//		final Paragraph currentParagraph = Paragraph.getParagraphFromLine(currentLine);
//		final Point newLineAt = currentParagraph.split(currentLine);
//		final Line lineFromToken = Line.getLineFromToken(item);
//		lineFromToken.deltaX((int) newLineAt.getX() - lineFromToken.getFirst().getX());
//		lineFromToken.deltaY((int) newLineAt.getY() - lineFromToken.getFirst().getY());
//		remagnet.callback(null);
//		final Line nextLine = lineFromToken.next();
//		if(nextLine != null && nextLine.getBoundingBox().getX() >= lineFromToken.getBoundingBox().getX()) {
//			final Paragraph paragraphFromNewLine = Paragraph.getParagraphFromLine(lineFromToken);
//			final Paragraph paragraphFromNextLine = Paragraph.getParagraphFromLine(nextLine);
//			if(paragraphFromNewLine.containsAll(paragraphFromNextLine)) {
//				final Rectangle newLineBoundingBox = lineFromToken.getBoundingBox();
//				nextLine.setX((int) newLineBoundingBox.getMaxX());
//				nextLine.setY((int) newLineBoundingBox.getY());
//			}
//		}
//		if(updateCursor) DisplayIO.MoveCursorToEndOfItem(item);
//		FrameIO.ForceSaveFrame(item.getParent());
//	}
}
