package org.expeditee.items.MagneticConstraint.Utilities;

import java.util.*;

import org.expeditee.io.flowlayout.XRawItem;
import org.expeditee.items.Item;
import org.expeditee.items.MagneticConstraint.MagneticConstraintActionWithArgumentsAndReturnType;

public class XGroupLogic {
	
	private static MagneticConstraintActionWithArgumentsAndReturnType<List<Item>, List<XRawItem>> action;

	public static void setXGroupAlgorithm(final MagneticConstraintActionWithArgumentsAndReturnType<List<Item>, List<XRawItem>> action) {
		XGroupLogic.action = action;
	}
	
	public static boolean areInSameXGroup(final Item item1, final Item item2) {
		final Item toFind1 = item1.getText().startsWith("@BP") ? item1.getParent().getItemWithID(item1.getMagnetizedItemRight()) : item1;
		final Item toFind2 = item2.getText().startsWith("@BP") ? item2.getParent().getItemWithID(item2.getMagnetizedItemRight()) : item2;
		final List<Item> unordered = new LinkedList<Item>(toFind1.getParent().getAllItems());
		final List<XRawItem> orderedItems = action.exec(null, unordered);
		XRawItem item1Raw = null;
		XRawItem item2Raw = null;
		for(final XRawItem canditate : orderedItems) {
			if(canditate.getItem() == toFind1) item1Raw = canditate;
			else if (canditate.getItem() == toFind2) item2Raw = canditate;
		}
		return item1Raw.getGroup() == item2Raw.getGroup();
	}
}
