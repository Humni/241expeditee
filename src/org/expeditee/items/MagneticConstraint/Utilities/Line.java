package org.expeditee.items.MagneticConstraint.Utilities;

import java.awt.Rectangle;
import java.util.LinkedList;

import org.expeditee.items.Item;

@SuppressWarnings("serial")
public class Line extends LinkedList<Item> {
		
	private Line() {
		super();
	}
	
	private Line(final Line toCopy) {
		super(toCopy);
	}
	
	public Line prependLine(final Line line) {
		final int x = this.getBoundingBox().x;
		final int width = line.getBoundingBox().width;
		this.deltaX(width);
		line.setX(x);
		line.setY(this.getFirst().getY());
		return Line.getLineFromToken(line.getFirst());
	}
	
	public Line appendLine(final Line line) {
		final int x = this.getBoundingBox().x;
		final int width = this.getBoundingBox().width;
		line.setX(x + width);
		line.setY(this.getFirst().getY());
		return Line.getLineFromToken(this.getFirst());
	}
	
	public static Line getLineFromToken(final Item token) {
		if(token == null) return new Line();
		else {
			return getLineFromToken(token, new Line());
		}
	}
	
	public static Line getLineContainingToken(final Item token) {
		if(token == null) return new Line();
		final Line ret = new Line();
		Item current = token;
		while(current.getMagnetizedItemLeft() != -1) {
			final Item potentialStart = current.getParent().getItemWithID(current.getMagnetizedItemLeft());
			if(potentialStart == null) break;
			current = potentialStart;
			ret.addFirst(current);
		}
		final Line following = getLineFromToken(token, new Line());
		ret.addAll(following);
		return ret;
//		return getLineFromToken(current, new Line());
	}
	
	public static int getLineHeight(final Line line) {
		return (int) (line.getBoundingBox().getHeight() * 1.2);
	}
	
	public Line setX(final int newX) {
		int delta = newX - this.getFirst().getX();
		deltaX(delta);
		return this;
	}
	
	public Line setY(final int newY) {
		int delta = newY - this.getFirst().getY();
		deltaY(delta);
		return this;
	}
	
	public Line deltaX(final int delta) {
		for(final Item token : this) token.setPosition(token.getX() + delta, token.getY());
		return this;
	}
	
	public Line deltaY(final int delta) {
		for(final Item token : this) token.setPosition(token.getX(), token.getY() + delta);
		return this;
	}
	
	public Rectangle getBoundingBox() {
//		final Rectangle rect = new Rectangle(this.getFirst().getX(), this.getFirst().getY(), this.getFirst().getBoundsWidth(), this.getFirst().getBoundsHeight());
//		for(final Item token : this) 
//			rect.add(new Rectangle(token.getX(), token.getY(), token.getBoundsWidth(), token.getBoundsHeight()));
//		return rect;
		final Rectangle rect = this.getFirst().getArea().getBounds();
		for(final Item token : this)
			rect.add(token.getArea().getBounds());
		return rect;
	}
	
	public Line next() {
		final Item lastInThisLine = this.getLast();//this.get(this.size() - 1);
		if(lastInThisLine.getMagnetizedItemBottom() == -1) return null;
		else return getLineFromToken(this.get(0).getParent().getItemWithID(lastInThisLine.getMagnetizedItemBottom()));
	}
	
	public Line last() {
		final Item firstInThisLine = this.getFirst();//this.get(0);
		if(firstInThisLine.getMagnetizedItemTop() == -1) return null;
		else return getLineContainingToken(this.get(0).getParent().getItemWithID(firstInThisLine.getMagnetizedItemTop()));
	}
		
	private static Line getLineFromToken(final Item token, final Line acc) {
		if(token == null) return acc;
		if(token.getMagnetizedItemRight() == -1) {
			acc.add(token);
			return acc;
		} else {
			final Line lineSoFar = new Line(acc);
			if(lineSoFar.contains(token)) return lineSoFar;
			lineSoFar.add(token);
			return getLineFromToken(token.getParent().getItemWithID(token.getMagnetizedItemRight()), lineSoFar);
		}
	}
}
