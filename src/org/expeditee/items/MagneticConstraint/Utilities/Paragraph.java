package org.expeditee.items.MagneticConstraint.Utilities;

import java.awt.*;
import java.util.LinkedList;
import org.expeditee.items.Text;

@SuppressWarnings("serial")
public class Paragraph extends LinkedList<Line> {
	private Paragraph() {
		super();
	}
	
	private Paragraph(final Paragraph toCopy) {
		super(toCopy);
	}
	
	public static Paragraph getParagraphFromLine(final Line line) {
		final int lineHeight = Paragraph.getLineHeight(line);
		return getParagraphFromLine(line, lineHeight, new Paragraph());
	}
	
	public Paragraph deltaX(final int delta) {
		for(final Line ln : this) ln.deltaX(delta);
		return this;
	}
	
	public Paragraph deltaY(final int delta) {
		for(final Line ln : this) ln.deltaY(delta);
		return this;
	}
	
	public Rectangle getBoundingBox() {
		final Rectangle rect = new Rectangle();
		for(final Line ln : this) rect.add(ln.getBoundingBox());
		return rect;
	}
	
	public Paragraph next() {
		final Line lastLineInParagraph = this.get(this.size() - 1);
		final Line firstLineNotInParagraph = lastLineInParagraph.next();
		if(firstLineNotInParagraph == null) return null;
		else return Paragraph.getParagraphFromLine(firstLineNotInParagraph);
	}
	
	public Point split(final Line splitAt) {
		final int lineHeight = Paragraph.getLineHeight(splitAt);
		//1. Get next line
		final Line nextLine = splitAt.next();
		//2. Is there a next line?
		if(nextLine == null) {
			//2a. No; then we simply need to calculate the position of where it would be and return that.
			final Rectangle boundingBox = splitAt.getBoundingBox();
			return new Point((int)boundingBox.getX(), (int)boundingBox.getMaxY() + lineHeight);
		}
		//3. Get paragraph starting from the next line
		final Paragraph paragraphStartingAtNextLine = getParagraphFromLine(nextLine);
		//4. Are we still working for the same paragraph?
		if(!this.containsAll(paragraphStartingAtNextLine)) {
			//4b. No; then we simply need to calculate the position of the next line and return that.
			final Rectangle boundingBox = splitAt.getBoundingBox();
			return new Point(((Text) splitAt.getFirst()).getX(), (int)boundingBox.getMaxY() + lineHeight);
		} 
		//NB at this point we know that there are lines below our 'splitAt' line in the same paragraph so they need to be moved down.
		//5. Move lines below 'splitAt' down by 'lineHeight'
		paragraphStartingAtNextLine.deltaY(lineHeight);
		//6. Return the position of the new line we have made space for.
		
		final Rectangle boundingBox = splitAt.getBoundingBox();
		final int x = ((Text) splitAt.getFirst()).getX();
		return new Point(x, (int)boundingBox.getMaxY() + lineHeight);
	}
	
	private static int getLineHeight(final Line line) {
		return (int) (line.get(0).getArea().getBounds().getHeight() * 1.2);
	}
	
	private static Paragraph getParagraphFromLine(final Line line, final int lineHeight, final Paragraph acc) {
		final Line nextLine = line.next();
		if(nextLine == null || !isInSameParagraph(line, nextLine, lineHeight)) {
			acc.add(line);
			return acc;
		} else {
			final Paragraph paragraphSoFar = new Paragraph(acc);
			paragraphSoFar.add(line);
			return getParagraphFromLine(nextLine, lineHeight, paragraphSoFar);
		}
	}
	
	private static boolean isInSameParagraph(final Line thisLine, final Line nextLine, final int lineHeight) {
		if(!XGroupLogic.areInSameXGroup(thisLine.get(0), nextLine.get(0))) return false;
		final Rectangle thisLineBoundingBox = thisLine.getBoundingBox();
		final Rectangle nextLineBoundingBox = nextLine.getBoundingBox();
		final Point toAdd = new Point(Math.min((int)thisLineBoundingBox.getX(),(int) nextLineBoundingBox.getX()), (int)thisLineBoundingBox.getMaxY() + lineHeight);
		final Rectangle overlappingSection = new Rectangle(thisLineBoundingBox);
		overlappingSection.add(toAdd);
		return overlappingSection.intersects(nextLineBoundingBox);
	}
}
