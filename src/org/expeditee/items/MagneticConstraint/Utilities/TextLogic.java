package org.expeditee.items.MagneticConstraint.Utilities;

import org.expeditee.gui.Browser;
import org.expeditee.gui.DisplayIO;
import org.expeditee.items.Text;

public class TextLogic {
	
	public static boolean XIsTowardsRight(final int x, final Text text) {
		if(text.getPixelBoundsUnion() == null) return false;
		final double distanceThroughText = x - text.getPixelBoundsUnion().getX();
		return /*text.getText().length() == 1 ||*/ distanceThroughText > (text.getPixelBoundsUnion().getWidth() / 2);
	}
	
	public static boolean XIsTowardsLeft(final Text text, final int x) {
		if(text.getPixelBoundsUnion() == null) return false;
		final double distanceThroughText = x - text.getPixelBoundsUnion().getX();
		return distanceThroughText < (text.getPixelBoundsUnion().getWidth() / 2);
	}
	
	public static boolean XIsBeforeCharacters(final Text text, final int x) {
		try {
			final int firstLetterWidth = Browser._theBrowser.getFontMetrics(Browser._theBrowser.getFont()).stringWidth(text.getText().substring(0, 1));
			final double distanceThroughText = x - text.getPixelBoundsUnion().getX();
			return firstLetterWidth > distanceThroughText;
		} catch (NullPointerException e) {
			return false;
		} catch (StringIndexOutOfBoundsException e) {
			return false;
		}
	}
	
	public static boolean XIsAfterCharacters(final Text text, final int x) {
		try {
			final double distanceThroughText = x - text.getPixelBoundsUnion().getX();
			return distanceThroughText > text.getPixelBoundsUnion().getWidth();
		} catch (NullPointerException e) {
			return false;
		} 
	}
	
	public static int GetInsertionIndexSelected(final Text text) {
		return text.getCharPosition(0, DisplayIO.getMouseX()).getInsertionIndex();
	}
}
