/**
 *    MagneticConstraints.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.items.MagneticConstraint;

import java.util.*;
//import java.util.concurrent.*;

import org.expeditee.items.Item;

public class MagneticConstraints {
	public static List<String> log = new LinkedList<String>();
	
	private static MagneticConstraints instance = null;
	
	private MagneticConstraintAction rightBorderHitAction = null;
	private MagneticConstraintAction endOfLineHitAction = null;
	private MagneticConstraintAction leftBorderHitAction = null;
	private MagneticConstraintAction startOfLineHitAction = null;
	private MagneticConstraintActionWithArguments<Float> textShrunkAction = null;
	private MagneticConstraintActionWithArguments<Float> textGrownAction = null;
	private MagneticConstraintAction topBorderHitAction = null;
	private MagneticConstraintAction bottomBorderHitAction = null;
	private Map<Integer, MagneticConstraintAction> keyEvents = new LinkedHashMap<Integer, MagneticConstraintAction>(); 
	
//	private final ExecutorService thread = Executors.newSingleThreadExecutor();

//	private boolean textShrunkRunnable = true;
//	private boolean textGrownRunnable = true;

	public static MagneticConstraints getInstance() {
		if (instance == null) {
			instance = new MagneticConstraints();
			return instance;
		} else
			return instance;
	}
		
	public static void Log(final Class<? extends MagneticConstraintAction> actionType, 
			final Item[] primaryInvolved, final Item[] line, final String additionalInfo) {
		final String nl = System.getProperty("line.separator");
		
		final String heading = "Action type: " + actionType;
		final String[] primaryInvolvedStringed = new String[primaryInvolved.length];
		for(int i = 0; i < primaryInvolved.length; i++) 
			primaryInvolvedStringed[i] = "\t" + "Primary item " + i + ".  Text: " + primaryInvolved[i].getText();
		String lineContent = "\tLine content: ";
		for(final Item i : line) lineContent += (i.getText() + " ");
		String logContent = heading + nl;
		for(final String s : primaryInvolvedStringed) logContent += (s + nl);
		logContent += (lineContent + nl + additionalInfo + nl);
		
		log.add(logContent);
	}
	
	public boolean keyHit(final int key, final Item item) {
		final MagneticConstraintAction action = keyEvents.get(key);
		if(action == null && key < 0 && keyEvents.get(-key) != null) {
			return keyEvents.get(-key).invert(item);
		} else if(action == null) return false;
		else return action.exec(item);
	}
	
	public void setKeyAction(final int key, final MagneticConstraintAction action) {
		keyEvents.put(key, action);
	}
	
	public boolean rightBorderHit(final Item item) {
	if (rightBorderHitAction != null)
		return rightBorderHitAction.exec(item);
	else
		return false;
	}

//	public boolean rightBorderHit(final Item item) {
//		try {
//			if(rightBorderHitAction != null) {
//				final FutureTask<Boolean> future = new FutureTask<Boolean>(new Callable<Boolean>() {
//					@Override
//					public Boolean call() throws Exception {
//						return rightBorderHitAction.exec(item);
//					}
//				});
//				thread.execute(future);
//				return future.get();
//			} else return false;
//		} catch (final Exception e) {
//			System.err.println("Concurrent Exception caught during rightBorderHit action");
//			return false;
//		}
//	}

	public void setRightBorderHitAction(final MagneticConstraintAction action) {
		rightBorderHitAction = action;
	}

	public boolean endOfLineHit(final Item item) {
		if (endOfLineHitAction != null)
			return endOfLineHitAction.exec(item);
		else
			return false;
	}
	
//	public boolean endOfLineHit(final Item item) {
//		try {
//			if(endOfLineHitAction != null) {
//				final FutureTask<Boolean> future = new FutureTask<Boolean>(new Callable<Boolean>() {
//					@Override
//					public Boolean call() throws Exception {
//						return endOfLineHitAction.exec(item);
//					}
//				});
//				thread.execute(future);
//				return future.get();
//			} else return false;
//		} catch (final Exception e) {
//			System.err.println("Concurrent Exception caught during endOfLineHit action");
//			return false;
//		}
//	}

	public void setEndOfLineHitAction(final MagneticConstraintAction action) {
		endOfLineHitAction = action;
	}

	public boolean leftBorderHit(final Item item) {
		if (leftBorderHitAction != null)
			return leftBorderHitAction.exec(item);
		else
			return false;
	}
	
//	public boolean leftBorderHit(final Item item) {
//		try {
//			if(leftBorderHitAction != null) {
//				final FutureTask<Boolean> future = new FutureTask<Boolean>(new Callable<Boolean>() {
//					@Override
//					public Boolean call() throws Exception {
//						return leftBorderHitAction.exec(item);
//					}
//				});
//				thread.execute(future);
//				return future.get();
//			} else return false;
//		} catch (final Exception e) {
//			System.err.println("Concurrent Exception caught during leftBorderHit action");
//			return false;
//		}
//	}

	public void setLeftBorderHitAction(final MagneticConstraintAction action) {
		leftBorderHitAction = action;
	}

	public boolean startOfLineHit(final Item item) {
		if (startOfLineHitAction != null)
			return startOfLineHitAction.exec(item);
		else
			return false;
	}
	
//	public boolean startOfLineHit(final Item item) {
//		try {
//			if(startOfLineHitAction != null) {
//				final FutureTask<Boolean> future = new FutureTask<Boolean>(new Callable<Boolean>() {
//					@Override
//					public Boolean call() throws Exception {
//						return startOfLineHitAction.exec(item);
//					}
//				});
//				thread.execute(future);
//				return future.get();
//			} else return false;
//		} catch (final Exception e) {
//			System.err.println("Concurrent Exception caught during startOfLineHit action");
//			return false;
//		}
//	}
	
	public void setStartOfLineHitAction(final MagneticConstraintAction action) {
		startOfLineHitAction = action;
	}
	
	public boolean textShrunk(final Item item, final float delta) {
		if(/*textShrunkRunnable &&*/ textShrunkAction != null)
			return textShrunkAction.exec(item, delta);
		else return false;
	}
	
//	public void suspendTextShrunkAction() {
//		textShrunkRunnable = false;
//	}
//	
//	public void resumeTextShrunkAction() {
//		textShrunkRunnable = true;
//	}
	
//	public boolean textShrunk(final Item item, final float delta) {
//		try {
//			if(textShrunkAction != null && textShrunkRunnable) {
//				final FutureTask<Boolean> future = new FutureTask<Boolean>(new Callable<Boolean>() { 
//					@Override
//					public Boolean call() throws Exception {
//						return textShrunkAction.exec(item, delta);
//					}
//				});
//				thread.execute(future);
//				return future.get();
//			} else return false;
//		} catch (final Exception e) {
//			System.err.println("Concurrent Exception caught during textShrunk action");
//			return false;
//		}
//	}
	
	public void setTextShrunkAction(final MagneticConstraintActionWithArguments<Float> action) {
		textShrunkAction = action;
	}
	
	public boolean textGrown(final Item item, final float delta) {
		if(/*textGrownRunnable &&*/ textGrownAction != null) 
			return textGrownAction.exec(item, delta);
		else return false;
	}
	
//	public void suspendTextGrownAction() {
//		textGrownRunnable = false;
//	}
//	
//	public void resumeTextGrownAction() {
//		textGrownRunnable  = true;
//	}
	
//	public boolean textGrown(final Item item, final float delta) {
//		try {
//			if(textGrownAction != null && textGrownRunnable) {
//				final FutureTask<Boolean> future = new FutureTask<Boolean>(new Callable<Boolean>() { 
//					@Override
//					public Boolean call() throws Exception {
//						return textGrownAction.exec(item, delta);
//					}
//				});
//				thread.execute(future);
//				return future.get();
//			} else return false;
//		} catch (final Exception e) {
//			System.err.println("Concurrent Exception caught during textGrown action");
//			return false;
//		}
//	}
	
	public void setTextGrownAction(final MagneticConstraintActionWithArguments<Float> action) {
		textGrownAction = action;
	}

	public boolean topBorderHit(final Item item) {
		if(topBorderHitAction != null)
			return topBorderHitAction.exec(item);
		else return false;
	}
	
//	public boolean topBorderHit(final Item item) {
//		try {
//			if(topBorderHitAction != null) {
//				final FutureTask<Boolean> future = new FutureTask<Boolean>(new Callable<Boolean>() {
//					@Override
//					public Boolean call() throws Exception {
//						return topBorderHitAction.exec(item);
//					}
//				});
//				thread.execute(future);
//				return future.get();
//			} else return false;
//		} catch (final Exception e) {
//			System.err.println("Concurrent Exception caught during topBorderHit action");
//			return false;
//		}
//	}
	
	public void setTopBorderHitAction(final MagneticConstraintAction action) {
		topBorderHitAction = action;
	}
	
	public boolean bottomBorderHit(final Item item) {
		if(bottomBorderHitAction != null) 
			return bottomBorderHitAction.exec(item);
		else return false;
	}
	
//	public boolean bottomBorderHit(final Item item) {
//		try {
//			if(bottomBorderHitAction != null) {
//				final FutureTask<Boolean> future = new FutureTask<Boolean>(new Callable<Boolean>() {
//					@Override
//					public Boolean call() throws Exception {
//						return bottomBorderHitAction.exec(item);
//					}
//				});
//				thread.execute(future);
//				return future.get();
//			} else return false;
//		} catch (final Exception e) {
//			System.err.println("Concurrent Exception caught during bottomBorderHit action");
//			return false;
//		}
//	}
	
	public void setBottomBorderHitAction(final MagneticConstraintAction action) {
		bottomBorderHitAction = action;
	}
}
