///**
// *    NextItemToLeftAction.java
// *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
// *
// *    This program is free software: you can redistribute it and/or modify
// *    it under the terms of the GNU General Public License as published by
// *    the Free Software Foundation, either version 3 of the License, or
// *    (at your option) any later version.
// *
// *    This program is distributed in the hope that it will be useful,
// *    but WITHOUT ANY WARRANTY; without even the implied warranty of
// *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// *    GNU General Public License for more details.
// *
// *    You should have received a copy of the GNU General Public License
// *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
// */
//
//package org.expeditee.items.MagneticConstraint;
//
//import java.awt.Font;
//import java.awt.Point;
//
//import org.expeditee.gui.Browser;
//import org.expeditee.gui.DisplayIO;
//import org.expeditee.gui.FrameGraphics;
//import org.expeditee.items.Item;
//import org.expeditee.items.Text;
//
//public class NextItemToLeftAction implements MagneticConstraintAction {
//
//	@Override
//	public boolean exec(final Item item) {
//		final Item toLeft = item.getParent().getItemWithID(item.getMagnetizedItemLeft());
//		return moveCursor(toLeft);
//	}
//	
//	private boolean moveCursor(final Item toMoveTo) {
//		if(toMoveTo == null) return false;
//		if(toMoveTo instanceof Text) {
//			final Text asText = (Text) toMoveTo;
//			final Font font = asText.getFont();
//			final int indent = Browser._theBrowser.g.getFontMetrics(font).stringWidth(asText.getText().substring(0, asText.getText().length() - 1));
//			final Point position = toMoveTo.getPosition();
//			DisplayIO.setCursorPosition(position.x + indent, position.y, false);
//			FrameGraphics.refresh(false);
//		} else DisplayIO.setCursorPosition(toMoveTo.getPosition(), false);
//		return true;
//	}
//
//}
