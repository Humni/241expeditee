///**
// *    RepelTextAction.java
// *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
// *
// *    This program is free software: you can redistribute it and/or modify
// *    it under the terms of the GNU General Public License as published by
// *    the Free Software Foundation, either version 3 of the License, or
// *    (at your option) any later version.
// *
// *    This program is distributed in the hope that it will be useful,
// *    but WITHOUT ANY WARRANTY; without even the implied warranty of
// *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// *    GNU General Public License for more details.
// *
// *    You should have received a copy of the GNU General Public License
// *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
// */
//
//package org.expeditee.items.MagneticConstraint;
//
//import org.expeditee.items.Item;
//
//public class RepelTextAction implements
//		MagneticConstraintActionWithArguments<Float> {
//
//	@Override
//	public boolean exec(final Item item) {
//		return exec(item, 0f);
//	}
//
//	@Override
//	public boolean exec(final Item item, final Float... args) {
//		if (item.getMagnetizedItemRight() == -1)
//			return false;
//		else if (item.getParent().getItemWithID(item.getMagnetizedItemRight()) == null) {
//			item.setMagnetizedItemRight(null);
//			return false;
//		} else {
//			final Item toMyRight = item.getParent().getItemWithID(
//					item.getMagnetizedItemRight());
//			System.err.println("Growing x: " + item.getX());
//			System.err.println("Growing width: " + item.getBoundsWidth());
//			System.err.println("Growing by: " + args[0]);
//			System.err.println("To My Right X: " + toMyRight.getX());
//			if(item.getX() + item.getBoundsWidth() + args[0] >= toMyRight.getX()) {
//				toMyRight.setX(toMyRight.getX() + args[0]);
//				new RepelTextAction().exec(toMyRight, args);
//				return true;
//			} else return false;
//		}
//	}
//
//}
