/**
 *    MagneticConstraintAction.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.items.MagneticConstraint;

import java.util.List;

import org.expeditee.gui.Frame;
import org.expeditee.items.Item;

public abstract class MagneticConstraintAction {
	protected MagneticConstraintAction callbackAction;
		
	abstract public boolean exec(final Item item);
	
	abstract public boolean invert(final Item item);
	
	public void callback(final Item item) {
		callbackAction.exec(item);
	}
	
	public void setCallbackAction(final MagneticConstraintAction action) {
		callbackAction = action;
	}
	
	protected boolean isSpIDERCodePage(final Frame frame) {
		if(frame.getTitleItem() == null) return false;
		final List<String> data = frame.getTitleItem().getData();
		boolean hasProjectTag = false;
		boolean hasPackageTag = false;
		boolean hasClassTag = false;
		if(data == null) return false;
		for(final String s : data) {
			if(s.startsWith("Project:")) hasProjectTag = true;
			else if(s.startsWith("Package:")) hasPackageTag = true;
			else if(s.startsWith("Class:")) hasClassTag = true;
		}
		return hasProjectTag && hasPackageTag && hasClassTag;
	}
}
