///**
// *    AttractTextAction.java
// *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
// *
// *    This program is free software: you can redistribute it and/or modify
// *    it under the terms of the GNU General Public License as published by
// *    the Free Software Foundation, either version 3 of the License, or
// *    (at your option) any later version.
// *
// *    This program is distributed in the hope that it will be useful,
// *    but WITHOUT ANY WARRANTY; without even the implied warranty of
// *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// *    GNU General Public License for more details.
// *
// *    You should have received a copy of the GNU General Public License
// *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
// */
//
//package org.expeditee.items.MagneticConstraint;
//
//import org.expeditee.items.Item;
//
//public class AttractTextAction implements
//		MagneticConstraintActionWithArguments<Float> {
//
//	@Override
//	public boolean exec(Item item) {
//		return exec(item, 0f);
//	}
//
//	@Override
//	public boolean exec(Item item, Float... args) {
//		if(item.getMagnetizedItemRight() == -1)
//			return false;
//		else if (item.getParent().getItemWithID(item.getMagnetizedItemRight()) == null) {
//			item.setMagnetizedItemRight(null);
//			return false;
//		}
//		else {
//			final Item toMyRight = item.getParent().getItemWithID(item.getMagnetizedItemRight());
//			toMyRight.setX(toMyRight.getX() - args[0]);
//			new AttractTextAction().exec(toMyRight, args);
//			return true;
//		}
//	}
//
//}
