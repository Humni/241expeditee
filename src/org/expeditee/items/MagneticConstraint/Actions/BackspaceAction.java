package org.expeditee.items.MagneticConstraint.Actions;

import org.expeditee.gui.DisplayIO;
import org.expeditee.items.Item;
import org.expeditee.items.Text;
import org.expeditee.items.MagneticConstraint.MagneticConstraintAction;
import org.expeditee.items.MagneticConstraint.Utilities.Line;
import org.expeditee.items.MagneticConstraint.Utilities.Paragraph;
import org.expeditee.items.MagneticConstraint.Utilities.TextLogic;

public class BackspaceAction extends MagneticConstraintAction {

	@Override
	public boolean exec(final Item item) {
		if(item.getParent() == null || !this.isSpIDERCodePage(item.getParent())) return false;
		if(item == null || ((Text) item).isEmpty()) {
			final Text temp = DisplayIO.getCurrentFrame().createNewText(".");
			callback(temp);
			if(temp.getMagnetizedItemLeft() != -1) 
				DisplayIO.setTextCursor((Text) temp.getParent().getItemWithID(temp.getMagnetizedItemLeft()), Text.END);
			else if(temp.getMagnetizedItemTop() != -1) 
				DisplayIO.setTextCursor((Text) temp.getParent().getItemWithID(temp.getMagnetizedItemTop()), Text.END);
			temp.delete();
			callback(temp);
			return true;
		} else if(item.getMagnetizedItemLeft() != -1 && TextLogic.XIsBeforeCharacters((Text) item, DisplayIO.getMouseX())) {
			new LastItemAction().exec(item);
			return true;
		} else if(item.getMagnetizedItemTop() != -1 && TextLogic.XIsBeforeCharacters((Text) item, DisplayIO.getMouseX())) {
			callback(item);
			final Line currentLine = Line.getLineFromToken(item);
			final Line lastLine = currentLine.last();
			if(lastLine != null && Paragraph.getParagraphFromLine(lastLine).contains(currentLine)) {
				lastLine.appendLine(currentLine);
				DisplayIO.setTextCursor((Text) item, Text.HOME);
				callback(item);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean invert(Item item) {
		System.err.println("Repel text up action does not have a inverted implementation.");
		return false;
	}
}
