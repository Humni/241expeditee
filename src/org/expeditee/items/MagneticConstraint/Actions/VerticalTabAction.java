package org.expeditee.items.MagneticConstraint.Actions;

import org.expeditee.gui.Browser;
import org.expeditee.gui.DisplayIO;
import org.expeditee.items.Item;
import org.expeditee.items.MagneticConstraint.MagneticConstraintAction;
import org.expeditee.items.MagneticConstraint.Utilities.Line;

public class VerticalTabAction extends MagneticConstraintAction {

	@Override
	public boolean exec(final Item item) {
		if(item.getParent() == null || !this.isSpIDERCodePage(item.getParent())) return false;
		final int tabWidth = Browser._theBrowser.getFontMetrics(Browser._theBrowser.getFont()).stringWidth("          ");
		tab(item, tabWidth);
		return true;
	}

	@Override
	public boolean invert(Item item) {
		if(item.getParent() == null || !this.isSpIDERCodePage(item.getParent())) return false;
		final int tabWidth = -(Browser._theBrowser.getFontMetrics(Browser._theBrowser.getFont()).stringWidth("     "));
		tab(item, tabWidth);
		return true;
	}
	
	private void tab(final Item item, final int tabWidth) {
		if(item == null) {
			DisplayIO.setCursorPosition(DisplayIO.getMouseX() + tabWidth, DisplayIO.getMouseY());
		} else {
			final Line postTabLine = Line.getLineFromToken(item);
			postTabLine.deltaX(tabWidth);
			if(postTabLine.size() > 0)
				DisplayIO.setCursorPosition(postTabLine.get(0).getPosition());
		}
	}
}
