package org.expeditee.items.MagneticConstraint.Actions;

import org.expeditee.gui.DisplayIO;
import org.expeditee.gui.FrameGraphics;
import org.expeditee.gui.FrameUtils;
import org.expeditee.items.Item;
import org.expeditee.items.Text;
import org.expeditee.items.MagneticConstraint.MagneticConstraintAction;
import org.expeditee.items.MagneticConstraint.MagneticConstraints;
import org.expeditee.items.MagneticConstraint.Utilities.Line;

public class LastItemAction extends MagneticConstraintAction {
	
	@Override
	public boolean exec(final Item item) {
		if(item.getParent() == null || !this.isSpIDERCodePage(item.getParent())) return false;
		final int idLeft = item.getMagnetizedItemLeft();
		final int idTop = item.getMagnetizedItemTop();
		if(idLeft != -1) {
			moveCursor(item.getParent().getItemWithID(idLeft), item);
//			
//			System.err.println("#Current item " + FrameUtils.getCurrentItem());
//			System.err.println("#Current items" + FrameUtils.getCurrentItems());
			
			MagneticConstraints.Log(this.getClass(), new Item[]{item, item.getParent().getItemWithID(idLeft)}, 
					Line.getLineContainingToken(item).toArray(new Item[]{}), 
					"LastItemAction does not move any items around.  Only moves the cursor.  Left neighbor found; second primary.");
			return true;
		} else if (idTop != -1) {
			moveCursor(item.getParent().getItemWithID(idTop), item);
			MagneticConstraints.Log(this.getClass(), new Item[]{item, item.getParent().getItemWithID(idTop)}, 
					Line.getLineContainingToken(item).toArray(new Item[]{}), 
					"LastItemAction does not move any items around.  Only moves the cursor.  Top neighbor found; second primary.");
			return true;
		} else return false;
	}
	
	private boolean moveCursor(final Item toMoveTo, Item toMoveFrom) {
		if(toMoveTo == null) return false;
		if(toMoveTo instanceof Text) {
			//System.err.println("Last item requests move to item with text: " + toMoveTo.getText());
//			final Text asText = (Text) toMoveTo;
//			final java.awt.Point position = toMoveTo.getPosition();
//			DisplayIO.setCursorPosition(position.x + asText.getPixelBoundsUnion().width, position.y, false);
//			FrameGraphics.refresh(false);
			DisplayIO.setTextCursor((Text)toMoveTo, Text.END);
			if(FrameUtils.getCurrentItem() != toMoveTo) {
				Line.getLineFromToken(toMoveFrom).deltaX(1);
				moveCursor(toMoveTo, toMoveFrom);
			}
		} else DisplayIO.setCursorPosition(toMoveTo.getPosition(), false);
		return true;
	}

	@Override
	public boolean invert(Item item) {
		System.err.println("Last item action does not have a inverted implementation.");
		return false;
	}
}
