package org.expeditee.items.MagneticConstraint.Actions;

import org.expeditee.gui.DisplayIO;
import org.expeditee.items.Item;
import org.expeditee.items.Text;
import org.expeditee.items.MagneticConstraint.MagneticConstraintAction;
import org.expeditee.items.MagneticConstraint.MagneticConstraints;
import org.expeditee.items.MagneticConstraint.Utilities.Line;

public class DownALineAction extends MagneticConstraintAction {

	@Override 
	public boolean exec(final Item item) {
		if(item.getParent() == null || !this.isSpIDERCodePage(item.getParent())) return false;
		final Line currentLine = Line.getLineContainingToken(item);
		final Item startOfThisLine = currentLine.get(0);
		final Line nextLine = currentLine.next();
		if(nextLine == null) return false;
		final int nextLineLength = 
				nextLine.get(nextLine.size() - 1).getX() + nextLine.get(nextLine.size() - 1).getBoundsWidth() - nextLine.get(0).getX();
		final int distanceThroughCurrentLine = DisplayIO.getMouseX() - startOfThisLine.getX();
		if(distanceThroughCurrentLine <= nextLineLength) {
			Item beneith = nextLine.get(0);
			for(final Item token : nextLine) {
				if(token.getX() <= item.getX()) beneith = token;
				else break;
			}
			moveCursor(beneith);
			MagneticConstraints.Log(this.getClass(), new Item[]{item, beneith}, 
					Line.getLineContainingToken(item).toArray(new Item[]{}), 
					"DownALineAction does not move any items around.  Only moves the cursor.  Item to move to detected as second primary.");
		} else {
			moveCursor(nextLine.get(nextLine.size() - 1));
			MagneticConstraints.Log(this.getClass(), new Item[]{item}, 
					Line.getLineContainingToken(item).toArray(new Item[]{}), 
					"DownALineAction does not move any items around.  Only moves the cursor.  Item to move to detected to be end of line below");
		}
		return true;
	}
		
	private boolean moveCursor(final Item toMoveTo) {
		if(toMoveTo == null) return false;
		if(toMoveTo instanceof Text) {
			final Text asText = (Text) toMoveTo;
			DisplayIO.setTextCursor(asText, Text.END);
		} else DisplayIO.setCursorPosition(toMoveTo.getPosition(), false);
		return true;
	}

	@Override
	public boolean invert(final Item item) {
		System.err.println("Down a line action does not have a inverted implementation.");
		return false;
	}
}
