package org.expeditee.items.MagneticConstraint.Actions;

import java.awt.Point;

import org.expeditee.gui.FrameUtils;
import org.expeditee.items.Item;
import org.expeditee.items.MagneticConstraint.MagneticConstraintActionWithArguments;
import org.expeditee.items.MagneticConstraint.Utilities.Line;

public class AttractTextAction extends
		MagneticConstraintActionWithArguments<Float> {

	public static boolean Enabled = true;
	
	@Override
	public boolean exec(Item item) {
		return exec(item, 0f);
	}

	@Override
	public boolean exec(Item item, Float... args) {
		if(!Enabled || item == null || item.getParent() == null || item.getText().startsWith("@") || !isSpIDERCodePage(item.getParent())) {
			return false;
		}
		if(FrameUtils.getCurrentItem() == null) return false;
		final Point selectedItemPosition = FrameUtils.getCurrentItem().getPosition();
		if(!selectedItemPosition.equals(item.getPosition())) return false;
		
		if(item.getMagnetizedItemRight() == -1) return false;
		else if(item.getParent().getItemWithID(item.getMagnetizedItemRight()) == null) {
			item.setMagnetizedItemRight(null);
			return false;
		} else {
			final Item toMyRight = item.getParent().getItemWithID(item.getMagnetizedItemRight());
			final Line toAttract = Line.getLineFromToken(toMyRight);
			toAttract.deltaX(-args[0].intValue());
			return true;
		}
	}

	@Override
	public boolean invert(Item item) {
		System.err.println("Attract text action does not have a inverted implementation.");
		return false;
	}

}
