package org.expeditee.items.MagneticConstraint.Actions;

import java.awt.Point;

import org.expeditee.gui.Browser;
import org.expeditee.gui.FrameUtils;
import org.expeditee.items.Item;
import org.expeditee.items.Text;
import org.expeditee.items.MagneticConstraint.MagneticConstraintActionWithArguments;
import org.expeditee.items.MagneticConstraint.MagneticConstraints;
import org.expeditee.items.MagneticConstraint.Utilities.BoxLogic;
import org.expeditee.items.MagneticConstraint.Utilities.Line;

public class RepelTextAction extends
		MagneticConstraintActionWithArguments<Float> {
	public static boolean Enabled = true;

	@Override
	public boolean exec(final Item item) {
		MagneticConstraints.Log(this.getClass(), new Item[]{item}, 
				Line.getLineContainingToken(item).toArray(new Item[]{}), 
				"YOU SHOULD NOT SEE THIS");
		return exec(item, 0f);
	}

	@Override
	public boolean exec(final Item item, final Float... args) {
		if(!Enabled || item == null || item.getParent() == null || item.getText().startsWith("@") || !isSpIDERCodePage(item.getParent())) {
			return false;
		}
		if(FrameUtils.getCurrentItem() == null) return false;
		final Point selectedItemPosition = FrameUtils.getCurrentItem().getPosition();
		if(!selectedItemPosition.equals(item.getPosition())) return false;
		
		if(item.getMagnetizedItemRight() == -1) {
			if(BoxLogic.boxCollision(item)) {
				RepelTextDownAction.MergeSameLineParagraphCollisions = true;
				final RepelTextDownAction textDownAction = new RepelTextDownAction();
				textDownAction.setCallbackAction(this.callbackAction);
				textDownAction.exec(item);
				RepelTextDownAction.MergeSameLineParagraphCollisions = false;
			}
		} else if(item.getParent().getItemWithID(item.getMagnetizedItemRight()) == null) {
			item.setMagnetizedItemRight(null);
			return false;
		} else {
			final int charDistance = Browser._theBrowser.getFontMetrics(((Text)item).getFont()).stringWidth("X");
			final Line tokensToMove = Line.getLineFromToken(item);
			tokensToMove.removeFirst();
			boolean afterGap = false;
			for(int i = 1; i < tokensToMove.size();) {
				if(afterGap) tokensToMove.remove(i);
				else if((tokensToMove.get(i).getX() - charDistance) > (tokensToMove.get(i - 1).getX() + tokensToMove.get(i - 1).getBoundsWidth())) {
					afterGap = true;
					tokensToMove.remove(i);
				} else i++;
			}
			tokensToMove.deltaX(args[0].intValue());
			for(final Item token : tokensToMove) {
				if(BoxLogic.boxCollision(token)) {
					RepelTextDownAction.MergeSameLineParagraphCollisions = true;
					final RepelTextDownAction textDownAction = new RepelTextDownAction();
					textDownAction.setCallbackAction(this.callbackAction);
					textDownAction.exec(token);
					RepelTextDownAction.MergeSameLineParagraphCollisions = false;
					break;
				}
			}
		}
		


		
		callback(item);

		return true;
	}

	@Override
	public boolean invert(final Item item) {
		System.err.println("Repel text action does not have a inverted implementation.");
		return false;
	}
}
