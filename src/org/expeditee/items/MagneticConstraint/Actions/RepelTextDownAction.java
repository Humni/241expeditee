package org.expeditee.items.MagneticConstraint.Actions;

import java.awt.Point;

import org.expeditee.gui.DisplayIO;
import org.expeditee.gui.FrameIO;
import org.expeditee.gui.FrameUtils;
import org.expeditee.items.Item;
import org.expeditee.items.Text;
import org.expeditee.items.MagneticConstraint.MagneticConstraintAction;
import org.expeditee.items.MagneticConstraint.Utilities.Line;
import org.expeditee.items.MagneticConstraint.Utilities.Paragraph;
import org.expeditee.items.MagneticConstraint.Utilities.TextLogic;

public class RepelTextDownAction extends MagneticConstraintAction {
	
	public static boolean MergeSameLineParagraphCollisions = false;
	
	@Override
	public boolean exec(final Item item) {
		if(item == null || item.getParent() == null || !this.isSpIDERCodePage(item.getParent())) return false;		
		callback(item);	
		//Calculate content that needs to be moved to next line.
			//1. Get remainder of line
		final Line remainderOfLine = Line.getLineFromToken(item);
			//2. Check if the current item should be included; remove it if not.
		if(remainderOfLine.getFirst().getText().isEmpty() || 
				(FrameUtils.getCurrentItem().getPosition().equals(item.getPosition()) 
				&& remainderOfLine.size() > 0 
				&& TextLogic.XIsTowardsRight(DisplayIO.getMouseX(), (Text) remainderOfLine.getFirst()))) {
			remainderOfLine.removeFirst();
		}
			
		final Line currentLine = Line.getLineContainingToken(item);
		final Paragraph currentParagraph = Paragraph.getParagraphFromLine(currentLine);
		final Line nextLine = currentLine.next();
		
		final Point newLineAt;
		//Move content contained in 'remainderOfLine'

		
		if(!remainderOfLine.isEmpty() && currentParagraph.contains(nextLine) && MergeSameLineParagraphCollisions) { 
			nextLine.prependLine(remainderOfLine);
			DisplayIO.setTextCursor((Text) remainderOfLine.getFirst(), Text.HOME);
			callback(remainderOfLine.getFirst());
			FrameIO.SaveFrame(remainderOfLine.getLast().getParent());
		} else if(!remainderOfLine.isEmpty()) {
			newLineAt = currentParagraph.split(currentLine);
			final Point first = remainderOfLine.getBoundingBox().getLocation();
			for(final Item token : remainderOfLine) {
				double newX = token.getX() - first.getX() + newLineAt.getX();
				token.setPosition((float) newX, (float) newLineAt.getY());
			}
			DisplayIO.setTextCursor((Text) remainderOfLine.getFirst(), Text.HOME);
			callback(remainderOfLine.getFirst());
			FrameIO.SaveFrame(remainderOfLine.getLast().getParent());
		}else {
				//3a No. Move the cursor
			newLineAt = currentParagraph.split(currentLine);
			DisplayIO.setCursorPosition(newLineAt);
		}
		
		return true;
	}

	@Override
	public boolean invert(Item item) {
		System.err.println("Repel text down action does not have a inverted implementation.");
		return false;
	}

}
