package org.expeditee.items.MagneticConstraint.Actions;

import org.expeditee.gui.DisplayIO;
import org.expeditee.items.Item;
import org.expeditee.items.Text;
import org.expeditee.items.MagneticConstraint.MagneticConstraintAction;
import org.expeditee.items.MagneticConstraint.Utilities.Line;
import org.expeditee.items.MagneticConstraint.Utilities.Paragraph;
import org.expeditee.items.MagneticConstraint.Utilities.TextLogic;

public class DeleteAction extends MagneticConstraintAction {
	
	@Override
	public boolean exec(final Item item) {
		if(item.getParent() == null || !this.isSpIDERCodePage(item.getParent())) return false;
		if(item == null || ((Text) item).isEmpty()) {
			final Text temp = DisplayIO.getCurrentFrame().createNewText(".");
			callback(temp);
			if(temp.getMagnetizedItemRight() != -1) 
				DisplayIO.setTextCursor((Text) temp.getParent().getItemWithID(temp.getMagnetizedItemRight()), Text.HOME);
			else if(temp.getMagnetizedItemBottom() != -1) 
				DisplayIO.setTextCursor((Text) temp.getParent().getItemWithID(temp.getMagnetizedItemBottom()), Text.HOME);
			temp.delete();
			callback(temp);
			return true;
		} else if(item.getMagnetizedItemRight() != -1 && TextLogic.XIsAfterCharacters((Text) item, DisplayIO.getMouseX())) {
			new NextItemAction().exec(item);
			return true;
		} else if(item.getMagnetizedItemBottom() != -1 && TextLogic.XIsAfterCharacters((Text) item, DisplayIO.getMouseX())) {
			callback(item);
			final Line currentLine = Line.getLineFromToken(item);
			final Line nextLine = currentLine.next();
			if(nextLine != null && Paragraph.getParagraphFromLine(currentLine).contains(nextLine)) {
				currentLine.appendLine(nextLine);
				DisplayIO.setTextCursor((Text) item, Text.END);
				callback(item);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean invert(Item item) {
		System.err.println("Delete action does not have a inverted implementation.");
		return false;
	}

}
