package org.expeditee.items.MagneticConstraint.Actions;

import org.expeditee.gui.DisplayIO;
import org.expeditee.items.Item;
import org.expeditee.items.Text;
import org.expeditee.items.MagneticConstraint.MagneticConstraintAction;
import org.expeditee.items.MagneticConstraint.Utilities.Line;

public class UpALineAction extends MagneticConstraintAction {

	@Override
	public boolean exec(final Item item) {
		if(item.getParent() == null || !this.isSpIDERCodePage(item.getParent())) return false;
		final Line thisLine = Line.getLineContainingToken(item);
		final Item startOfThisLine = thisLine.get(0);
		final Item startOfLastLine = thisLine.last().get(0);
		if(startOfLastLine == null) return false;
		final Line lastLine = Line.getLineFromToken(startOfLastLine);
		final int lastLineLength = 
				lastLine.get(lastLine.size() - 1).getX() + lastLine.get(lastLine.size() - 1).getBoundsWidth() - lastLine.get(0).getX();
		final int distanceThroughCurrentLine = DisplayIO.getMouseX() - startOfThisLine.getX();
		if(distanceThroughCurrentLine <= lastLineLength) {
			Item above = lastLine.get(0);
			for(final Item token : lastLine) {
				if(token.getX() <= item.getX()) above = token;
				else break;
			}
			moveCursor(above);
		} else {
			moveCursor(lastLine.get(lastLine.size() - 1));
		}
		return true;
	}
	
	private boolean moveCursor(final Item toMoveTo) {
		if(toMoveTo == null) return false;
		if(toMoveTo instanceof Text) {
			final Text asText = (Text) toMoveTo;
			DisplayIO.setTextCursor(asText, Text.END);
		} else DisplayIO.setCursorPosition(toMoveTo.getPosition(), false);
		return true;
	}

	@Override
	public boolean invert(final Item item) {
		System.err.println("Up a line action does not have a inverted implementation.");
		return false;
	}	
}
