package org.expeditee.items.MagneticConstraint.Actions;

import java.util.Map;

import org.expeditee.items.Item;
import org.expeditee.items.MagneticConstraint.MagneticConstraintAction;
import org.expeditee.items.MagneticConstraint.MagneticConstraints;

public class PrintMagneticConstraintsLog extends MagneticConstraintAction {

	@Override
	public boolean exec(final Item item) {
		System.err.println("Log follows: ");
		for(final String s : MagneticConstraints.log)
			System.err.println(s);
		System.err.println();
		final Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
		for(final Thread thr : allStackTraces.keySet()) {
			System.err.println(thr.getName());
			final StackTraceElement[] stackTraceElements = allStackTraces.get(thr);
			for(final StackTraceElement ln : stackTraceElements) System.err.println(ln);
			System.err.println();
		}
		return true;
	}

	@Override
	public boolean invert(final Item item) {
		return false;
	}

}
