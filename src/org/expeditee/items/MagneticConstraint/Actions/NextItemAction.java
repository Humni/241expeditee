package org.expeditee.items.MagneticConstraint.Actions;

import org.expeditee.gui.DisplayIO;
import org.expeditee.items.Item;
import org.expeditee.items.Text;
import org.expeditee.items.MagneticConstraint.MagneticConstraintAction;
import org.expeditee.items.MagneticConstraint.MagneticConstraints;
import org.expeditee.items.MagneticConstraint.Utilities.Line;

public class NextItemAction extends MagneticConstraintAction {

	@Override
	public boolean exec(final Item item) {
		if(item.getParent() == null || !this.isSpIDERCodePage(item.getParent())) return false;
		final int idRight = item.getMagnetizedItemRight();
		final int idBottom = item.getMagnetizedItemBottom();
		if(idRight != -1) {
			moveCursor(item.getParent().getItemWithID(idRight));
			MagneticConstraints.Log(this.getClass(), new Item[]{item, item.getParent().getItemWithID(idRight)}, 
					Line.getLineContainingToken(item).toArray(new Item[]{}), 
					"NextItemAction does not move any items around.  Only moves the cursor.  Right neighbor found; second primary.");
			return true;
		} else if(idBottom != -1) {
			moveCursor(item.getParent().getItemWithID(idBottom));
			MagneticConstraints.Log(this.getClass(), new Item[]{item, item.getParent().getItemWithID(idBottom)}, 
					Line.getLineContainingToken(item).toArray(new Item[]{}), 
					"NextItemAction does not move any items around.  Only moves the cursor.  Bottom neighbor found; second primary.");
			return true;
		} else return false;
	}
	
	private boolean moveCursor(final Item toMoveTo) {
		if (toMoveTo == null)
			return false;
		//System.err.println("Next item requests move to item with text: " + toMoveTo.getText());
		DisplayIO.setTextCursor((Text)toMoveTo, Text.HOME);
		return true;
	}

	@Override
	public boolean invert(Item item) {
		System.err.println("Next item action does not have a inverted implementation.");
		return false;
	}
}
