package org.expeditee.items.MagneticConstraint;

import org.expeditee.items.Item;

public abstract class MagneticConstraintActionWithArgumentsAndReturnType<ArgType, ReturnType> extends MagneticConstraintAction {

	@Override
	public boolean exec(Item item) {
		return false;
	}

	abstract public ReturnType exec(final Item item, final ArgType args);
}
