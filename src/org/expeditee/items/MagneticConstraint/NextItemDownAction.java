///**
// *    NextItemDownAction.java
// *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
// *
// *    This program is free software: you can redistribute it and/or modify
// *    it under the terms of the GNU General Public License as published by
// *    the Free Software Foundation, either version 3 of the License, or
// *    (at your option) any later version.
// *
// *    This program is distributed in the hope that it will be useful,
// *    but WITHOUT ANY WARRANTY; without even the implied warranty of
// *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// *    GNU General Public License for more details.
// *
// *    You should have received a copy of the GNU General Public License
// *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
// */
//
//package org.expeditee.items.MagneticConstraint;
//
//import org.expeditee.gui.DisplayIO;
//import org.expeditee.items.Item;
//
//public class NextItemDownAction implements MagneticConstraintAction {
//
//	@Override
//	public boolean exec(Item item) {
//		final Item below = item.getParent().getItemWithID(
//				item.getMagnetizedItemBottom());
//		return moveCursor(below);
//	}
//
//	private boolean moveCursor(final Item toMoveTo) {
//		if (toMoveTo == null)
//			return false;
//		DisplayIO.setCursorPosition(toMoveTo.getPosition(), true);
//		return true;
//	}
//
//}
