/**
 *    DotType.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.items;

public enum DotType {
	square, circle, diamond, roundSquare, triangle;
	
	/**
	 * Converts the given Expeditee justification code into a int corresponding
	 * to the constants defined in Item.
	 * 
	 * @param justCode
	 *            The Expeditee justification code to convert
	 * @return The resulting int corresponding to one of the constants defined
	 *         in Item
	 */
	public static DotType convertString(String justCode) {
		assert (justCode != null);
		justCode = justCode.trim().toLowerCase();

		// if it is a single char just match the first character
		if (justCode.length() == 1) {
			char code = justCode.charAt(0);
			DotType[] values = values();
			for (int i = 0; i < values.length; i++) {
				DotType j = values[i];
				if (Character.toLowerCase(j.name().charAt(0)) == code)
					return j;
			}
			// Otherwise match the whole string
		} else {
			try {
				return valueOf(justCode);
			} catch (Exception e) {
			}
		}

		// default justification
		return square;
	}
	
	public char getCode() {
		return Character.toUpperCase(this.toString().charAt(0));
	}

	public String toString() {
		return this.name();
	}
}
