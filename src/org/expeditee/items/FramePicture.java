/**
 *    FramePicture.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.items;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.ImageObserver;

import org.expeditee.gui.AttributeValuePair;
import org.expeditee.gui.Frame;

public abstract class FramePicture extends Picture {
	protected FramePicture(Text source, ImageObserver observer, Image image) {
		super(source, observer, image);
	}

	@Override
	public void setColor(Color c) {
		super.setColor(c);
		refresh();
	}
	
	@Override
	public void setLink(String frameName) {
		if (_source == null)
			return;
		else {
			_source.setLink(frameName);
			//remove the picture if the link is being removed
			if (_source.getLink() == null) {
				Frame parent = getParent();
				if (parent != null) {
					parent.removeItem(this);
					_source.setHidden(false);
					_source.removeEnclosure(this);
				}
			} else {
				refresh();
			}
		}
		updatePolygon();
	}
	
	@Override
	public String getName() {
		return _source.getAbsoluteLink();
	}
	
	@Override
	protected String getImageSize() {
		return new AttributeValuePair(_source.getText()).getValue();
	}
}
