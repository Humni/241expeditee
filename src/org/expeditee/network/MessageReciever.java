/**
 *    MessageReciever.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.network;

import java.io.IOException;
import java.net.DatagramPacket;

import org.expeditee.gui.MessageBay;

public class MessageReciever extends DefaultServer {
	public final static int OFFSET = 2;
	
	public MessageReciever(int port) throws IOException {
		super("MessageReciever", port + OFFSET);
	}

	public void listenForMessages() throws IOException {
		byte[] buf = new byte[FrameServer.MAX_PACKET_LENGTH];

		// receive request
		DatagramPacket packet = new DatagramPacket(buf, buf.length);
		socket.receive(packet);

		String packetContents = new String(packet.getData(), 0, packet
				.getLength());
		String sendersName = FrameShare.getInstance().getPeerName(
				packet.getPort() + 1, packet.getAddress());
		/**
		 * If the sender of the message is not on our peer list then use their
		 * IP addy.
		 */
		if (sendersName == null)
			sendersName = packet.getAddress().toString();

		packetContents = sendersName + " says: " + packetContents;
		MessageBay.displayMessage(packetContents);
	}
}
