/**
 *    Peer.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.network;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.expeditee.gui.AttributeValuePair;
import org.expeditee.gui.MessageBay;

public class Peer {
	public static final int DEFAULT_PORT = 3000;

	private String name_ = null;

	private int port_ = DEFAULT_PORT;

	private InetAddress ip_ = null;

	public Peer(AttributeValuePair avp) throws UnknownHostException {
		name_ = avp.getAttribute();
		String[] address = avp.getValue().split("\\s+");
		
		ip_ = InetAddress.getByName(address[0]);
		
		if (address.length > 1) {
			try {
				port_ = Integer.parseInt(address[1]);
			} catch (Exception e) {
				MessageBay.errorMessage("Could not parse port in ["
						+ avp.toString() + "]");
			}
		}
	}

	public int getPort() {
		return port_;
	}

	public InetAddress getAddress() {
		return 	ip_;
	}

	public String getName() {
		return name_;
	}
}
