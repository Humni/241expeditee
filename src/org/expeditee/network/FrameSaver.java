/**
 *    FrameSaver.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.DatagramPacket;

import org.expeditee.gui.FrameIO;
import org.expeditee.gui.MessageBay;

public class FrameSaver extends DefaultServer {
	public final static int OFFSET = 1;

	public FrameSaver(int port) throws IOException {
		super("FrameSaver", port + OFFSET);
	}

	@Override
	protected void listenForMessages() throws IOException {
		byte[] buf = new byte[MAX_PACKET_LENGTH];

		// receive request
		DatagramPacket packet = new DatagramPacket(buf, buf.length);
		socket.receive(packet);

		BufferedReader packetContents = new BufferedReader(new StringReader(
				new String(packet.getData(), 0, packet.getLength())));

		// Get the name of the frame
		String frameName = packetContents.readLine();
		int version = Integer.parseInt(packetContents.readLine());

		MessageBay.displayMessage("Recieved request to save " + frameName);

		// TODO: find out what's causing a NullPointerException here
		try {
			FrameIO.SavePublicFrame(FrameShare.getInstance().getPeerName(
				packet.getPort() + 2, packet.getAddress()), frameName, version,
				packetContents);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}
