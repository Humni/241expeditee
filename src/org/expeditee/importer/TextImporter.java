/**
 *    TextImporter.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.importer;

import java.awt.Color;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.expeditee.gui.FrameCreator;
import org.expeditee.gui.FrameGraphics;
import org.expeditee.gui.MessageBay;
import org.expeditee.items.Item;
import org.expeditee.items.Text;

public class TextImporter implements FileImporter {

	public TextImporter() {
		super();
	}

	public Item importFile(final File f, Point location) throws IOException {
		if (location == null || f == null) {
			return null;
		}
		final String fullPath = f.getAbsolutePath();

		final Text source = FrameDNDTransferHandler.importString(f.getPath(),
				location);

		// Create a frameCreator to write the text
		final FrameCreator frames = new FrameCreator(f.getName());

		new Thread() {
			public void run() {
				try {
					// Open a file stream to the file
					BufferedReader br = new BufferedReader(new FileReader(
							fullPath));

					MessageBay.displayMessage("Importing " + f.getName() + "...");

					// Read in the text
					String nextLine;
					while ((nextLine = br.readLine()) != null) {
						frames.addText(nextLine, null, null, null, false);
					}

					frames.save();
					source.setLink(frames.getName());
					MessageBay.displayMessage(f.getName() + " import complete", Color.GREEN);
					FrameGraphics.requestRefresh(true);
				} catch (Exception e) {
					e.printStackTrace();
					MessageBay.errorMessage(e.getMessage());
				}
			}
		}.start();
		FrameGraphics.refresh(true);
		return source;
	}
}
