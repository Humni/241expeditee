/**
 *    Overlay.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.gui;

import java.util.Collection;

import org.expeditee.items.UserAppliedPermission;

public class Overlay {
	public Frame Frame;

	public UserAppliedPermission permission;

	public Overlay(Frame overlay, UserAppliedPermission level) {
		Frame = overlay;
		permission = level;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || o.getClass() != Overlay.class)
			return false;

		return ((Overlay) o).Frame == Frame;
	}

	@Override
	public int hashCode() {
		return 0;
	}
	
	public static Overlay getOverlay(Collection<Overlay>overlays, Frame frame){
		// Check the frame is in the list of overlays
		for (Overlay o : overlays) {
			if (o.Frame.equals(frame)) {
				return o;
			}
		}
		return null;
	}
}
