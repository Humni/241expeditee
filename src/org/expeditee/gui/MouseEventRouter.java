/**
 *    MouseEventRouter.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.gui;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Event;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JComponent;
import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import org.expeditee.io.ExpClipReader;
import org.expeditee.io.ItemSelection;
import org.expeditee.io.ItemSelection.ExpDataHandler;
import org.expeditee.items.Item;
import org.expeditee.items.ItemUtils;

/**
 * The gateway for mouse input; conditionally forwards mouse messages to swing
 * components / expeditee frames for the Browser.
 * 
 * Various mouse listeners can attatch themselves here to listen to mouse events
 * forwared to expeditee .. this excludes widget-exclusive mouse messages.
 * 
 * @author Brook Novak
 * 
 */
public class MouseEventRouter extends JComponent {

	private static final long serialVersionUID = 1L;

	private JMenuBar _menuBar;

	private Container _contentPane;

	private List<MouseListener> _mouseListeners = new LinkedList<MouseListener>();

	private List<MouseMotionListener> _mouseMotionListeners = new LinkedList<MouseMotionListener>();

	private List<MouseWheelListener> _mouseWheelListeners = new LinkedList<MouseWheelListener>();

	private static MouseEvent _currentMouseEvent = null;
	
	/*
	 * Custom events
	 */
	public boolean released = false;
    public boolean longTouch = false;
    public Timer longTimer = new Timer();
    public Timer countTimer = new Timer();
    public MouseEvent ePress;
    public boolean doubleTouch = false;
    public boolean tripleTouch = false;
    public boolean pressed = false;
    public int pressCounter = 0;
    public boolean waitingForPress = false;
    public boolean simulated = false;

	/**
	 * Routes events on given frame layers... the menu bar and content pane
	 * 
	 * @param menuBar
	 *            Must not be null.
	 * 
	 * @param contentPane
	 *            Must not be null.
	 */
	public MouseEventRouter(JMenuBar menuBar, Container contentPane) {
		if (contentPane == null)
			throw new NullPointerException("contentPane");

		// Listen for all AWT events (ensures popups are included)
		Toolkit.getDefaultToolkit().addAWTEventListener(
				new EventCatcher(),
				AWTEvent.MOUSE_MOTION_EVENT_MASK | AWTEvent.MOUSE_EVENT_MASK
						| AWTEvent.MOUSE_WHEEL_EVENT_MASK);

		this._menuBar = menuBar;
		this._contentPane = contentPane;
	}

	/**
	 * Listens only to events to frames... i.e. to exeditee, not to widgets.
	 * 
	 * @param listener
	 *            The listener to add.
	 */
	public void addExpediteeMouseListener(MouseListener listener) {
		if (listener == null)
			throw new NullPointerException("listener");
		_mouseListeners.add(listener);
	}

	public void removeExpediteeMouseListener(MouseListener listener) {
		if (listener == null)
			throw new NullPointerException("listener");
		_mouseListeners.remove(listener);
	}

	public void addExpediteeMouseMotionListener(MouseMotionListener listener) {
		if (listener == null)
			throw new NullPointerException("listener");
		_mouseMotionListeners.add(listener);
	}

	public void removeExpediteeMouseMotionListener(MouseMotionListener listener) {
		if (listener == null)
			throw new NullPointerException("listener");
		_mouseMotionListeners.remove(listener);
	}

	public void addExpediteeMouseWheelListener(MouseWheelListener listener) {
		if (listener == null)
			throw new NullPointerException("listener");
		_mouseWheelListeners.add(listener);
	}

	public void removeExpediteeMouseWheelListener(MouseWheelListener listener) {
		if (listener == null)
			throw new NullPointerException("listener");
		_mouseWheelListeners.remove(listener);
	}

	/**
	 * Conceal event catching from outside
	 * 
	 * @author Brook Novak
	 */
	private class EventCatcher implements AWTEventListener {

		/**
		 * All events for every component in frame are fired to here
		 */
		public void eventDispatched(AWTEvent event) {
			if (event instanceof MouseEvent) {
				routeMouseEvent((MouseEvent) event);
			}
		}

	}

	/**
	 * Forwards the mouse event to its appropriate destination. For events that
	 * belong to Expiditee space the events are consumed and manually routed to
	 * the mouse actions handler.
	 * 
	 * @param e
	 *            The mouse event for any component in the browser frame
	 */
	private void routeMouseEvent(MouseEvent e) {
		_currentMouseEvent = e;

		// First convert the point to expeditee space
		Point containerPoint = SwingUtilities.convertPoint(e.getComponent(), e
				.getPoint(), _contentPane);
		
		// TODO: Find a reliable way of detecting when the mouse moved onto a window that isn't a child of ours
		if(e.getID() == MouseEvent.MOUSE_EXITED) {
			// System.out.println(e.getComponent());
			if(containerPoint.x <= 0 || containerPoint.x >= _contentPane.getWidth() ||
					containerPoint.y <= 0 || containerPoint.y >= _contentPane.getHeight()) {
				FrameMouseActions.mouseExitedWindow(e);
			}
		}

		if (containerPoint.y < 0) { // not in the content pane

			if (_menuBar != null
					&& containerPoint.y + _menuBar.getHeight() >= 0) {
				// The mouse event is over the menu bar.
				// Could handle specially.
			} else {
				// The mouse event is over non-system window
				// decorations, such as the ones provided by
				// the Java look and feel.
				// Could handle specially.
			}

		} else {

			// Check to see if the mouse is over an expeditee item or
			// whether an expeditee item is currently picked up
			boolean forwardToExpiditee = false;
			boolean isOverPopup = PopupManager.getInstance().isPointOverPopup(
					containerPoint);
			if (isOverPopup) {
				// Popups have highest preference
				// forwardToExpiditee = false...

				// If there ate items in free space - keep them moving with the
				// cursor over the
				// popups.
				if (!FreeItems.getInstance().isEmpty()) {
					FrameMouseActions.move(FreeItems.getInstance());
				}

				// Note: all frame.content pane events belong to expeditee
			} else if (e.getSource() == _contentPane
					|| e.getSource() == Browser._theBrowser
					|| !FreeItems.getInstance().isEmpty()) {
				forwardToExpiditee = true;
			} else if (DisplayIO.getCurrentFrame() != null) {
				/* is mouse over a specific expeditee item? */
				// NOTE: Do not use FrameUtils.getCurrentItem() - thats relevent
				// for old mouse position only
				/*
				 * for (Item i : DisplayIO.getCurrentFrame().getItems()) { if
				 * (i.getPolygon().contains(containerPoint)) {
				 * forwardToExpiditee = true; break; } }
				 */// ABOVE: Does not consider overlays
				// NOTE: Below is an expensive operation and could be re-used
				// when passing mouse events!!!
				forwardToExpiditee = (FrameUtils.onItem(DisplayIO
						.getCurrentFrame(), containerPoint.x, containerPoint.y,
						true) != null);
			}

			// Create artificial mouse event and forward it to expeditee
			MouseEvent expediteeEvent = SwingUtilities.convertMouseEvent(e
					.getComponent(), e, _contentPane);

			// NOTE: Convert util masks-out the needed extensions
			MouseEvent withExtensions = null;

			if (forwardToExpiditee) {

				// Ensure that underlying widgets don't get the event
				e.consume();

				switch (e.getID()) {
				case MouseEvent.MOUSE_MOVED:

					for (MouseMotionListener listener : _mouseMotionListeners) {
						listener.mouseMoved(expediteeEvent);
					}

					// Ensure that expiditee has focus only if no popups exist
					if (Browser._theBrowser != null
							&& Browser._theBrowser.getContentPane() != null) {
						if (!Browser._theBrowser.getContentPane()
								.isFocusOwner()
								&& !isPopupVisible()) {
							Browser._theBrowser.getContentPane().requestFocus();
						}
					}

					break;

				case MouseEvent.MOUSE_CLICKED:
					if(!waitingForPress || simulated){
						/*System.out.println("component: " + e.getComponent());
						System.out.println("button: " + e.getID());
						System.out.println("clickcount: " + e.getClickCount());
						System.out.println("popup trigger: " + e.isPopupTrigger());
						System.out.println("buttons: " + e.getButton());
						System.out.println("modifiers: " + e.getModifiers());*/
						if(simulated){
							if(e.getButton() == 2)
	            				System.out.println("middle press");
	            			else if(e.getButton() == 3)
	            				System.out.println("right press");
						}
						withExtensions = duplicateMouseEvent(expediteeEvent, e
								.getModifiers()
								| e.getModifiersEx());
	
						for (MouseListener listener : _mouseListeners) {
							listener.mouseClicked(withExtensions);
						}
					}
					break;

				case MouseEvent.MOUSE_PRESSED:
					/*System.out.println("component: " + e.getComponent());
					System.out.println("button: " + e.getID());
					System.out.println("clickcount: " + e.getClickCount());
					System.out.println("popup trigger: " + e.isPopupTrigger());
					System.out.println("buttons: " + e.getButton());
					System.out.println("modifiers: " + e.getModifiers());
					
					withExtensions = duplicateMouseEvent(expediteeEvent, e
    					.getModifiers()
    					| e.getModifiersEx());

	    			for (MouseListener listener : _mouseListeners) {
	    				listener.mousePressed(withExtensions);
	    			}/**/
					released = false;
					
					//long touch code
					if(simulated){
						withExtensions = duplicateMouseEvent(expediteeEvent, e
            					.getModifiers()
            					| e.getModifiersEx());

            			for (MouseListener listener : _mouseListeners) {
            				listener.mousePressed(withExtensions);
            			}

            			if(e.getButton() == 2)
            				System.out.println("middle press");
            			else if(e.getButton() == 3)
            				System.out.println("right press");
            			break;
					}
					
					if(waitingForPress){
						pressCounter ++;
						waitingForPress = false;
						countTimer.cancel();
					} else {
						pressCounter = 0;
					}

					longTouch = false;
					longTimer = new Timer();
					longTimer.schedule(new TimerTask()
	                {
	                    public void run()
	                    {
	                    	if(released == false && pressCounter < 1)
	                    	{
	                    		System.out.println("long touch");
	                    		longTouch = true;
	                    		
	                    	} else {
	                    		longTimer.cancel();
	                    	}
	                    }
	                },1300);
					//long touch end
					ePress = e;//*/
					break;
				case MouseEvent.MOUSE_RELEASED:
					/*System.out.println("component: " + e.getComponent());
					System.out.println("button: " + e.getID());
					System.out.println("clickcount: " + e.getClickCount());
					System.out.println("popup trigger: " + e.isPopupTrigger());
					System.out.println("buttons: " + e.getButton());
					System.out.println("modifiers: " + e.getModifiers());
					
					withExtensions = duplicateMouseEvent(expediteeEvent, e
    					.getModifiers()
    					| e.getModifiersEx());

	    			for (MouseListener listener : _mouseListeners) {
	    				listener.mouseReleased(withExtensions);
	    			}/**/
					
					//long touch code
					released = true;
					longTimer.cancel();
					
					if(simulated){
						withExtensions = duplicateMouseEvent(expediteeEvent, e
            					.getModifiers()
            					| e.getModifiersEx());

            			for (MouseListener listener : _mouseListeners) {
            				listener.mouseReleased(withExtensions);
            			}
            			
            			if(e.getButton() == 2)
            				System.out.println("middle release");
            			else if(e.getButton() == 3)
            				System.out.println("right release");
            				
            			break;
					}


	                if(longTouch == true)
	                {
	                	longTouch = false;
	                }
					//long touch end
	                else {
	                	//Double tap code
	                	waitingForPress = true;
	                	countTimer = new Timer();
	                	countTimer.schedule(new TimerTask(){
	                		public void run(){
	                			waitingForPress = false;
	                			
	    	                	if(pressCounter == 0){
	    	                		if(e != null){
	    	                			final MouseEvent withExtensions = duplicateMouseEvent(expediteeEvent, e
	    	                					.getModifiers()
	    	                					| e.getModifiersEx());

	    	                			for (MouseListener listener : _mouseListeners) {
	    	                				listener.mousePressed(withExtensions);
	    	                			}
	    	                    	}
	    	                    
	    	                		final MouseEvent withExtensions = duplicateMouseEvent(expediteeEvent, e
	    	                				.getModifiers()
	    	                				| e.getModifiersEx());

	    	                		for (MouseListener listener : _mouseListeners) {
	    	                			listener.mouseReleased(withExtensions);
	    	                		}
	    	                	} else if( pressCounter == 1){
	    	                		System.out.println("Double Touch " + doubleTouch);
	    	                		//if it is not double touching
	    	                		if(!doubleTouch){
		    	                		doubleTouch = true;
		    	                		simulated = true;
		    	                		MouseEvent event;
		    	                		event = new MouseEvent(e.getComponent(), 501, e.getWhen(), 8, e.getX(), e.getY(), 1, false, 2);
		    	                		routeMouseEvent(event);
		    	                		event = new MouseEvent(e.getComponent(), 502, e.getWhen(), 8, e.getX(), e.getY(), 1, false, 2);
		    	                		routeMouseEvent(event);
		    	            			simulated = false;
	    	                		} else {
		    	                		doubleTouch = false;
		    	                		simulated = true;
		    	                		MouseEvent event;
		    	                		event = new MouseEvent(e.getComponent(), 501, e.getWhen(), 8, e.getX(), e.getY(), 1, false, 2);
		    	                		routeMouseEvent(event);
		    	                		event = new MouseEvent(e.getComponent(), 502, e.getWhen(), 8, e.getX(), e.getY(), 1, false, 2);
		    	                		routeMouseEvent(event);
		    	            			simulated = false;
	    	                		}
	    	                	} else if( pressCounter >= 2){
	    	                		System.out.println("Triple touch " + tripleTouch);
	    	                		//if it is not double touch
	    	                		simulated = true;
	    	                		MouseEvent event;
	    	                		event = new MouseEvent(e.getComponent(), 501, e.getWhen(), 8, e.getX(), e.getY(), 1, false, 2);
	    	                		routeMouseEvent(event);
	    	                		event = new MouseEvent(e.getComponent(), 501, e.getWhen(), 4, e.getX(), e.getY(), 1, false, 3);
	    	                		routeMouseEvent(event);
	    	                		event = new MouseEvent(e.getComponent(), 502, e.getWhen(), 4, e.getX(), e.getY(), 1, false, 3);
	    	                		routeMouseEvent(event);
	    	                		event = new MouseEvent(e.getComponent(), 500, e.getWhen(), 4, e.getX(), e.getY(), 1, false, 3);
	    	                		routeMouseEvent(event);
	    	                		event = new MouseEvent(e.getComponent(), 500, e.getWhen(), 8, e.getX(), e.getY(), 1, false, 2);
	    	                		routeMouseEvent(event);
	    	                		event = new MouseEvent(e.getComponent(), 502, e.getWhen(), 8, e.getX(), e.getY(), 1, false, 2);
	    	                		routeMouseEvent(event);
	    	            			simulated = false;
	    	                	}
	                		}
	                	}, 250);
	                }//*/
	                	//Double tap end
					break;
				case MouseEvent.MOUSE_WHEEL:
					MouseWheelEvent mwe = (MouseWheelEvent) expediteeEvent;
					for (MouseWheelListener listener : _mouseWheelListeners) {
						listener.mouseWheelMoved(mwe);
					}
					break;
				case MouseEvent.MOUSE_ENTERED:
					withExtensions = duplicateMouseEvent(expediteeEvent, e
							.getModifiers()
							| e.getModifiersEx());
					for (MouseListener listener : _mouseListeners) {
						listener.mouseEntered(withExtensions);
					}
					break;
				case MouseEvent.MOUSE_EXITED:
					for (MouseListener listener : _mouseListeners) {
						listener.mouseExited(withExtensions);
					}
					break;
				case MouseEvent.MOUSE_DRAGGED:
					for (MouseMotionListener listener : _mouseMotionListeners) {
						listener.mouseDragged(expediteeEvent);
					}
					break;
				}

			} else {

				// Keep expeditees mouse X/Y updated
				FrameMouseActions.MouseX = expediteeEvent.getX();
				FrameMouseActions.MouseY = expediteeEvent.getY();

				// If forwarding to swing ensure that widgets are not
				// highlighted
				// to give visual feedback yo users such that swing has focus.
				Item i = FrameMouseActions.getlastHighlightedItem();
				if (i != null
						&& i.getHighlightMode() != Item.HighlightMode.None) {
					FrameGraphics.changeHighlightMode(i,
							Item.HighlightMode.None);
				}

				// Also bring expideditee behaviour to swing: Auto-focus on
				// component
				// whenever the mouse moves over it.
				if (e.getID() == MouseEvent.MOUSE_MOVED) {
					if (e.getSource() instanceof Component) {
						Component target = (Component) e.getSource();
						if (!target.isFocusOwner()) {
							target.requestFocus();
						}
					}
					// Auto-hide popups when user click on something other other
					// than
					// a popup - and is not a on a popup invoker
				} else if (!isOverPopup
						&& e.getID() == MouseEvent.MOUSE_PRESSED
						&& !PopupManager.getInstance().isInvoker(
								e.getComponent())) {
					PopupManager.getInstance().hideAutohidePopups();
				}
			}
		}
		
		Help.updateStatus();
	}

	public static boolean isPopupVisible() {
		return isPopupVisible(Browser._theBrowser.getLayeredPane());
	}

	private static boolean isPopupVisible(Container parent) {

		for (Component c : parent.getComponents()) {

			if (c instanceof JPopupMenu && ((JPopupMenu) c).isVisible()) {
				return true;

			} else if (c instanceof Container
					&& c != Browser._theBrowser.getContentPane()) {
				if (isPopupVisible((Container) c))
					return true;
			}
		}

		return false;
	}

	private MouseEvent duplicateMouseEvent(MouseEvent e, int modifiers) {
		return new MouseEvent(e.getComponent(), e.getID(), e.getWhen(),
				modifiers, e.getX(), e.getY(),
				/** The below methods are not compatible with Java 1.5 */
				/*
				 * e.getXOnScreen(), e.getYOnScreen(),
				 */
				e.getClickCount(), e.isPopupTrigger(), e.getButton());
	}

	public static MouseEvent getCurrentMouseEvent() {

		return _currentMouseEvent;

	}
}