package org.expeditee.gui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;

import org.expeditee.items.Item;

//Manages slide animations between frames
//Reflection is used to call these methods, therefore any additions should only need to be added in this class.
//No further changes to other code should be required.
public class FrameTransitions {	
	
	//Stores the frame before the new frame being transitioned to
	private static Image _slideModeBaseImage = null;
			
	//Tells graphics which method of transition is going to be used
	private static String _slideModeMethod;
	
	//Used for power point slide effects
	private static boolean slide = false;
		
	public static Boolean getSlide(){
		
		return slide;
	}
	public static void setSlideFalse(){
		
		slide = false;
	}
	public static void setSlideTrue(){
		
		slide = true;
	}
	public static void setSlideModeBaseImage(Image i){
		
		_slideModeBaseImage = i;
	}
	public static Image getSlideModeBaseImage(){
		
		return _slideModeBaseImage;		
	}
	public static void setSlideModeMethod(String s){
		
		_slideModeMethod = s;
	}
	public static String getslideModeMethod(){
		
		return _slideModeMethod;
	}
	
	//Slides new frame over current from from Right to Left
	public static void Swipe(Graphics g, Image i, Dimension d){
		
		Image image = i;
		
		int frameX = d.width;
		
		while(frameX != 0){
			
			g.drawImage(image, frameX, 0, Item.DEFAULT_BACKGROUND, null);
								
			frameX--;								
		}	
	}
	//Drops new frame from top of display
	//Draws rectangle with gradient effect to create illusion of drop shadow	
	public static void FallDown(Graphics g, Image i, Dimension d){
		
		Graphics2D g2 = (Graphics2D) g;
	    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	    RenderingHints.VALUE_ANTIALIAS_ON);

	    Image image = i;		
		int frameY = 0 - d.height;
		int vertCount = 0;
		
		while(frameY != 0){
			
			GradientPaint shadow = new GradientPaint(0, vertCount + 10, Color.WHITE, 0, vertCount, Color.gray);
		    g2.setPaint(shadow);
			g.drawImage(image,  0,  frameY,  Item.DEFAULT_BACKGROUND,  null);	
			g2.fillRect(0, vertCount, d.width, 10);		
			
			frameY++;
			vertCount++;
			try {
				Thread.sleep(3);
			} catch (InterruptedException e) {
			
				e.printStackTrace();
			}			
		}        
	}		

	//Use synchronized to ensure no issues with concurrent update issues	
	//Fades out current frame to black, fades new slide in from black
	public static void FadeInBlack(Graphics g, Image i, Dimension d){		
	
		String fadeType = "black";
		float alpha = 1.0f;		
		Graphics2D g2d = (Graphics2D) g;		
		
		while(alpha >= 0.3f){
			
			g.drawImage(FrameTransitions.getSlideModeBaseImage(),  0,  0,  Item.DEFAULT_BACKGROUND,  null);			
			g2d.setComposite(alphaCalcUp(alpha, fadeType));				
			g2d.fillRect(0, 0, d.width, d.height);
			alpha = alphaCalcUp(alpha, fadeType).getAlpha();
			
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
			
				e.printStackTrace();
			}			
		}		
		while(alpha <= 0.8f){			
		
			g.drawImage(i,  0,  0,  Item.DEFAULT_BACKGROUND,  null);
			g2d.setComposite(alphaCalcDown(alpha, fadeType));			
			g2d.fillRect(0, 0, d.width, d.height);
			alpha = alphaCalcDown(alpha, fadeType).getAlpha();
			
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}	
		}       
	}
	//Fades current frame out, and new frame in
	//Calls AlphaCalcUp and AlphaCalcDown to calculate Alpha values of these transitions
	//AlphaComposite is passed in as a parameter, and it holds the alpha value
	public static void FadeInWhite(Graphics g, Image i, Dimension d){
		
		String fadeType = "white";
		float alpha = 1.0f;
		
		Graphics2D g2d = (Graphics2D) g;		
		
		while(alpha >= 0.1f){			
			
			g.drawImage(FrameTransitions.getSlideModeBaseImage(),  0,  0,  Item.DEFAULT_BACKGROUND,  null);			
			g2d.setComposite(alphaCalcUp(alpha, fadeType));				
			g2d.setColor(Color.WHITE);
			g2d.fillRect(0, 0, d.width, d.height);
			alpha = alphaCalcUp(alpha, fadeType).getAlpha();
			
			try {
				Thread.sleep(25);
			} catch (InterruptedException e) {
			
				e.printStackTrace();
			}			
			
			System.out.println(alpha);
		}		
		while(alpha <= 0.75f){			
		
			g.drawImage(i,  0,  0,  Item.DEFAULT_BACKGROUND,  null);
			g2d.setComposite(alphaCalcDown(alpha, fadeType));
			g2d.setColor(Color.WHITE);
			g2d.fillRect(0, 0, d.width, d.height);
			alpha = alphaCalcDown(alpha, fadeType).getAlpha();
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}	
			
			System.out.println(alpha);
		} 
	}
	//Calculates alpha values for transitions from transparent to opaque
	//AlphaComposite is returned
	private static AlphaComposite alphaCalcUp(float i, String fadeType){
		
		float alpha = i;
		float increment = -0.05f;
		alpha += increment;		

		if(fadeType == "black"){
		
			AlphaComposite alcom = AlphaComposite.getInstance(AlphaComposite.DST_IN, alpha);
			return alcom;		
		}
		else{
			//fadeType White
			AlphaComposite alcom = AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, alpha);
			return alcom;
		}
	}
	//Calculates alpha values for transitions from opaque to transparent	
	//AlphaComposite is returned
	private static AlphaComposite alphaCalcDown(float i, String fadeType){

		float alpha = i;
		float increment = -0.05f;
		alpha -= increment;		

		if(fadeType == "black"){
		
			AlphaComposite alcom = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
			return alcom;		
		}
		else{
			//fadeType White
			//AlphaComposite alcom = AlphaComposite.SrcOver.derive(alpha);
			AlphaComposite alcom = AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, alpha);
			return alcom;
		}
	}
}
