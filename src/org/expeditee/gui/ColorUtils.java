/**
 *    ColorUtils.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.gui;

import java.awt.Color;

public class ColorUtils {

	/**
	 * Gets the next color in the specified color wheel.
	 * 
	 * @param color
	 *            the current color
	 * @param wheel
	 *            the color wheel from which to find the next color
	 * @param skip
	 *            a color to ignore, for example the current background color
	 *            when getting the next foreground color or null if no colors
	 *            should be skipped
	 * @return the next color on the color wheel
	 */
	public static Color getNextColor(Color color, Color[] wheel, Color skip) {
		if (color == null) {
			color = wheel[0];
		} else {
			// search through the colour wheel to find the next colour
			int pos = -1;
			for (int i = 0; i < wheel.length; i++) {
				if (color.equals(wheel[i])) {
					pos = i;
					break;
				}
			}
			pos++;
			pos = pos % wheel.length;
			color = wheel[pos];
		}
		
		if (skip != null && skip.equals(color))
			return getNextColor(color, wheel, skip);

		return color;
	}
}
