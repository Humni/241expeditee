/**
 *    Vector.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.text.NumberFormat;

import org.expeditee.items.Item;
import org.expeditee.items.UserAppliedPermission;

public class Vector extends Overlay {

	public Point Origin;

	public float Scale;

	public Color Foreground;

	public Color Background;
	
	public Item Source;
	
	public Dimension Size;

	public Vector(Frame overlay, UserAppliedPermission permission,
			Float scale, Item source) {
		super(overlay, permission);
		Origin = source.getPosition();
		Scale = scale;
		Foreground = source.getColor();
		Background = source.getBackgroundColor();
		Source = source;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || o.getClass() != Vector.class)
			return false;
		Vector v = (Vector) o;

		return v.Frame == Frame && Origin.equals(v.Origin)
				&& Scale == v.Scale && Foreground == v.Foreground
				&& Background == v.Background;
	}

	@Override
	public int hashCode() {
		return 0;
	}

	/**
	 * Converts the given x position to be relative to the overlay frame.
	 * 
	 * @param x
	 * @return
	 */
	public float getX(int x) {
		return (x - Origin.x) / Scale;
	}

	public float getY(int y) {
		return (y - Origin.y) / Scale;
	}

	public void setSize(int maxX, int maxY) {
		Size = new Dimension(maxX, maxY);
	}

	public static NumberFormat getNumberFormatter() {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(4);
		return nf;
	}
}
