package org.expeditee.gui.indirect.keyboard;

import org.expeditee.items.Text;

public interface KeyboardAction {
	public Text exec(final KeyboardInfo info, char c);
}
