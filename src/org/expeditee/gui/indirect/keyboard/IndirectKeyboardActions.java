package org.expeditee.gui.indirect.keyboard;

public class IndirectKeyboardActions {

	private enum Action {
		//Function Keys
		DropDown, SizeUp, SizeDown, ChangeColor, ToggleAnnotation, InsertDate, NewFrameset, ChangeFontStyle, ChangeFontFamily,
		AudienceMode, XRayMode, Save, Refresh,
		//Process character
		NewText, InsertCharacter
	}
	
	private static final int ACTIONQUANTITY = Action.values().length;
	
	private static IndirectKeyboardActions instance = new IndirectKeyboardActions();
	
	private IndirectKeyboardActions() { }
	
	public static IndirectKeyboardActions getInstance() { return instance; }
	
	public KeyboardAction[] actions = new KeyboardAction[ACTIONQUANTITY];
	
	public KeyboardAction getDropDownAction() { return actions[Action.DropDown.ordinal()]; }
	public KeyboardAction setDropDownAction(final KeyboardAction action) { actions[Action.DropDown.ordinal()] = action; return action; }
	
	public KeyboardAction getSizeUpAction() { return actions[Action.SizeUp.ordinal()]; }
	public KeyboardAction setSizeUpAction(final KeyboardAction action) { actions[Action.SizeUp.ordinal()] = action; return action; }
	
	public KeyboardAction getSizeDownAction() { return actions[Action.SizeDown.ordinal()]; }
	public KeyboardAction setSizeDownAction(final KeyboardAction action) { actions[Action.SizeDown.ordinal()] = action; return action; }
	
	public KeyboardAction getChangeColorAction() { return actions[Action.ChangeColor.ordinal()]; }
	public KeyboardAction setChangeColorAction(final KeyboardAction action) { actions[Action.ChangeColor.ordinal()] = action; return action; }
	
	public KeyboardAction getToggleAnnotationAction() { return actions[Action.ToggleAnnotation.ordinal()]; }
	public KeyboardAction setToggleAnnotationAction(final KeyboardAction action) { actions[Action.ToggleAnnotation.ordinal()] = action; return action; }
	
	public KeyboardAction getInsertDateAction() { return actions[Action.InsertDate.ordinal()]; }
	public KeyboardAction setInsertDateAction(final KeyboardAction action) { actions[Action.InsertDate.ordinal()] = action; return action; }
	
	public KeyboardAction getNewFramesetAction() { return actions[Action.NewFrameset.ordinal()]; }
	public KeyboardAction setNewFramesetAction(final KeyboardAction action) { actions[Action.NewFrameset.ordinal()] = action; return action; }
	
	public KeyboardAction getChangeFontStyleAction() { return actions[Action.ChangeFontStyle.ordinal()]; }
	public KeyboardAction setChangeFontStyleAction(final KeyboardAction action) { actions[Action.ChangeFontStyle.ordinal()] = action; return action; }

	public KeyboardAction getChangeFontFamilyAction() { return actions[Action.ChangeFontFamily.ordinal()]; }
	public KeyboardAction setChangeFontFamilyAction(final KeyboardAction action) { actions[Action.ChangeFontFamily.ordinal()] = action; return action; }
	
	public KeyboardAction getAudienceModeAction() { return actions[Action.AudienceMode.ordinal()]; }
	public KeyboardAction setAudienceModeAction(final KeyboardAction action) { actions[Action.AudienceMode.ordinal()] = action; return action; }
	
	public KeyboardAction getXRayModeAction() { return actions[Action.XRayMode.ordinal()]; }
	public KeyboardAction setXRayModeAction(final KeyboardAction action) { actions[Action.XRayMode.ordinal()] = action; return action; }
	
	public KeyboardAction getSaveAction() { return actions[Action.Save.ordinal()]; }
	public KeyboardAction setSaveAction(final KeyboardAction action) { actions[Action.Save.ordinal()] = action; return action; }
	
	public KeyboardAction getRefreshAction() { return actions[Action.Refresh.ordinal()]; }
	public KeyboardAction setRefreshAction(final KeyboardAction action) { actions[Action.Refresh.ordinal()] = action; return action; }

	public KeyboardAction getCreateNewTextAction() { return actions[Action.NewText.ordinal()]; }
	public KeyboardAction setCreateNewTextAction(final KeyboardAction action) { actions[Action.NewText.ordinal()] = action; return action; }
	
	public KeyboardAction getInsertCharacterAction() { return actions[Action.InsertCharacter.ordinal()]; }
	public KeyboardAction setInsertCharacterAction(final KeyboardAction action) { actions[Action.InsertCharacter.ordinal()] = action; return action; }
}
