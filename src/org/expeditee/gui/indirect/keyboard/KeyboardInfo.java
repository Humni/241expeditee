package org.expeditee.gui.indirect.keyboard;

import java.util.Collection;

import org.expeditee.gui.FunctionKey;
import org.expeditee.items.Item;

public class KeyboardInfo {
	public FunctionKey key;
	public int repeat;
	public boolean isShiftDown;
	public boolean isControlDown;
	public Collection<Item> enclosed;
	public Item firstConnected;
	public Collection<Item> connected;
	public Collection<Item> lineEnds;

	public KeyboardInfo(final FunctionKey key, final int repeat,
			final boolean isShiftDown, final boolean isControlDown, final Collection<Item> enclosed, 
			final Item firstConnected, Collection<Item> connected, Collection<Item> lineEnds) {
		this.key = key;
		this.repeat = repeat;
		this.isShiftDown = isShiftDown;
		this.isControlDown = isControlDown;
		this.enclosed = enclosed;
		this.firstConnected = firstConnected;
		this.connected = connected;
		this.lineEnds = lineEnds;
	}

}
