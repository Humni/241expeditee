package org.expeditee.gui.indirect.mouse;

public class IndirectMouseActions {
	
	private enum Action {
		//Left Mouse
		BACK, TDFC, FRAMENAMECLICK, FOLLOWLINK, EXECUTEACTION,
		//Middle Mouse
		NEWLINE, GROUPPICKUP, DETACHLINE, ANCHORFREEITEMS, DELETEITEMS,
		//Right Mouse
		CREATERECTANGLE, EXTENDLINE, MAKEGROUPCOPY, MAKECOPY, EXTRUDE, RUBBERBANDCOPY, 
		RUBBERBANDCORNERCOPY, STAMP, MERGESINGLE, MERGETWO, MERGEGROUP
	}//Where has properties gone?
	
	private static final int ACTIONQUANTITY = Action.values().length;
	
	private static IndirectMouseActions instance = new IndirectMouseActions();
	
	private IndirectMouseActions() { }
	
	public static IndirectMouseActions getInstance() { return instance; }
	
	public MouseAction[] actions = new MouseAction[ACTIONQUANTITY];
	
	public MouseAction getBackAction() { return actions[Action.BACK.ordinal()]; }
	public MouseAction setBackAction(final MouseAction action) { actions[Action.BACK.ordinal()] = action; return action; }

	public MouseAction getTDFCAction() { return actions[Action.TDFC.ordinal()]; }
	public MouseAction setTDFCAction(final MouseAction action) { actions[Action.TDFC.ordinal()] = action; return action; }

	public MouseAction getRespondToFrameNameClickAction() { return actions[Action.FRAMENAMECLICK.ordinal()]; }
	public MouseAction setRespondToFrameNameClickAction(final MouseAction action) { actions[Action.FRAMENAMECLICK.ordinal()] = action; return action; }

	public MouseAction getFollowLinkAction() { return actions[Action.FOLLOWLINK.ordinal()]; }
	public MouseAction setFollowLinkAction(final MouseAction action) { actions[Action.FOLLOWLINK.ordinal()] = action; return action; }

	public MouseAction getExecuteActionAction() { return actions[Action.EXECUTEACTION.ordinal()]; }
	public MouseAction setExecuteActionAction(final MouseAction action) { actions[Action.EXECUTEACTION.ordinal()] = action; return action; }

	public MouseAction getCreateRectangleAction() { return actions[Action.CREATERECTANGLE.ordinal()]; }
	public MouseAction setCreateRectangleAction(final MouseAction action) { actions[Action.CREATERECTANGLE.ordinal()] = action; return action; }

	public MouseAction getExtendLineAction() { return actions[Action.EXTENDLINE.ordinal()]; }
	public MouseAction setExtendLineAction(final MouseAction action) { actions[Action.EXTENDLINE.ordinal()] = action; return action; }

	public MouseAction getMakeGroupCopyAction() { return actions[Action.MAKEGROUPCOPY.ordinal()]; }
	public MouseAction setMakeGroupCopyAction(final MouseAction action) { actions[Action.MAKEGROUPCOPY.ordinal()] = action; return action; }

	public MouseAction getMakeCopyAction() { return actions[Action.MAKECOPY.ordinal()]; }
	public MouseAction setMakeCopyAction(final MouseAction action) { actions[Action.MAKECOPY.ordinal()] = action; return action; }
	
	public MouseAction getExtrudeAction() { return actions[Action.EXTRUDE.ordinal()]; }
	public MouseAction setExtrudeAction(final MouseAction action) { actions[Action.EXTRUDE.ordinal()] = action; return action; }

	public MouseAction getRubberBandingCopyAction() { return actions[Action.RUBBERBANDCOPY.ordinal()]; }
	public MouseAction setRubberBandingCopyAction(final MouseAction action) { actions[Action.RUBBERBANDCOPY.ordinal()] = action; return action; }

	public MouseAction getRubberBandingCornerAction() { return actions[Action.RUBBERBANDCORNERCOPY.ordinal()]; }
	public MouseAction setRubberBandingCornerAction(final MouseAction action) { actions[Action.RUBBERBANDCORNERCOPY.ordinal()] = action; return action; }

	public MouseAction getStampAction() { return actions[Action.STAMP.ordinal()]; }
	public MouseAction setStampAction(final MouseAction action) { actions[Action.STAMP.ordinal()] = action; return action; }

	public MouseAction getMergeSingleItemAction() { return actions[Action.MERGESINGLE.ordinal()]; }
	public MouseAction setMergeSingleItemAction(final MouseAction action) { actions[Action.MERGESINGLE.ordinal()] = action; return action; }

	public MouseAction getMergeTwoItemsAction() { return actions[Action.MERGETWO.ordinal()]; }
	public MouseAction setMergeTwoItemsAction(final MouseAction action) { actions[Action.MERGETWO.ordinal()] = action; return action; }

	public MouseAction getMergeGroupAction() { return actions[Action.MERGEGROUP.ordinal()]; }
	public MouseAction setMergeGroupAction(final MouseAction action) { actions[Action.MERGEGROUP.ordinal()] = action; return action; }

	public MouseAction getNewLineAction() { return actions[Action.NEWLINE.ordinal()]; }
	public MouseAction setNewLineAction(final MouseAction action) { actions[Action.NEWLINE.ordinal()] = action; return action; }

	public MouseAction getGroupPickupAction() { return actions[Action.GROUPPICKUP.ordinal()]; }
	public MouseAction setGroupPickupAction(final MouseAction action) { actions[Action.GROUPPICKUP.ordinal()] = action; return action; }

	public MouseAction getDetachLineAction() { return actions[Action.DETACHLINE.ordinal()]; }
	public MouseAction setDetachLineAction(final MouseAction action) { actions[Action.DETACHLINE.ordinal()] = action; return action; }
	
	public MouseAction getAnchorFreeItemsAction() { return actions[Action.ANCHORFREEITEMS.ordinal()]; }
	public MouseAction setAnchorFreeItemsAction(final MouseAction action) { actions[Action.ANCHORFREEITEMS.ordinal()] = action; return action; }

	public MouseAction getDeleteItemsAction() { return actions[Action.DELETEITEMS.ordinal()]; }
	public MouseAction setDeleteItemsAction(final MouseAction action) { actions[Action.DELETEITEMS.ordinal()] = action; return action; }
}
