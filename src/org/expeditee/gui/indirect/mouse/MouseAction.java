package org.expeditee.gui.indirect.mouse;

import java.util.List;

import org.expeditee.items.Item;

public interface MouseAction {
	
	public List<Item> exec(MouseInfo info);
}
