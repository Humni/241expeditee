package org.expeditee.gui.indirect.mouse;

import java.util.Collection;

import org.expeditee.items.Item;

public class MouseInfo {
	public Item clicked;
	public Collection<Item> clickedIn;
	public boolean isShiftDown;
	public boolean isControlDown;

	public MouseInfo(final Item clicked, final Collection<Item> clickedIn, final boolean isShiftDown, final boolean isControldown) {
		this.clicked = clicked;
		this.clickedIn = clickedIn;
		this.isShiftDown = isShiftDown;
		this.isControlDown = isControldown;			
	}
}
