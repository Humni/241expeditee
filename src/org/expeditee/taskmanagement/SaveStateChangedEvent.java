/**
 *    SaveStateChangedEvent.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.taskmanagement;

import java.util.EventObject;


/**
 * Occurs when a saveable entities (save)state changes.
 * 
 * @author Brook Novak
 */
public class SaveStateChangedEvent extends EventObject {
    
	private static final long serialVersionUID = 1L;
	
	private SaveableEntity entity;
	
	public SaveStateChangedEvent(Object source, SaveableEntity entity) {
        super(source);
        this.entity = entity;
    }

	public SaveableEntity getEntity() {
		return entity;
	}

}

