/**
 *    SaveableEntity.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.taskmanagement;


/**
 * A saveable entity. These can be added to the SaveEntityManager.
 * 
 * @author Brook Novak
 * 
 * @see EntitySaveManager
 * 
 */
public interface SaveableEntity {

	/**
	 * @return 
	 * 		True if the entity needs saving. False if does not need to save
	 */
	public boolean doesNeedSaving();

	/**
	 * Perform all save logic here. This will be invoked on a dedicated thread.
	 */
	public void performSave();
	
	/**
	 * @return 
	 * 		A human readable string using sentance captalization rules.
	 * 		should be short, and should decribe the entity being saved. 
	 * 		<br>For example: "Video: "goodtimes.avi""
	 * 		<br>or "Audio track".
	 */
	public String getSaveName();
	
}
