/**
 *    SaveStateChangedEventListener.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.taskmanagement;

import java.util.EventListener;


/**
 * 
 * An EventListener for save events.
 * 
 * @author Brook Novak
 * 
 */
public interface SaveStateChangedEventListener extends EventListener {

	/**
	 * Raised whenever a saveable entity has started or is about to start saving
	 * 
	 * @param event The event information.
	 */
	public void saveStarted(SaveStateChangedEvent event);

	/**
	 * Raised whenever a saveable entity has finished saving.
	 * 
	 * @param event The event information.
	 */
	public void saveCompleted(SaveStateChangedEvent event);
	
}
