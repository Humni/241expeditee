/**
 *    SPointer.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.simple;

public class SPointer<T> extends SVariable<T> {

	public static final String framePrefix = SVariable.prefix + "fp"
			+ SVariable.separator;

	public static final String itemPrefix = SVariable.prefix + "ip"
			+ SVariable.separator;

	public static final String filePrefix = SVariable.prefix + "f"
			+ SVariable.separator;

	public static final String associationPrefix = SVariable.prefix + "ap"
			+ SVariable.separator;

	public SPointer(String name, T value) {
		super(name, value);
	}

	public void setValue(T newValue) {
		value_ = newValue;
	}

	@Override
	public String stringValue() {
		return value_.toString();
	}

	@Override
	public void setValue(SVariable<T> v) throws Exception {
		if (v instanceof SPointer) {
			setValue(v.getValue());
			return;
		}
		throw new Exception("Can not set pointer variable with primitive");
	}
}
