/**
 *    SReal.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.simple;

public class SReal extends SPrimitive<Double> {
	public static String prefix = SVariable.prefix + "r" + SVariable.separator;

	public SReal() {
		super();
	}

	public SReal(String name, Double value) {
		super(name, value);
	}

	public SReal(double value) throws Exception {
		super(value);
	}

	public SReal(float value) throws Exception {
		super((double) value);
	}

	@Override
	public void parse(String s) throws Exception {
		if (s.equals(""))
			value_ = 0.0;
		else {
			try {
				value_ = Double.parseDouble(s);
			} catch (NumberFormatException ne) {
				value_ = (double) Long.decode(s);
			}
		}
	}

	@Override
	public Boolean booleanValue() {
		return value_ > 0;
	}

	@Override
	public Long integerValue() {
		return Math.round(value_);
	}

	@Override
	public Double doubleValue() {
		return value_;
	}

	@Override
	public Character characterValue() {
		return new Character((char) Math.round(value_));
	}

	@Override
	public void setValue(SPrimitive v) throws IncorrectTypeException {
		value_ = v.doubleValue();
	}
}
