/**
 *    SInteger.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.simple;

public class SInteger extends SPrimitive<Long> {
	public static String prefix = SVariable.prefix + "i" + SVariable.separator;

	public SInteger() {
		super();
	}

	public SInteger(String name, Long value) {
		super(name, value);
	}

	public SInteger(String name, Integer value) {
		super(name, value.longValue());
	}

	public SInteger(long value) throws Exception {
		super(value);
	}

	public SInteger(int value) throws Exception {
		super((long) value);
	}

	@Override
	public void parse(String s) throws Exception {
		if (s.equals(""))
			value_ = 0L;
		else {
			try {
				value_ = Long.decode(s);
			} catch (NumberFormatException ne) {
				value_ = Math.round(Double.parseDouble(s));
			}
		}
	}

	@Override
	public Boolean booleanValue() {
		return value_ > 0;
	}

	@Override
	public Long integerValue() {
		return value_;
	}

	@Override
	public Double doubleValue() {
		return value_.doubleValue();
	}

	@Override
	public Character characterValue() {
		return new Character((char) value_.intValue());
	}

	@Override
	public void setValue(SPrimitive v) throws IncorrectTypeException {
		value_ = v.integerValue();
	}
}
