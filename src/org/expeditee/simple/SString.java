/**
 *    SString.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.simple;

public class SString extends SPrimitive<String> {
	public static String prefix = SVariable.prefix + "s" + SVariable.separator;

	Double doubleValue_ = null;

	public SString() {
		super();
	}

	public SString(String name, String value) {
		super(name, value);
	}

	public SString(String value) /* throws Exception */{
		super(value);
	}

	@Override
	public void parse(String s) {
		value_ = s;
		doubleValue_ = null;
	}

	@Override
	public Boolean booleanValue() {
		return Boolean.parseBoolean(value_);
	}

	@Override
	public Long integerValue() {
		if (value_.equals(""))
			return 0L;
		try {
			return Long.decode(value_);
		} catch (NumberFormatException ne) {
		}
		return Math.round(Double.parseDouble(value_));
	}

	@Override
	public Double doubleValue() {
		if (doubleValue_ != null)
			return doubleValue_;

		if (value_.equals(""))
			doubleValue_ = 0.0;
		else {
			try {
				doubleValue_ = Double.parseDouble(value_);
			} catch (NumberFormatException ne) {
				try{
				doubleValue_ =(double) Long.decode(value_);
				}catch(Exception e){
					doubleValue_ = Double.NaN;
				}
			}
		}
		return doubleValue_;
	}

	@Override
	public String stringValue() {
		return value_;
	}

	@Override
	public Character characterValue() {
		if (value_.length() > 0)
			return value_.charAt(0);

		return '\0';
	}

	@Override
	public void setValue(SPrimitive v) {
		value_ = v.stringValue();
		doubleValue_ = null;
	}

	public int length() {
		return value_.length();
	}
}