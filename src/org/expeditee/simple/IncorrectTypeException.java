/**
 *    IncorrectTypeException.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.simple;

public class IncorrectTypeException extends RuntimeException {
	static final long serialVersionUID = -7034897190745766939L;
	
	public IncorrectTypeException(String type, int no) {
		super("Expected param " + no + " to be " + type);
	}

	public IncorrectTypeException(String var, String type) {
		super("Expected param " + var + " to be " + type);
	}
	
	public IncorrectTypeException(String message) {
		super(message);
	}
}
