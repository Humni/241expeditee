/**
 *    SCharacter.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.simple;

public class SCharacter extends SPrimitive<Character> {
	public static String prefix = SVariable.prefix + "c" + SVariable.separator;

	public SCharacter() {
		super();
	}

	public SCharacter(String name, Character value) {
		super(name, value);
	}

	public SCharacter(Character value) throws Exception {
		super(value);
	}

	@Override
	public void parse(String s) {
		if (s.equals(""))
			value_ = '\0';
		else
			value_ = s.charAt(0);
	}

	@Override
	public Boolean booleanValue() {
		return value_ == 'T' || value_ == 't';
	}

	@Override
	public Long integerValue() {
		return (long) value_.charValue();
	}

	@Override
	public Double doubleValue() {
		return (double) value_.charValue();
	}

	@Override
	public String stringValue() {
		return value_.toString();
	}

	@Override
	public Character characterValue() {
		return value_;
	}

	@Override
	public void setValue(SPrimitive v) throws IncorrectTypeException {
		value_ = v.characterValue();
	}
}