/**
 *    SVariable.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.simple;

/**
 * Class for a generic variable.
 * 
 * @author mrww1
 * 
 */
public abstract class SVariable<T extends Object> {
	public static final String prefix = "$";

	protected static final String separator = ".";

	protected String name_;

	protected T value_;

	public SVariable(String name, T value) {
		name_ = name;
		value_ = value;
	}

	public SVariable() {
	}

	public String getName() {
		return name_;
	}

	public T getValue() {
		return value_;
	}

	protected void setName(String newName) {
		name_ = newName;
	}

	public String stringValue() {
		return value_.toString();
	}

	public abstract void setValue(SVariable<T> v) throws Exception;
}
