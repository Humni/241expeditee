/**
 *    SBoolean.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.simple;

public class SBoolean extends SPrimitive<Boolean> {
	public static String prefix = SVariable.prefix + "b" + SVariable.separator;

	public SBoolean() {
		super();
	}

	public SBoolean(String name, Boolean value) {
		super(name, value);
	}

	public SBoolean(Boolean value) throws IncorrectTypeException {
		super(value);
	}

	@Override
	public void parse(String s) {
		String lowerCase = s.toLowerCase();
		value_ = lowerCase.equals("true") || lowerCase.equals("t")
				|| lowerCase.equals("yes") || lowerCase.equals("on");
	}

	@Override
	public Boolean booleanValue() {
		return value_;
	}

	@Override
	public Long integerValue() {
		return value_ ? 1L : 0L;
	}

	@Override
	public Double doubleValue() {
		return value_ ? 1.0 : 0.0;
	}

	@Override
	public Character characterValue() {
		return value_ ? 'T' : 'F';
	}

	@Override
	public void setValue(SPrimitive v) throws IncorrectTypeException {
		value_ = v.booleanValue();
	}
}
