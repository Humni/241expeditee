/**
 *    Debug.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.actions;

import java.util.List;

import org.expeditee.gui.DisplayIO;
import org.expeditee.items.Constraint;
import org.expeditee.items.Item;

/**
 * This class is used for temporary debugging actions that will get removed from
 * the final product.
 * 
 * @author jdm18
 * 
 */
public class Debug {

	/**
	 * Outputs the list of constraints all Items in the current Frame have.
	 */
	public static void ShowConstraints() {
		List<Item> items = DisplayIO.getCurrentFrame().getItems();

		for (Item i : items)
			if (i.isLineEnd()) {
				System.out.println(i.getID());

				for (Constraint c : i.getConstraints())
					if (c.getOppositeEnd(i) != null)
						System.out.println("\t"
								+ c.getOppositeEnd(i).getID());
					else
						System.out.println("\tNULL");

				System.out.println("------------");
			}

		System.out.println("==================");
	}

	public static void PrintLine() {
		System.out.println("Action");
	}
}
