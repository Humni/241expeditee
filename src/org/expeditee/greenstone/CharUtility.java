/**
 *    CharUtility.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.greenstone;

public class CharUtility {

	/** Returns an int value that is negative if cCompare comes before cRef in the alphabet, zero if
	*   the two are equal, and positive if cCompare comes after cRef in the alphabet.
	*/
	public static int compareCharsAlphabetically(char cCompare, char cRef) {
		return (alphabetizeChar(cCompare) - alphabetizeChar(cRef));
	}
	
	private static int alphabetizeChar(char c) {
		if(c < 65) return c;
		if(c < 89) return (2 * c) - 65;
		if(c < 97) return c + 24;
		if(c < 121) return (2 * c) - 128;
		return c;
	}
	
}
