/**
 *    Result.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.greenstone;

public class Result {
	private String docID;
	private int rank;
	private String score;

	Result(String docID, int rank, String score) {
		this.docID = docID;
		this.rank = rank;
		this.score = score;
	}

	public String toString() {
		String dump = this.rank + "\t" + this.score + "\t" + this.docID + "\n";
		return dump;
	}

	public String getDocID() {
		return this.docID;
	}

	public int getRank() {
		return this.rank;
	}

	public String getScore() {
		return this.score;
	}
}
