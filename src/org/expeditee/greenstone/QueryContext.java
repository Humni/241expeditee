/**
 *    QueryContext.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.greenstone;

public class QueryContext
{
        private int rank;
        private String score;
        private Query query;

        public QueryContext(int rank, String score, Query query)
        {
		this.rank = rank;
		this.score = score;
		this.query = query;
        }

	public String toString() {
		String dump = "\n\nQueryContext..\n";
		dump = dump + "rank\t\t" + rank + "\n";
		dump = dump + "score\t\t" + score ;
		dump = dump + query.toString();

		return dump;
	}

	public int getRank() {
		return this.rank;
	}

	public String getScore() {
		return this.score;
	}

	public Query getQuery() {
		return this.query;
	}
}
