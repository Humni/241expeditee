/**
 *    HFormat.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.agents;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.expeditee.gui.Frame;
import org.expeditee.items.Item;
import org.expeditee.items.Text;

/**
 * A simple formatting agent that aligns the Y values.
 * 
 * @author Mike
 * 
 */
public class HFormat extends Format {

	public HFormat() {
		super();
	}

	@Override
	public Frame process(Frame start) {
		Collection<Text> itemsToFormat = getItemsToFormat(start);
		
		if(itemsToFormat.size() == 0)
			return null;
		
		List<Item> changedItems = new ArrayList<Item>();
		
		float anchorY = 0F;
		float yThreshold = 0F;
		
		for(Text t: itemsToFormat){
			if(t.getY() > yThreshold + anchorY){
				anchorY = t.getY();
				yThreshold = t.getSize();
			}else{
				if(t.getY() != anchorY && !changedItems.contains(t)) {
					Item copy = t.copy();
					copy.setID(t.getID());
					changedItems.add(copy);
				}
				t.setY(anchorY);
			}
		}
		
		System.out.println(changedItems);
		start.addToUndoMove(changedItems);
		return null;
	}
}
