/**
 *    SearchFrameset.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.agents;

import java.util.Collection;

import org.expeditee.gui.Frame;
import org.expeditee.gui.FrameIO;

public class SearchFrameset extends SearchAgent {
	private long _firstFrame = 1;

	private long _maxFrame = Integer.MAX_VALUE;

	public SearchFrameset(long firstFrame, long maxFrame, String searchText) {
		this(searchText);
		_firstFrame = firstFrame;
		_maxFrame = maxFrame;
	}

	public SearchFrameset(String searchText) {
		super(searchText);
	}

	@Override
	protected Frame process(Frame frame) {
		if (frame == null) {
			frame = FrameIO.LoadFrame(_startName + '0');
		}
		String path = frame.getPath();

		int count = FrameIO.getLastNumber(_startName);
		for (long i = _firstFrame; i <= _maxFrame && i <= count; i++) {
			if (_stop) {
				break;
			}
			String frameName = _startName + i;
			overwriteMessage("Searching " + frameName);
			Collection<String> found = FrameIO.searchFrame(frameName, _pattern,
					path);
			addResults(i + "", frameName, found);
		}
		_results.save();

		String resultFrameName = _results.getName();
		if (_clicked != null)
			_clicked.setLink(resultFrameName);

		return _results.getFirstFrame();
	}
}
