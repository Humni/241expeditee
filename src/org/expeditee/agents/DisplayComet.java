/**
 *    DisplayComet.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.agents;

import java.util.Collection;
import java.util.HashSet;
import java.util.Stack;

import org.expeditee.gui.DisplayIO;
import org.expeditee.gui.Frame;
import org.expeditee.gui.FrameIO;
import org.expeditee.gui.MessageBay;
import org.expeditee.items.Item;
import org.expeditee.items.ItemUtils;

public class DisplayComet extends DefaultAgent {
	private Stack<Frame> _frameList = new Stack<Frame>();

	public DisplayComet(String delay) {
		super(delay);
	}

	public DisplayComet() {
		super();
	}

	@Override
	protected Frame process(Frame frame) {
		Collection<String> seen = new HashSet<String>();

		DisplayIO.addToBack(frame);

		// Goto the end of the comet
		Item old = null;
		Frame oldFrame = frame;
		while (oldFrame != null) {
			if (_stop)
				return null;
			seen.add(oldFrame.getName().toLowerCase());
			_frameList.push(oldFrame);
			old = ItemUtils.FindExactTag(oldFrame.getItems(), ItemUtils
					.GetTag(ItemUtils.TAG_BACKUP));
			oldFrame = null;
			if (old != null) {
				String link = old.getAbsoluteLink();

				if (link != null) {
					String linkLowerCase = link.toLowerCase();
					if (!seen.contains(linkLowerCase)) {
						oldFrame = FrameIO.LoadFrame(linkLowerCase);
						MessageBay.overwriteMessage("Loading frames: " + link);
					}
				}
			}
		}
		// Back out one frame at a time
		while (!_frameList.empty()) {
			if (_stop)
				return null;
			DisplayIO.setCurrentFrame(_frameList.pop(), true);
			_frameCount++;
			pause(_delay);
		}

		return _start;
	}

}
