/**
 *    SpellcheckTree.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.agents;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.expeditee.agents.wordprocessing.JSpellChecker;

public class SpellcheckTree extends SearchTree {

	private JSpellChecker _spellChecker = null;

	public SpellcheckTree() {
		super(null);

		try {
			_spellChecker = JSpellChecker.getInstance();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// TODO better handle errors
	}

	@Override
	protected String getResultSurrogate(String toSearch) {
		if (_spellChecker == null)
			return null;
		_spellChecker.setText(toSearch);
		_spellChecker.check();

		String badSpelling = _spellChecker.getMisspelledWord();
		String result = badSpelling;
//		while (badSpelling != null) {
//			_spellChecker.ignoreWord(true);
//			_spellChecker.check();
//			badSpelling = _spellChecker.getMisspelledWord();
//			if (badSpelling != null)
//				result += "," + badSpelling;
//		}

		if (badSpelling == null || badSpelling.length() <= 1)
			return null;

		return "[" + result + "] " + toSearch;
	}
	
	@Override
	protected String getResultsTitleSuffix() {
		return "";
	}
}
