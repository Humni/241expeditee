/**
 *    SwitchyardTree.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.agents;

import java.awt.Color;
import java.awt.geom.Point2D;

import org.expeditee.gui.Frame;
import org.expeditee.gui.FrameIO;
import org.expeditee.items.Text;

public class SwitchyardTree extends DefaultAgent {

	@Override
	protected Frame process(Frame frame) {
		//MessageBay.displayMessage("Running switchyard tree...");

		for (Text textItem : frame.getBodyTextItems(false)) {
			// Delete all non-annotations with more that one letter
			if (textItem.getText().length() > 1){
				frame.removeItem(textItem);
				if(_stop)
					return null;
			} else {
				// goto child frame of any linked 1 letter items
				String link = textItem.getAbsoluteLink();
				if (link != null) {
					Frame childFrame = FrameIO.LoadFrame(link);
					if (childFrame != null) {
						Point2D.Float lastItemEnd = textItem.getParagraphEndPosition();
						for (Text childItem : childFrame.getBodyTextItems(false)) {
							// look for red items (remember get color may be null
							if (Color.RED.equals(childItem.getPaintColor())) {
								// make a copy and add to parent frame
								Text itemCopy = childItem.copy();
								// add to the right of parent item
								lastItemEnd.setLocation(lastItemEnd.x + 20, lastItemEnd.y);
								itemCopy.setPosition(lastItemEnd.x, lastItemEnd.y);
								lastItemEnd = itemCopy
										.getParagraphEndPosition();
								itemCopy.setID(frame.getNextItemID());
								frame.addItem(itemCopy);
							}
							_itemCount++;
							if(_stop)
								return null;
						}
					}
				}
			}
		}
		_frameCount++;
		return null;
	}
}
