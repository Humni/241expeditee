/**
 *    DisplayTreeLeaves.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.agents;

import org.expeditee.gui.Frame;
import org.expeditee.items.Item;

/**
 * Displays the Tree of Frames starting from the given frame. The delay between
 * frames can be adjusted, either per-frame with the
 * 
 * @DisplayFramePause tag, or mid-stream with the
 * @DisplayTreePause tag, both tags should be followed with the desired delay in
 *                   ms. The default delay between Frames is 50ms.
 * @author jdm18
 * 
 */
public class DisplayTreeLeaves extends DisplayTree {

	public DisplayTreeLeaves(String delay) {
		super(delay);
	}

	public DisplayTreeLeaves() {
		super();
	}

	@Override
	protected void processFrame(Frame toProcess) {
		// call display trees process only if the frame doesnt have any linked
		// items
		for (Item item : toProcess.getItems()) {
			if (item.getLink() != null && !item.isAnnotation())
				return;
		}

		super.processFrame(toProcess);
	}
}
