/**
 *    ArraySetting.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.setting;

import java.util.Arrays;

import org.expeditee.items.Text;

public abstract class ArraySetting<T> extends VariableSetting {

	protected T[] _default;
	protected T[] _value;
	
	public ArraySetting(String tooltip, T[] value) {
		super(tooltip);
		_default = Arrays.copyOf(value, value.length);
		_value = value;
	}

    	public T getSafe(int i) {
		
		if(i < _value.length){
			
			return _value[i];
		}
		else{
			return _default[i];
		}		
	}

	public T[] get() {
		return _value;
	}
	
	public void set(T[] value) {
		_value = value;
	}
	
	public abstract boolean setSetting(Text text);
	
	public void setDefault(T[] value) {
		_default = Arrays.copyOf(value, value.length);
	}
	
	public void reset() {
		_value = Arrays.copyOf(_default, _default.length);
	}

}
