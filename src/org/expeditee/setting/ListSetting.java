/**
 *    ListSetting.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.setting;

import java.util.LinkedList;
import java.util.List;

import org.expeditee.items.Text;

public abstract class ListSetting<T> extends VariableSetting {

	protected List<T> _default;
	protected List<T> _value;
	
	public ListSetting(String tooltip, List<T> value) {
		super(tooltip);
		_default = new LinkedList<T>(value);
		_value = value;
	}
	
	public ListSetting(String tooltip) {
		this(tooltip, new LinkedList<T>());
	}
	
	public List<T> get() {
		return _value;
	}
	
	public void set(List<T> value) {
		_value = value;
	}
	
	public abstract boolean setSetting(Text text);
	
	public void setDefault(List<T> value) {
		_default = new LinkedList<T>(value);
	}
	
	public void reset() {
		_value = new LinkedList<T>(_default);
	}

}
