/**
 *    TextSetting.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.expeditee.setting;

import org.expeditee.items.Text;

public abstract class TextSetting extends GenericSetting<Text> {

	public TextSetting(String tooltip) {
		super(Text.class, tooltip, null);
		_default = generateText();
		_value = _default;
	}
	
	@Override
	public Text get() {
		return _value == null ? null : _value.copy();
	}
	
	@Override
	public boolean setSetting(Text text) {
		_value = text.getTemplateForm();
		return true;
	}
	
	public abstract Text generateText();

}
