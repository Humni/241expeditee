/**
 *    ZeusMouseEventRouter.java
 *    Copyright (C) 2010 New Zealand Digital Library, http://expeditee.org
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.zeus;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Event;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JMenuBar;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import org.expeditee.gui.Browser;
import org.expeditee.gui.DisplayIO;
import org.expeditee.gui.FrameGraphics;
import org.expeditee.gui.FrameMouseActions;
import org.expeditee.gui.FrameUtils;
import org.expeditee.gui.FreeItems;
import org.expeditee.gui.Help;
import org.expeditee.gui.PopupManager;
import org.expeditee.items.Item;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.VBox;

/**
 * The gateway for mouse input; conditionally forwards mouse messages to swing
 * components / expeditee frames for the Browser.
 * 
 * Various mouse listeners can attatch themselves here to listen to mouse events
 * forwared to expeditee .. this excludes widget-exclusive mouse messages.
 * 
 * @author Brook Novak
 * 
 */
public class ZeusMouseEventRouter extends JFXPanel {

	private static final long serialVersionUID = 1L;

	private JMenuBar _menuBar;

	private Component _contentPane;

	private JFXPanel _JFXpanel;

	private List<MouseListener> _mouseListeners = new LinkedList<MouseListener>();

	private List<MouseMotionListener> _mouseMotionListeners = new LinkedList<MouseMotionListener>();

	private List<MouseWheelListener> _mouseWheelListeners = new LinkedList<MouseWheelListener>();

	private static MouseEvent _currentMouseEvent = null;

	/**
	 * Routes events on given frame layers... the menu bar and content pane
	 * 
	 * @param menuBar
	 *            Must not be null.
	 * 
	 * @param contentPane
	 *            Must not be null.
	 */
	public ZeusMouseEventRouter(JMenuBar menuBar, Container contentPane) {

		if (contentPane == null)
			throw new NullPointerException("contentPane");

		_JFXpanel = new JFXPanel();

		// Listen for all AWT events (ensures popups are included)
		Toolkit.getDefaultToolkit().addAWTEventListener(new EventCatcher(),
				AWTEvent.MOUSE_MOTION_EVENT_MASK | AWTEvent.MOUSE_EVENT_MASK | AWTEvent.MOUSE_WHEEL_EVENT_MASK);

		this._menuBar = menuBar;
		this._contentPane = contentPane;
		this._contentPane.add(_JFXpanel);

		this._contentPane.setSize(300, 200);
		this._contentPane.setVisible(true);
		
		init();
	}
	
    private VBox pane;
	
	private void init() {
        pane = new VBox();
        pane.setAlignment(Pos.CENTER);
        Platform.runLater(this::createScene);
    }
 
    private void createScene() {
        Scene scene = new Scene(pane);
        
        scene.setOnMousePressed(new EventHandler<javafx.scene.input.MouseEvent>() {
	        @Override public void handle(javafx.scene.input.MouseEvent event) {
            	long unixTime = System.currentTimeMillis() / 1000L;
            	System.out.println(event.getTarget());
            	//Component widget = (Component) checkWidget((int) event.getSceneX(), (int) event.getSceneY());
            	//if(widget == null) widget = _contentPane;
            	Component c = SwingUtilities.convertMouseEvent((Component)((JFXPanel)event.getSource()), event, _contentPane);
            	MouseEvent simulated = new MouseEvent(c, Event.MOUSE_DOWN, unixTime, 0, (int) event.getSceneX() , (int) event.getSceneY() , (int) event.getScreenX() , (int) event.getScreenY(), 1, false, MouseEvent.BUTTON1);
            	forwardMouseEvent(simulated);
	        }
	    });

	    scene.setOnMouseReleased(new EventHandler<javafx.scene.input.MouseEvent>() {
	        @Override public void handle(javafx.scene.input.MouseEvent event) {
            	long unixTime = System.currentTimeMillis() / 1000L;
            	System.out.println(event.getSource());
            	Component c = SwingUtilities.convertMouseEvent(event.getComponent(), event, _contentPane);
            	MouseEvent simulated = new MouseEvent(c, Event.MOUSE_UP, unixTime, 0, (int) event.getSceneX() , (int) event.getSceneY() , (int) event.getScreenX() , (int) event.getScreenY(), 1, false, MouseEvent.BUTTON1);
            	forwardMouseEvent(simulated);
	        }
	    });
	    
	    scene.setOnMouseMoved(new EventHandler<javafx.scene.input.MouseEvent>() {
	        @Override public void handle(javafx.scene.input.MouseEvent event) {
            	long unixTime = System.currentTimeMillis() / 1000L;
            	MouseEvent simulated = new MouseEvent(_contentPane, Event.MOUSE_MOVE, unixTime, 0, (int) event.getSceneX() , (int) event.getSceneY() , (int) event.getScreenX() , (int) event.getScreenY(), 1, false, MouseEvent.NOBUTTON);
            	forwardMouseEvent(simulated);
	        }
	    });
        
        setScene(scene);
    }
    
    /**
     * Finds any widgets in the clickable space
     */
    public Item checkWidget(int x, int y){
    	Collection<Item> items = FrameUtils.getCurrentItems(FrameUtils.getCurrentItem());
    	for(Item i : items){
    		if(i.contains(x, y))
    			return i;
    	}
    	return null;
    }
    

	/**
	 * Listens only to events to frames... i.e. to exeditee, not to widgets.
	 * 
	 * @param listener
	 *            The listener to add.
	 */
	public void addExpediteeMouseListener(MouseListener listener) {
		if (listener == null)
			throw new NullPointerException("listener");
		_mouseListeners.add(listener);
	}

	public void removeExpediteeMouseListener(MouseListener listener) {
		if (listener == null)
			throw new NullPointerException("listener");
		_mouseListeners.remove(listener);
	}

	public void addExpediteeMouseMotionListener(MouseMotionListener listener) {
		if (listener == null)
			throw new NullPointerException("listener");
		_mouseMotionListeners.add(listener);
	}

	public void removeExpediteeMouseMotionListener(MouseMotionListener listener) {
		if (listener == null)
			throw new NullPointerException("listener");
		_mouseMotionListeners.remove(listener);
	}

	public void addExpediteeMouseWheelListener(MouseWheelListener listener) {
		if (listener == null)
			throw new NullPointerException("listener");
		_mouseWheelListeners.add(listener);
	}

	public void removeExpediteeMouseWheelListener(MouseWheelListener listener) {
		if (listener == null)
			throw new NullPointerException("listener");
		_mouseWheelListeners.remove(listener);
	}

	/**
	 * Conceal event catching from outside
	 * 
	 * @author Brook Novak
	 */
	private class EventCatcher implements AWTEventListener {

		/**
		 * All events for every component in frame are fired to here
		 */
		public void eventDispatched(AWTEvent event) {
			//do nothing
			if (event instanceof MouseEvent) {
				//routeMouseEvent((MouseEvent) event);
				//System.out.println("event: " + event.toString());
			}
		}

	}

	/**
	 * Forwards the mouse event to its appropriate destination. For events that
	 * belong to Expiditee space the events are consumed and manually routed to
	 * the mouse actions handler.
	 * 
	 * @param e
	 *            The mouse event for any component in the browser frame
	 */
	private void forwardMouseEvent(MouseEvent e) {
		_currentMouseEvent = e;

		// First convert the point to expeditee space
		Point containerPoint = SwingUtilities.convertPoint(e.getComponent(), e
				.getPoint(), _contentPane);
		
		// TODO: Find a reliable way of detecting when the mouse moved onto a window that isn't a child of ours
		if(e.getID() == MouseEvent.MOUSE_EXITED) {
			// System.out.println(e.getComponent());
			if(containerPoint.x <= 0 || containerPoint.x >= _contentPane.getWidth() ||
					containerPoint.y <= 0 || containerPoint.y >= _contentPane.getHeight()) {
				FrameMouseActions.mouseExitedWindow(e);
			}
		}

		if (containerPoint.y < 0) { // not in the content pane

			if (_menuBar != null
					&& containerPoint.y + _menuBar.getHeight() >= 0) {
				// The mouse event is over the menu bar.
				// Could handle specially.
			} else {
				// The mouse event is over non-system window
				// decorations, such as the ones provided by
				// the Java look and feel.
				// Could handle specially.
			}

		} else {

			// Check to see if the mouse is over an expeditee item or
			// whether an expeditee item is currently picked up
			boolean forwardToExpiditee = false;
			boolean isOverPopup = PopupManager.getInstance().isPointOverPopup(
					containerPoint);
			if (isOverPopup) {
				// Popups have highest preference
				// forwardToExpiditee = false...

				// If there ate items in free space - keep them moving with the
				// cursor over the
				// popups.
				if (!FreeItems.getInstance().isEmpty()) {
					FrameMouseActions.move(FreeItems.getInstance());
				}

				// Note: all frame.content pane events belong to expeditee
			} else if (e.getSource() == _contentPane
					|| e.getSource() == Browser._theBrowser
					|| !FreeItems.getInstance().isEmpty()) {
				forwardToExpiditee = true;
			} else if (DisplayIO.getCurrentFrame() != null) {
				/* is mouse over a specific expeditee item? */
				// NOTE: Do not use FrameUtils.getCurrentItem() - thats relevent
				// for old mouse position only
				/*
				 * for (Item i : DisplayIO.getCurrentFrame().getItems()) { if
				 * (i.getPolygon().contains(containerPoint)) {
				 * forwardToExpiditee = true; break; } }
				 */// ABOVE: Does not consider overlays
				// NOTE: Below is an expensive operation and could be re-used
				// when passing mouse events!!!
				forwardToExpiditee = (FrameUtils.onItem(DisplayIO
						.getCurrentFrame(), containerPoint.x, containerPoint.y,
						true) != null);
			}

			// Create artificial mouse event and forward it to expeditee
			MouseEvent expediteeEvent = SwingUtilities.convertMouseEvent(e
					.getComponent(), e, _contentPane);

			// NOTE: Convert util masks-out the needed extensions
			MouseEvent withExtensions = null;

			if (forwardToExpiditee) {

				// Ensure that underlying widgets don't get the event
				e.consume();

				switch (e.getID()) {
				case MouseEvent.MOUSE_MOVED:

					for (MouseMotionListener listener : _mouseMotionListeners) {
						listener.mouseMoved(expediteeEvent);
					}

					// Ensure that expiditee has focus only if no popups exist
					if (Browser._theBrowser != null
							&& Browser._theBrowser.getContentPane() != null) {
						if (!Browser._theBrowser.getContentPane()
								.isFocusOwner()
								&& !isPopupVisible()) {
							Browser._theBrowser.getContentPane().requestFocus();
						}
					}

					break;

				case MouseEvent.MOUSE_CLICKED:

					withExtensions = duplicateMouseEvent(expediteeEvent, e
							.getModifiers()
							| e.getModifiersEx());

					for (MouseListener listener : _mouseListeners) {
						listener.mouseClicked(withExtensions);
					}

					break;

				case MouseEvent.MOUSE_PRESSED:

					withExtensions = duplicateMouseEvent(expediteeEvent, e
							.getModifiers()
							| e.getModifiersEx());

					for (MouseListener listener : _mouseListeners) {
						listener.mousePressed(withExtensions);
					}

					break;

				case MouseEvent.MOUSE_RELEASED:
					withExtensions = duplicateMouseEvent(expediteeEvent, e
							.getModifiers()
							| e.getModifiersEx());

					for (MouseListener listener : _mouseListeners) {
						listener.mouseReleased(withExtensions);
					}

					break;
				case MouseEvent.MOUSE_WHEEL:
					MouseWheelEvent mwe = (MouseWheelEvent) expediteeEvent;
					for (MouseWheelListener listener : _mouseWheelListeners) {
						listener.mouseWheelMoved(mwe);
					}
					break;
				case MouseEvent.MOUSE_ENTERED:
					withExtensions = duplicateMouseEvent(expediteeEvent, e
							.getModifiers()
							| e.getModifiersEx());
					for (MouseListener listener : _mouseListeners) {
						listener.mouseEntered(withExtensions);
					}
					break;
				case MouseEvent.MOUSE_EXITED:
					for (MouseListener listener : _mouseListeners) {
						listener.mouseExited(withExtensions);
					}
					break;
				case MouseEvent.MOUSE_DRAGGED:
					for (MouseMotionListener listener : _mouseMotionListeners) {
						listener.mouseDragged(expediteeEvent);
					}
					break;
				}

			} else {

				// Keep expeditees mouse X/Y updated
				FrameMouseActions.MouseX = expediteeEvent.getX();
				FrameMouseActions.MouseY = expediteeEvent.getY();

				// If forwarding to swing ensure that widgets are not
				// highlighted
				// to give visual feedback yo users such that swing has focus.
				Item i = FrameMouseActions.getlastHighlightedItem();
				if (i != null
						&& i.getHighlightMode() != Item.HighlightMode.None) {
					FrameGraphics.changeHighlightMode(i,
							Item.HighlightMode.None);
				}

				// Also bring expideditee behaviour to swing: Auto-focus on
				// component
				// whenever the mouse moves over it.
				if (e.getID() == MouseEvent.MOUSE_MOVED) {
					if (e.getSource() instanceof Component) {
						Component target = (Component) e.getSource();
						if (!target.isFocusOwner()) {
							target.requestFocus();
						}
					}
					// Auto-hide popups when user click on something other other
					// than
					// a popup - and is not a on a popup invoker
				} else if (!isOverPopup
						&& e.getID() == MouseEvent.MOUSE_PRESSED
						&& !PopupManager.getInstance().isInvoker(
								e.getComponent())) {
					PopupManager.getInstance().hideAutohidePopups();
				}
			}
		}
		
		Help.updateStatus();
	}

	public static boolean isPopupVisible() {
		return isPopupVisible(Browser._theBrowser.getLayeredPane());
	}

	private static boolean isPopupVisible(Container parent) {

		for (Component c : parent.getComponents()) {

			if (c instanceof JPopupMenu && ((JPopupMenu) c).isVisible()) {
				return true;

			} else if (c instanceof Container && c != Browser._theBrowser.getContentPane()) {
				if (isPopupVisible((Container) c))
					return true;
			}
		}

		return false;
	}

	private MouseEvent duplicateMouseEvent(MouseEvent e, int modifiers) {
		return new MouseEvent(e.getComponent(), e.getID(), e.getWhen(), modifiers, e.getX(), e.getY(),
				e.getClickCount(), e.isPopupTrigger(), e.getButton());
	}

	public static MouseEvent getCurrentMouseEvent() {

		return _currentMouseEvent;

	}
}