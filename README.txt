

To compile the Expeditee source code, make sure you a version of the Java
Development Kit (JDK) installed.  A minimum of Java 1.7 is required, and we
recommend Java 1.8 to utilize the JFX features Expeditee includes.

For Unix users you might, for example, do something like the following:

  # Put javac on your PATH
  export JAVA_HOME=/opt/java/jdk1.8.0
  export PATH=$JAVA_HOME/bin:$PATH

  # Compile up the code
  ant build

  # Then run it
  ant run

