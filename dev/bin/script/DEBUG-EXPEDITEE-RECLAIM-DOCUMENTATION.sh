#!/bin/bash

if [ "x$expeditee_filespace_home" = "x" ] ; then
  expeditee_filespace_home="/tmp/expeditee-filespace-home"
fi

argc=$#
if [ $argc != 1 ] ; then
    echo "Usage: DEBUG-EXPEDITEE-RECLAIM-DOCUMENTATION.sh doc-frameset" 1>&2
    exit
fi

fset=$1

if [ ! -d "$expeditee_filespace_home/documentation/$fset" ] ; then
    echo "Error: Failed to find directory '$fset'" 1>&2
    exit
fi

    
echo "****"

echo "* Reclaiming document frameset '$fset' from expeditee.home=$expeditee_filespace_home"
echo " ..."

echo "     Copying from: "
echo "       $expeditee_filespace_home/documentation/$fset"
echo "     To:"
echo "       $EXPEDITEE_HOME/src/org/expeditee/assets/resources/documentation/."

/bin/cp -r $expeditee_filespace_home/documentation/$fset \
	$EXPEDITEE_HOME/src/org/expeditee/assets/resources/documentation/.

echo " ... Done"
echo "****"

echo ""
echo "****"
echo "* Now run:"
echo "*   ant jar"
echo "* to ensure the new documentation frameset is bundled into Expeditee.jar"
echo "****"
